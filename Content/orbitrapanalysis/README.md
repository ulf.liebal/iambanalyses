# orbitrapanalysis
This README contains all information on the orbitrapanalysis package and how to use it.

---

## Basics
orbitrapanalysis features two classes for storing and processing data from Orbitrap mass spectrometers:
- The `ExpData` class serves as the first point of contact for the user. It is directly constructed from a .mzML file by specifying its location like this: `exp_data = ExpData("path/to/file.mzML")`. After construction `exp_data` will be the object you use for analysis. Of course you can also give it a different name or create several objects, e.g. from different files. It has two copies of the data (as so-called dataframes) you can access by using either `exp_data.df_original` for the original converted data from the file or `exp_data.df` for the working copy of that data that will be used for filtering et. by default. This way you can always quickly go back to your original data by typing `exp_data.df = exp_data.df_original.copy()`.
- The `FormattedData` class is constructed from an `ExpData` object like this: `formatted_data = FormattedData(exp_data, round_to_decimals=4)` and formats the data in a better human readable form, that can also be exported to .csv and analyzed with other tools. The `round_to_decimals` parameter specifies the number of decimals to which the m/z values are rounded. This is necessary because the m/z values are subject to some measurement error and therefore have to be rounded so that two measurements that really stem from the same m/z are also mapped to the same value instead of each on their own. It is however not strictly required to use the `round_to_decimals` parameter. Depending on the accuracy of your experiment you have to pick a suitable value. The default value is 4, which is usually a good starting point. 

## Functions
#### `convert_file(input_path, output_path)`
Converts an OrbiTrap .raw file to .mzML format. `input_path` can either specify a single file or a whole directory. If a directory is given, all .raw files in that directory will be converted. The converted files will be saved in the directory specified by `output_path`. If `output_path` is not specified, the converted files will be saved in the same directory as the original files. The function returns a list of the paths to the converted files. If a .mzML file already exists in the output directory, it will not be overwritten.
Examples: 
- `convert_file("path/to/directory", "path/to/output")`
- `convert_file("path/to/file.RAW", "path/to/output_file.mzML")`

### ExpData class methods
The following methods are available for the `ExpData` class. They can be called by typing `exp_data.method_name()`, where `exp_data` is an `ExpData` object.

---

#### `subsample(step_size, df=None, inplace=True)`
Subsamples the data by keeping every `step_size`-th entry.
- `step_size` specifies the step size to use for subsampling.
- ***optional***: `df` specifies the dataframe to subsample. If `df` is not specified, the working dataframe of the ExpData object will be subsampled. (default: None)
- ***optional***: `inplace` specifies whether to subsample the dataframe inplace or to return a copy of the subsampled dataframe. (default: True)

#### `filter_by_intensity(df=None, min_intensity=None, max_intensity=None, inplace=True, disable_tqdm=False)`
Filters the data by intensity, i.e. removes all entries where the intensity is lower than `min_intensity` or higher than `max_intensity`.
- ***optional***: `df` specifies the dataframe to filter. If `df` is not specified, the working dataframe of the ExpData object will be filtered. (default: None)
- ***optional***: `min_intensity` specifies the minimum intensity to keep. (default: None)
- ***optional***: `max_intensity` specifies the maximum intensity to keep. (default: None)
- ***optional***: `inplace` specifies whether to filter the dataframe inplace or to return a copy of the filtered dataframe. (default: True)
- ***optional***: `disable_tqdm` specifies whether to disable the progress bar. (default: False)

#### `remove_mz(mz_range, inplace=False, disable_tqdm=False)`
Removes all entries with m/z values in the specified range.
- `mz_range` is a tuple that specifies the m/z range to remove. (example: `(100, 200)`)
- ***optional***: `inplace` specifies whether to remove the entries inplace or to return a copy of the dataframe with the entries removed. (default: False)
- ***optional***: `disable_tqdm` specifies whether to disable the progress bar. (default: False)

#### `filter_time(min_time=None, max_time=None, df=None, inplace=True)`
Filters the data by time, i.e. removes all data where the time is lower than `min_time` or higher than `max_time`.
- ***optional***: `min_time` specifies the minimum time to keep. (default: None)
- ***optional***: `max_time` specifies the maximum time to keep. (default: None)
- ***optional***: `df` specifies the dataframe to filter. If `df` is not specified, the working dataframe of the ExpData object will be filtered. (default: None)
- ***optional***: `inplace` specifies whether to filter the dataframe inplace or to return a copy of the filtered dataframe. (default: True)

#### `plot_intensity_distribution(n_bins=50, show_original=True, save_path=None)`
Plots a histogram of the intensity distribution of the data. 
- `n_bins` specifies the number of bins to use for the histogram. (default: 50)
- ***optional***: `show_original` specifies whether to plot the distribution of `df_original` as well for comparison. (default: True) 
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `plot_mz_distribution(n_bins=50, show_original=True, save_path=None)`
Plots a histogram of the m/z distribution of the data.
- `n_bins` specifies the number of bins to use for the histogram. (default: 50)
- ***optional***: `show_original` specifies whether to plot the distribution of `df_original` as well for comparison. (default: True)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `plot_mz_intensity_histogram(n_bins=100, save_path=None)`
Plots a 2D histogram of the m/z and intensity distribution of the data.
- `n_bins` specifies the number of bins to use for the histogram. (default: 100)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `plot_boxplot(showfliers_intensity=False, showfliers_mz=True, save_path=None)`
Plots a boxplot of the intensity and m/z values of the data.
- ***optional***: `showfliers_intensity` specifies whether to show outliers in the intensity boxplot. (default: False)
- ***optional***: `showfliers_mz` specifies whether to show outliers in the m/z boxplot. (default: True)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `scatter_plot(time_min=None, time_max=None, mz_min=None, mz_max=None, normalize_intensity=True, save_path=None)`
Plots a scatter plot of the data. The plot can be restricted to a certain time and m/z range to zoom in. This can be useful to inspect the data for further processing, e.g. with `filter_control_samples()`.
- ***optional***: `time_min` specifies the minimum time to plot. (default: None)
- ***optional***: `time_max` specifies the maximum time to plot. (default: None)
- ***optional***: `mz_min` specifies the minimum m/z to plot. (default: None)
- ***optional***: `mz_max` specifies the maximum m/z to plot. (default: None)
- ***optional***: `normalize_intensity` specifies whether to normalize the intensity values to the range [0, 1]. (default: True)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `scatter_plot3d(time_min=None, time_max=None, mz_min=None, mz_max=None, save_path=None)`
Plots a 3D scatter plot of the data. The plot can be restricted to a certain time and m/z range to zoom in. This can be useful to inspect the data for further processing, e.g. with `filter_control_samples()`.
- ***optional***: `time_min` specifies the minimum time to plot. (default: None)
- ***optional***: `time_max` specifies the maximum time to plot. (default: None)
- ***optional***: `mz_min` specifies the minimum m/z to plot. (default: None)
- ***optional***: `mz_max` specifies the maximum m/z to plot. (default: None)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `plot_features_per_time(save_path=None)`
Plots the number of features per time point.
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `plot_mean_intensity_per_time(save_path=None)`
Plots the mean intensity per time point.
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `export(path, time_interval=None)`
Exports the ExpData objects working dataframe to a .csv file.
- `path` specifies the path to export the dataframe to.
- ***optional***: `time_interval` specifies the time interval to export as a tuple (example: `(30.5, 120.7)`). If `time_interval` is not specified, the whole dataframe will be exported. (default: None)

### FormattedData class methods
The following methods are available for FormattedData objects. You can use them on your FormattedData object like this: `formatted_data.method_name()`.

---

#### `filter_mz(mz_range: tuple)`
Filters the data by m/z range.
- `mz_range` specifies the m/z range to filter by as a tuple (example: `(100, 200)`).

#### `filter_time(min_time=None, max_time=None, df=None, inplace=True)`
Filters the data by time, i.e. removes all data where the time is lower than `min_time` or higher than `max_time`.
- ***optional***: `min_time` specifies the minimum time to keep. (default: None)
- ***optional***: `max_time` specifies the maximum time to keep. (default: None)
- ***optional***: `df` specifies the dataframe to filter. If `df` is not specified, the working dataframe of the ExpData object will be filtered. (default: None)
- ***optional***: `inplace` specifies whether to filter the dataframe inplace or to return a copy of the filtered dataframe. (default: True)

#### `filter_control_samples(control_sample_interval, inplace=True)`
Filters all m/z traces that can be found in the control samples. This can be useful to remove background noise from the data.
- `control_sample_interval` specifies the time interval to use for the control samples as a tuple (example: `(30.5, 60.7)`).
- ***optional***: `inplace` specifies whether to filter the dataframe inplace or to return a copy of the filtered dataframe. (default: True)

#### `scatter_plot(time_min=None, time_max=None, mz_min=None, mz_max=None, normalize_intensity=True, save_path=None)`
Plots a scatter plot of the data. The plot can be restricted to a certain time and m/z range to zoom in. This can be useful to inspect the data for further processing, e.g. with `filter_control_samples()`.
- ***optional***: `time_min` specifies the minimum time to plot. (default: None)
- ***optional***: `time_max` specifies the maximum time to plot. (default: None)
- ***optional***: `mz_min` specifies the minimum m/z to plot. (default: None)
- ***optional***: `mz_max` specifies the maximum m/z to plot. (default: None)
- ***optional***: `normalize_intensity` specifies whether to normalize the intensity values to the range [0, 1]. (default: True)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `scatter_plot3d(time_min=None, time_max=None, mz_min=None, mz_max=None, normalize_intensity=True, save_path=None)`
Plots a 3D scatter plot of the data. The plot can be restricted to a certain time and m/z range to zoom in. This can be useful to inspect the data for further processing, e.g. with `filter_control_samples()`.
- ***optional***: `time_min` specifies the minimum time to plot. (default: None)
- ***optional***: `time_max` specifies the maximum time to plot. (default: None)
- ***optional***: `mz_min` specifies the minimum m/z to plot. (default: None)
- ***optional***: `mz_max` specifies the maximum m/z to plot. (default: None)
- ***optional***: `normalize_intensity` specifies whether to normalize the intensity values to the range [0, 1]. (default: True)
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `order_by_mean_activation_time(inplace=False)`
Orders the data by the mean activation time of the m/z traces. This can be visualized in the heatmap and serves as an alternative to the sorting of the clustermap.
- ***optional***: `inplace` specifies whether to order the dataframe inplace or to return a copy of the ordered dataframe. (default: False)

#### `plot_heatmap(save_path=None)`
Plot the heatmap of the data.
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `normalize(df=None, inplace=False)`
Normalizes the intensities row-wise to the range [0, 1] by calculating x' = (x - min(row)) / (max(row) - min(row)).
- ***optional***: `df` specifies the dataframe to normalize. If `df` is not specified, the working dataframe of the ExpData object will be normalized. (default: None)
- ***optional***: `inplace` specifies whether to normalize the dataframe inplace or to return a copy of the normalized dataframe. (default: False)

#### `z_score(df=None, inplace=False)`
Standardizes the intensities row-wise by calculating x' = (x - mean(row)) / std(row), so that each row has a mean of 0 and a standard deviation of 1.
- ***optional***: `df` specifies the dataframe to standardize. If `df` is not specified, the working dataframe of the ExpData object will be standardized. (default: None)
- ***optional***: `inplace` specifies whether to standardize the dataframe inplace or to return a copy of the standardized dataframe. (default: False)

#### `plot_clustermap(save_path=None)`
Plot the clustermap of the data. The clustermap is similar to the heatmap, but also clusters the data using ward's method with euclidean distance in a hierarchical manner.
- ***optional***: `save_path` specifies the path to save the plot to. If `save_path` is not specified, the plot will not be saved. (default: None)

#### `export(path, time_interval=None)`
Exports the data to a .csv file.
- `path` specifies the path to export the data to.
- ***optional***: `time_interval` specifies the time interval to export. If `time_interval` is not specified, the whole data will be exported. (default: None)

## Information for developers
The file conversion from .raw to .mzML uses [ThermoRawFileParser](https://compomics.github.io/projects/ThermoRawFileParser).
The processing of the .mzML files is based on [PyOpenMS](https://pyopenms.readthedocs.io/).