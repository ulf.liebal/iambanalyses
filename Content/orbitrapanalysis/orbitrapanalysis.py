import os
import logging
import glob
import warnings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from pathlib import Path
from typing import Tuple, List
from pyopenms import MSExperiment, MzMLFile, SavitzkyGolayFilter
from tqdm.notebook import tqdm
from scipy.cluster.hierarchy import ward, fcluster, dendrogram
from scipy.spatial.distance import pdist, squareform
pd.options.mode.chained_assignment = None  # default='warn'

logging.basicConfig(format="[%(asctime)s] %(levelname)s - %(message)s", level=logging.INFO)
SCRIPT_DIR = Path.cwd()  # Directory of the file that is currently executing this package
PACKAGE_PATH = Path(__file__).parent.resolve()  # This packages directory
FILEPARSER_PATH = PACKAGE_PATH.joinpath("ThermoRawFileParser1.4.2", "ThermoRawFileParser.exe")  # Path to ThermoRawFileParser.exe


def handle_paths(input_path, output_path=None) -> Tuple[List[Path], Path]:
    """
    Handles the preprocessing of input and output paths for the convert_file function.
    :param input_path:
    :param output_path:
    :return: input_file_paths, output_dir_path: List of input file paths and output directory path
    """
    if input_path is None:
        raise Exception("No input path specified.")
    elif input_path.is_file():
        input_file_paths = [input_path]
    elif input_path.is_dir():
        input_file_paths = [Path(file_path) for file_path in glob.glob(os.path.join(input_path, '*.[rR][aA][wW]'))]
    else:
        raise Exception("No .raw file found for the given path. Please make sure you specified the path correctly and the files you wish to convert are placed correctly.")

    if output_path is not None:
        # TODO: check if output_path is legal path
        output_dir_path = PACKAGE_PATH.joinpath(output_path)
    else:
        if input_path.is_file():
            output_dir_path = input_path.parent
        elif input_path.is_dir():
            output_dir_path = input_path
        else:
            raise Exception()
    if not output_dir_path.exists() and '.' not in output_dir_path.name:
        output_dir_path.mkdir()

    return input_file_paths, output_dir_path


def convert_file(input_path, output_path=None) -> None:
    """
    Converts OrbiTrap files from .RAW to .mzML format using ThermoRawFileParser (https://github.com/compomics/ThermoRawFileParser)
    :param input_path: either the file path of the file to convert or the directory containing all files to convert
    :param output_path: path to directory to save the converted files
    :return: None
    """
    input_paths, output_path = handle_paths(input_path, output_path)

    # Convert all found files to .mzML
    if not input_paths:
        logging.info("No files found.")
    else:
        for input_path in input_paths:
            if Path(output_path.joinpath(input_path.stem).with_suffix('.mzML')).exists():
                logging.warning(f"{output_path.joinpath(input_path.stem).with_suffix('.mzML')} already exists.")
            else:
                logging.info(f"Starting conversion of '{input_path.name}' ...")

                if '.' in output_path.name:
                    output_type = '-b'  # flag for output file
                    output_path = output_path.parent.joinpath(output_path.stem)  # remove file extension
                else:
                    output_type = '-o'  # flag for output directory

                command = f"mono '{FILEPARSER_PATH}' -i='{input_path}' {output_type}='{output_path}'"
                logging.debug(command)
                os.system(command)

                logging.info(f"'{input_path.name}' conversion completed!")


def export_to_csv(df: pd.DataFrame, file_path: str, disable_tqdm: bool = False) -> None:
    """
    Exports the given dataframe to a csv file.
    :param disable_tqdm:
    :param df: dataframe to export
    :param file_path: path to export to
    :return: None
    """
    try:
        if df.shape[1] < 100:
            df.to_csv(file_path)
        else:
            chunks = np.array_split(df.index, 100)  # split dataframe into 100 chunks

            for chunk, subset in enumerate(tqdm(chunks, disable=disable_tqdm)):
                if chunk == 0:  # first row
                    df.loc[subset].to_csv(file_path, mode='w', index=True)
                else:
                    df.loc[subset].to_csv(file_path, header=None, mode='a', index=True)

        logging.info(f"Exported DataFrame to csv file: {file_path}")
    except Exception as e:
        logging.error(f"Could not export to csv file: {e}")


def get_all_values(dataframe_column) -> np.ndarray:
    """
    Get all values from a dataframe column, even if each cell contains a list.
    :param dataframe_column:
    :return: Array of all values from the given dataframe column
    """
    return np.concatenate(dataframe_column.values)


class ExpData:
    def __init__(self, mzml_path: Path):
        """
        Initializes the ExpDataFrame object.
        :param mzml_path: path to mzml file
        """
        # Init original dataframe `df_original` and dataframe `df` for processing
        self.exp = self.load_mzml(mzml_path)
        self.df_original = self.exp_to_dataframe()
        self.df = self.df_original.copy()

    @staticmethod
    def load_mzml(file_path) -> MSExperiment:
        file_path = str(file_path)
        try:
            logging.info(f"Loading '{file_path}' ...")
            exp = MSExperiment()
            MzMLFile().load(file_path, exp)
            # Apply Savitzky-Golay filter to smooth data (TODO: check if this is necessary)
            sgf = SavitzkyGolayFilter()
            sgf.filterExperiment(exp)
            logging.info(f"Successfully loaded '{file_path}'")
            return exp
        except Exception as e:
            logging.error(e)
            raise e

    def exp_to_dataframe(self) -> pd.DataFrame:
        """
        Converts the MSExperiment to a pandas dataframe.
        :return: pandas dataframe with columns 'time', 'mz', 'intensity'
        """
        # df = exp.get_df()  # only works in pyOpenMS >=3.0.0
        df = pd.DataFrame(columns=['time', 'mz', 'intensity'])
        df['mz'], df['intensity'] = zip(*[spectrum.get_peaks() for spectrum in self.exp])
        df['time'] = [spectrum.getRT() for spectrum in self.exp]
        return df

    def get_stats(self) -> None:
        """
        Print size of dataframe and number of unique mz values.
        :return:
        """
        time = self.df['time'].max() - self.df['time'].min()
        logging.info(f"Dataframe contains {self.df.shape[0]} time points in a range from {self.df['time'].min()}s - {self.df['time'].max()}s \n"
                     f"Duration: {time / 60} min | {time / 3600} h\n"
                     f"Time points per second: {self.df.shape[0] / time}\n"
                     f"Distinct m/z values: {len(np.unique(get_all_values(self.df['mz'])))}\n"
                     f"Total data points: {len(get_all_values(self.df['mz']))}\n")

    def subsample(self, step_size, df: pd.DataFrame = None, inplace: bool = True):
        """
        Subsamples the ExpData dataframe by only keeping every n-th time step. Accepts optional dataframe argument.
        :return: None or subsampled dataframe
        """
        if df is None:
            df = self.df_original.copy()
        df = df.iloc[::step_size]
        logging.info(f"Subsampled dataframe. Now contains {df.shape[0]} time points instead of {self.df.shape[0]}.")

        if inplace:
            self.df = df
        else:
            return df

    def filter_by_intensity(self, df: pd.DataFrame = None, min_intensity: float = None, max_intensity: float = None, inplace: bool = True, disable_tqdm: bool = False) -> None or pd.DataFrame:
        """
        Filters the given MSExperiment by intensity.
        :param df: Optional dataframe to filter. If None, the dataframe of the MSExperiment is used.
        :param min_intensity: minimum intensity to filter by
        :param max_intensity: maximum intensity to filter by
        :param inplace:
        :param disable_tqdm: whether to disable the tqdm progress bar (default: False)
        :return: filtered dataframe if inplace is False, otherwise None
        """
        logging.info(f"Filtering dataframe by intensity ...")
        if df is None:
            df = self.df.copy()
        for index, row in tqdm(df.iterrows(), total=df.shape[0], disable=disable_tqdm):
            if min_intensity is not None:
                df['intensity'][index] = row['intensity'][row['intensity'] > min_intensity]
                df['mz'][index] = row['mz'][row['intensity'] > min_intensity]
            if max_intensity is not None:
                df['intensity'][index] = row['intensity'][row['intensity'] < max_intensity]
                df['mz'][index] = row['mz'][row['intensity'] < max_intensity]

        if inplace:
            self.df = df
        else:
            return df

    def remove_mz(self, mz_range: tuple, inplace: bool = False, disable_tqdm: bool = False) -> pd.DataFrame:
        """
        Filters the given mass charge ratio range from the given dataframe.
        :param inplace:
        :param disable_tqdm: specifies whether to disable tqdm
        :param mz_range: range of mass charge ratios to filter
        :return: dataframe with mz filtered
        """
        logging.info("Filtering mz range...")
        df = self.df.copy()
        for index, row in tqdm(df.iterrows(), total=df.shape[0], disable=disable_tqdm):
            df['intensity'][index] = row['intensity'][np.logical_or(row['mz'] < mz_range[0], row['mz'] > mz_range[1])]
            df['mz'][index] = row['mz'][np.logical_or(row['mz'] < mz_range[0], row['mz'] > mz_range[1])]
        logging.info("mz filtering complete.")

        if inplace:
            self.df = df
        else:
            return df

    def filter_time(self, min_time: float = None, max_time: float = None, df: pd.DataFrame = None, inplace: bool = True) -> None or pd.DataFrame:
        """
        Filters everything except the given time range from the given dataframe.
        :param min_time: Minimum time to filter
        :param max_time: Maximum time to filter
        :param df: Optional dataframe to filter. If not specified, the dataframe stored in the object will be filtered.
        :param inplace: Whether to filter the dataframe in place or return a new dataframe
        :return: dataframe with time filtered
        """
        if df is None:
            df = self.df.copy()

        if min_time is None and max_time is None:
            raise ValueError("Either min_time or max_time must be specified. Otherwise, the dataframe will not be filtered.")
        elif min_time is None:
            min_time = df.time.min()
        elif max_time is None:
            max_time = df.time.max()

        df = df[(df['time'] >= min_time) & (df['time'] <= max_time)]

        if inplace:
            self.df = df
        else:
            return df

    def plot_intensity_distribution(self, n_bins: int = 50, show_original: bool = True, save_path: Path = None) -> None:
        """
        Plots the intensity distribution of the given dataframe.
        :param n_bins: number of bins for the histogram (default: 50)
        :param show_original: whether to show the original dataframe for comparison (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        if show_original:
            fig, (ax1, ax2) = plt.subplots(1, 2)
            ax2.set_xlabel("intensity (original)")
            ax2.hist(get_all_values(self.df_original['intensity']), bins=n_bins)
        else:
            fig, ax1 = plt.subplots(1, 1)

        fig.suptitle('intensity distribution')
        ax1.set_xlabel("intensity (filtered)")
        ax1.set_ylabel("count")
        ax1.hist(get_all_values(self.df['intensity']), bins=n_bins)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def plot_mz_distribution(self, n_bins: int = 50, show_original: bool = True, save_path: Path = None) -> None:
        """
        Plots the mass to charge ratio distribution of the given dataframe.
        :param n_bins: number of bins for the histogram (default: 50)
        :param show_original: whether to show the original dataframe for comparison (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        if show_original:
            fig, (ax1, ax2) = plt.subplots(1, 2)
            ax2.set_xlabel("mz (original)")
            ax2.hist(get_all_values(self.df_original['mz']), bins=n_bins)
        else:
            fig, ax1 = plt.subplots(1, 1)

        fig.suptitle('mz distribution')
        ax1.set_xlabel("mz (filtered)")
        ax1.set_ylabel("count")
        ax1.hist(get_all_values(self.df['mz']), bins=n_bins)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def plot_mz_intensity_histogram(self, n_bins: int = 100, save_path: Path = None) -> None:
        """
        Plots a 2D histogram of the mz and intensity values.
        :param n_bins: number of bins for the histogram (default: 100)
        :param save_path: path to save the plot to
        :return: None
        """
        fig, ax = plt.subplots()
        ax.set_xlabel("intensity")
        ax.set_ylabel("mz")
        ax.set_title("mz / intensity histogram")
        x = get_all_values(self.df['intensity'])
        y = get_all_values(self.df['mz'])
        ax.hist2d((x - min(x)) / (max(x) - min(x)), y, bins=(n_bins, n_bins), cmin=1, cmap='viridis')
        # plt.colorbar(label='count')

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def plot_boxplot(self, showfliers_intensity=False, showfliers_mz=True, save_path: Path = None) -> None:
        """
        Plots a boxplot of the given dataframe.
        :param showfliers_intensity: whether to show outliers in the intensity boxplot (default: False)
        :param showfliers_mz: whether to show outliers in the mz boxplot (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        # Collect all intensity and mass charge ratio values in one array each
        intensities = get_all_values(self.df['intensity'])
        mass_charge_ratios = get_all_values(self.df['mz'])

        # Plot boxplots
        fig, (ax1, ax2) = plt.subplots(1, 2)
        # plt.suptitle(f"Boxplots of intensity and mass charge ratio")
        ax1.set_title(f'min: {min(intensities)}\nmax: {max(intensities)}')
        ax1.boxplot(intensities, showfliers=showfliers_intensity)
        ax1.set_xticklabels(['intensity'])
        ax2.set_title(f'min: {min(mass_charge_ratios)}\nmax: {max(mass_charge_ratios)}')
        ax2.boxplot(mass_charge_ratios, showfliers=showfliers_mz)
        ax2.set_xticklabels(['mass charge ratio'])
        fig.tight_layout()

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def scatter_plot(self, time_min: float = None, time_max: float = None, mz_min: float = None,
                     mz_max: float = None, normalize_intensity: bool = True, save_path: Path = None) -> None:
        """
        Plots a scatter plot of the given dataframe.
        :param time_min: Minimum time to plot
        :param time_max: Maximum time to plot
        :param mz_min: Minimum mass charge ratio to plot
        :param mz_max: Maximum mass charge ratio to plot
        :param normalize_intensity: Whether to normalize the intensity values (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        fig, ax = plt.subplots()
        ax.set_xlabel("Time [s]")
        ax.set_ylabel("mass charge ratio (m/z)")
        ax.set_title("m/z over time")

        x = np.concatenate([[row['time']] * len(row['mz']) for index, row in self.df.iterrows()])
        y = np.concatenate(self.df['mz'].values)
        z = np.concatenate(self.df['intensity'].values)
        if normalize_intensity:
            z = (z - min(z)) / (max(z) - min(z))

        plt.scatter(x, y, c=(z - min(z)) / (max(z) - min(z)), cmap='viridis', s=1)
        plt.colorbar(label='intensity')

        if time_min is not None and time_max is not None:
            plt.xlim(time_min, time_max)
        elif time_min is not None:
            plt.xlim(time_min, max(x))
        elif time_max is not None:
            plt.xlim(min(x), time_max)
        if mz_min is not None and mz_max is not None:
            plt.ylim(mz_min, mz_max)
        elif mz_min is not None:
            plt.ylim(mz_min, max(y))
        elif mz_max is not None:
            plt.ylim(min(y), mz_max)

        logging.info(
            "Plotting complete. Commencing rendering... Depending on the size of the dataset, this may take a while.")

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def scatter_plot3d(self, time_min: float = None, time_max: float = None, mz_min: float = None,
                       mz_max: float = None, save_path: Path = None) -> None:
        """
        Plots a 3d scatter plot of the given dataframe.
        :param time_min: Minimum time to plot
        :param time_max: Maximum time to plot
        :param mz_min: Minimum mass charge ratio to plot
        :param mz_max: Maximum mass charge ratio to plot
        :param save_path: path to save the plot to
        :return: None
        """
        # from mpl_toolkits import mplot3d
        ax = plt.axes(projection='3d')  # syntax for 3-D projection
        ax.set_xlabel("time (s)")
        ax.set_ylabel("mass charge ratio (m/z)")
        ax.set_zlabel("intensity")
        ax.set_title("m/z over time")

        x = np.concatenate([[row['time']] * len(row['mz']) for index, row in self.df.iterrows()])
        y = np.concatenate(self.df['mz'].values)
        z = np.concatenate(self.df['intensity'].values)

        ax.scatter(x, y, (z - min(z)) / (max(z) - min(z)), c=z / (max(z) - min(z)), s=1)

        if time_min is not None and time_max is not None:
            ax.set_xlim(time_min, time_max)
        elif time_min is not None:
            ax.set_xlim(time_min, max(x))
        elif time_max is not None:
            ax.set_xlim(min(x), time_max)
        if mz_min is not None and mz_max is not None:
            ax.set_ylim(mz_min, mz_max)
        elif mz_min is not None:
            ax.set_ylim(mz_min, max(y))
        elif mz_max is not None:
            ax.set_ylim(min(y), mz_max)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def plot_features_per_time(self, save_path: Path = None) -> None:
        """
        Plots the number of features recorded per time point.
        :param save_path: path to save the plot to
        :return: None
        """
        fig, ax = plt.subplots()

        number_of_features = [len(row['mz']) for index, row in self.df.iterrows()]
        time = self.df['time'].values

        ax.set_title('Number of Features over time')
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Number of Features')
        ax.plot(time, number_of_features)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def plot_mean_intensity_per_time(self, save_path: Path = None) -> None:
        """
        Plots the mean intensity per time point.
        :param save_path: path to save the plot to
        :return: None
        """
        fig, ax = plt.subplots()

        # Expecting 'RuntimeWarning: Mean of empty slice.' here if there are no intensity values for a given time point
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            mean_intensity = [np.mean(row['intensity']) for index, row in self.df.iterrows()]
        time = self.df['time'].values

        ax.set_title('Mean Intensity over time')
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Mean Intensity')
        ax.plot(time, mean_intensity)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def export(self, path: Path, time_interval: tuple = None) -> None:
        """
        Exports the given dataframe to a csv file.
        :param path: path to export the dataframe to
        :param time_interval: time interval to export
        :return: None
        """
        df_export = self.df.copy()
        if time_interval is not None:
            df_export = self.df[self.df['time'].between(time_interval[0], time_interval[1])]
        export_to_csv(df_export, str(path), disable_tqdm=False)


class FormattedData:
    def __init__(self, exp_data: ExpData, round_to_decimals: int = None):
        # Init reformatted dataframe
        if round_to_decimals is not None:
            self.df = self.reformat_dataframe(exp_data.df, round_to_decimals)
        else:
            self.df = self.reformat_dataframe(exp_data.df)

        self.parent_df = exp_data.df

    @staticmethod
    def reformat_dataframe(df: pd.DataFrame, round_to_dec: int = None, disable_tqdm: bool = False) -> pd.DataFrame:
        """
        Reformat the given dataframe to the format expected by the export_to_csv function.
        :param disable_tqdm:
        :param df:
        :param round_to_dec:
        :return: reformatted dataframe
        """
        # TODO: fill in columns with no intensity values
        logging.info("Reformatting dataframe...")

        # Initialize the index column for the dataframe
        index_column = pd.DataFrame(columns=['mz'])
        if round_to_dec is not None:
            index_column['mz'] = pd.Series(np.unique(get_all_values(df['mz']).round(decimals=round_to_dec)))
        else:
            index_column['mz'] = pd.Series(np.unique(get_all_values(df['mz'])))
        index_column.set_index('mz', inplace=True)

        # Determine all time columns and store them in a list
        columns = [index_column]
        for index, row in tqdm(df.iterrows(), total=df.shape[0], disable=disable_tqdm):
            data = dict(zip(row['mz'], row['intensity']))
            if len(data.values()) > 0:  # if there are any intensity values for the given time
                if round_to_dec is not None:
                    # Split the data from the dictionary into mz (key/index) and intensity (value) values
                    index_data = np.sort(np.array(list(data.keys())).round(decimals=round_to_dec))
                    value_data = np.array(list(data.values()))
                    # Create a dataframe from the given data and group by mz to sum the intensity values for each mz
                    df_data = pd.DataFrame({'mz': index_data, 'intensity': value_data}).groupby('mz').sum()
                    df_data.reset_index(inplace=True)  # reset index to make mz available as a column

                    column = pd.DataFrame(df_data['intensity'].values, index=df_data['mz'], columns=[row['time']])
                else:
                    column = pd.DataFrame(pd.Series(data.values(), index=data.keys(), name=row['time']))
                columns.append(column)
        logging.info(
            "Dataframe reformatting complete. Now concatenating columns... Depending on the size of the dataframe, this may take a while.")
        df_reformatted = pd.concat(columns, axis=1)
        logging.info("Concatenation complete.")

        print(f"Shape of reformatted dataframe: {df_reformatted.shape} (mz values x time points)")

        return df_reformatted

    def filter_mz(self, mz_range: tuple) -> None:
        """
        Filter the reformatted dataframe by the given mz range.
        :param mz_range: mz range to filter by
        :return: None
        """
        logging.info("Filtering mz values...")
        for index, row in tqdm(self.df.iterrows(), total=self.df.shape[0]):
            self.df['mz'][index] = row['mz'][np.logical_or(row['mz'] < mz_range[0], row['mz'] > mz_range[1])]
        logging.info("mz filtering complete.")

    def filter_time(self, min_time: float = None, max_time: float = None, df: pd.DataFrame = None, inplace: bool = True) -> None:
        """
        Filter the reformatted dataframe by the given time range.
        :return: None
        """
        if df is None:
            df = self.df.copy()

        if min_time is None and max_time is None:
            raise ValueError(
                "Either min_time or max_time must be specified. Otherwise, the dataframe will not be filtered.")
        elif min_time is None:
            min_time = df.time.min()
        elif max_time is None:
            max_time = df.time.max()

        if inplace:
            self.df = df.loc[:, (df.columns >= min_time) & (df.columns <= max_time)]
        else:
            return df.loc[:, (df.columns >= min_time) & (df.columns <= max_time)]

    def filter_control_samples(self, control_sample_interval: tuple, inplace: bool = True) -> pd.DataFrame:
        """
        Filters the given dataframe by excluding all mz values that are present in the control samples.
        :param inplace:
        :param control_sample_interval:
        :return: Filtered dataframe
        """
        # identify all mz values that are present in the control sample interval
        df_filtered = self.df.copy()
        # Loop over a given time slice of the dataframe. If an entry in the mz row is found, drop the corresponding mz row.
        for col_index in df_filtered.loc[:, control_sample_interval[0]:control_sample_interval[1]]:
            # drop rows where mz is not NaN
            df_filtered = df_filtered[df_filtered[col_index].isna()]

        print(f"Old table size: {self.df.shape} Table size filtered by control sample: {df_filtered.shape}")

        if inplace:
            self.df = df_filtered
        else:
            return df_filtered

    def scatter_plot(self, time_min: float = None, time_max: float = None, mz_min: float = None,
                     mz_max: float = None, normalize_intensity: bool = True, save_path: Path = None) -> None:
        """
        Plots a scatter plot of the given dataframe.
        :param time_min: Minimum time to plot
        :param time_max: Maximum time to plot
        :param mz_min: Minimum mass charge ratio to plot
        :param mz_max: Maximum mass charge ratio to plot
        :param normalize_intensity: Whether to normalize the intensity values (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        logging.info("Plotting scatter plot...")
        data = self.df.to_numpy()

        # get the indices of the non-NaN values
        ix, iy = np.where(~np.isnan(data.T))

        if normalize_intensity:
            min_val = np.nanmin(data[iy, ix])
            max_val = np.nanmax(data[iy, ix])
            z = (data[iy, ix] - min_val) / (max_val - min_val)
        else:
            z = data[iy, ix]

        # Set x and y to the time and mz coordinates of the non-NaN values
        x = self.df.columns.values[ix]  # time values
        y = self.df.index.to_numpy()[iy]  # mz values

        fig, ax = plt.subplots()

        plt.scatter(x, y, c=z, cmap='viridis', s=1)
        ax.set_title('Scatter Plot of Intensity Values')
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('m/z')
        plt.colorbar(label='intensity')

        if time_min is not None and time_max is not None:
            ax.set_xlim(time_min, time_max)
        elif time_min is not None:
            ax.set_xlim(time_min, max(x))
        elif time_max is not None:
            ax.set_xlim(min(x), time_max)
        if mz_min is not None and mz_max is not None:
            ax.set_ylim(mz_min, mz_max)
        elif mz_min is not None:
            ax.set_ylim(mz_min, max(y))
        elif mz_max is not None:
            ax.set_ylim(min(y), mz_max)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def scatter_plot3d(self, time_min: float = None, time_max: float = None, mz_min: float = None,
                     mz_max: float = None, normalize_intensity: bool = True, save_path: Path = None) -> None:
        """
        Plots a scatter plot of the given dataframe.
        :param time_min: Minimum time to plot
        :param time_max: Maximum time to plot
        :param mz_min: Minimum mass charge ratio to plot
        :param mz_max: Maximum mass charge ratio to plot
        :param normalize_intensity: Whether to normalize the intensity values (default: True)
        :param save_path: path to save the plot to
        :return: None
        """
        ax = plt.axes(projection='3d')  # syntax for 3-D projection
        ax.set_xlabel("time (s)")
        ax.set_ylabel("mass charge ratio (m/z)")
        ax.set_zlabel("intensity")
        ax.set_title("m/z over time")

        data = self.df.to_numpy()

        # get the indices of the non-NaN values
        ix, iy = np.where(~np.isnan(data.T))

        if normalize_intensity:
            min_val = np.nanmin(data[iy, ix])
            max_val = np.nanmax(data[iy, ix])
            z = (data[iy, ix] - min_val) / (max_val - min_val)
        else:
            z = data[iy, ix]

        # Set x and y to the time and mz coordinates of the non-NaN values
        x = self.df.columns.values[ix]  # time values
        y = self.df.index.to_numpy()[iy]  # mz values

        ax.scatter(x, y, (z - min(z)) / (max(z) - min(z)), c=z / (max(z) - min(z)), s=1)

        if time_min is not None and time_max is not None:
            ax.set_xlim(time_min, time_max)
        elif time_min is not None:
            ax.set_xlim(time_min, max(x))
        elif time_max is not None:
            ax.set_xlim(min(x), time_max)
        if mz_min is not None and mz_max is not None:
            ax.set_ylim(mz_min, mz_max)
        elif mz_min is not None:
            ax.set_ylim(mz_min, max(y))
        elif mz_max is not None:
            ax.set_ylim(min(y), mz_max)

        if save_path:
            plt.savefig(save_path)
        plt.show()

    def order_by_mean_activation_time(self, inplace: bool = False) -> None:
        # Get working copy of dataframe
        df = self.df.copy().reset_index()
        df.drop(columns=["mz"], inplace=True)

        #mean_activation_times = [np.mean(row.index[~row.isna()].to_numpy()) for i, row in df.iterrows()]
        mean_activation_times = []
        for i, row in df.iterrows():
            diff = row.max() - row.min()
            if diff == 0:
                mean_activation_times.append(np.mean(row.index[~row.isna()].to_numpy()))
            else:
                mean_activation_times.append(np.mean((row.index[~row.isna()] / diff).to_numpy()))

        # Add mean activation time as new column to dataframe
        df["mean_activation_time"] = mean_activation_times

        # Add mz column back to dataframe and make it the index
        df["mz"] = self.df.index
        df.set_index("mz", inplace=True)

        # Order rows by mean activation time
        df.sort_values(by="mean_activation_time", inplace=True)
        # Drop mean activation time column
        df.drop(columns=["mean_activation_time"], inplace=True)

        if inplace:
            self.df = df
        else:
            return df

    def plot_heatmap(self, save_path: Path = None) -> None:
        fig, ax = plt.subplots()
        ax.imshow(np.ma.masked_where(np.isnan(self.df), self.df), cmap='coolwarm', interpolation='nearest', aspect='auto')
        ax.set_yticks(np.arange(len(self.df.index.to_numpy())), labels=self.df.index.to_numpy())
        ax.set_xticklabels(self.df.columns)
        ax.set_title('Heatmap of Intensity Values')
        ax.set_xlabel('Time')
        ax.set_ylabel('m/z')
        ax.invert_yaxis()  # Flip the y-axis

        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        fig.tight_layout()

        if save_path:
            plt.savefig(save_path)

        plt.show()

    def normalize(self, df: pd.DataFrame = None, inplace: bool = False) -> None or pd.DataFrame:
        """
        Normalizes each mz trace of the dataframe with respect to its maximum value.
        :return:
        """
        # TODO: verify that normalization works by calculating row sums
        if df is None:
            df = self.df.copy()

        for index, row in tqdm(df.iterrows(), total=len(df)):
            df.loc[index] = (row - row.min()) / (row.max() - row.min())

        if inplace:
            self.df = df
        else:
            return df

    def z_score(self, df: pd.DataFrame = None, inplace: bool = False) -> None or pd.DataFrame:
        """
        Standardizes each mz trace of the dataframe with respect to its mean and standard deviation, i.e. so that each mz trace has mean 0 and standard deviation 1. See https://en.wikipedia.org/wiki/Standard_score for more information.
        :return:
        """
        if df is None:
            df = self.df.copy()

        df = df.fillna(0)

        for index, row in tqdm(df.iterrows(), total=len(df)):
            row = row[row != 0]
            df.loc[index] = (row - row.mean()) / row.std()

        if inplace:
            self.df = df
        else:
            return df

    def plot_clustermap(self, df: pd.DataFrame = None, save_path: Path = None) -> None:
        """
        Plots a clustermap of the dataframe.
        :param df: Dataframe to plot clustermap of. If None, the clustermap of the dataframe of the object is plotted.
        :param save_path: Path to save the clustermap to. If None, the clustermap is shown.
        :return: None
        """
        if df is None:
            df = self.df.copy()
        if save_path:
            sns.clustermap(df.fillna(0), metric='euclidean', method='ward', cmap='coolwarm', col_cluster=False, mask=df.isnull()).savefig(save_path)
        else:
            sns.clustermap(df.fillna(0), metric='euclidean', method='ward', cmap='coolwarm', col_cluster=False, mask=df.isnull())

    def export(self, path: Path, time_interval: tuple = None) -> None:
        """
        Exports the reformatted dataframe to a csv file.
        :param path: path to export to
        :param time_interval: time interval to export
        :return: None
        """
        logging.info("Exporting reformatted dataframe...")

        # if a time interval is given, filter the dataframe by the given time interval
        df_export = self.df.copy()
        if time_interval:
            df_export = df_export.loc[:, time_interval[0]:time_interval[1]]
        export_to_csv(df_export, str(path), disable_tqdm=False)

        logging.info("Export complete.")
