package FluxY_Lib::MS::DataClass_FA;

################################################################################
# This class provides support for fatty acid methyl esthers and the McLafferty
# fragments.
# - These compounds/fragments are not derivatized (they are methylized).
# - The class data is specific to a carbon labelling experiment.
#
# This class inherits basic functionality from the FragmentClass parent
# class, which extends CompoundClass, initializing it with the compound name, 
# fatty acid fragments, and the atom list which are defined by this class.
# - It overrides the PrintInfoTable() method for a custom layout.
#
# Additional helper methods are implemented;
# - GenerateTotalAtomCount(): Generates a per fragment atom list, and
# - GenerateCBPosition(): Generates a per fragment carbon backbone position list.
# - GenerateCBNum(): Generates a per fragment list of carbon backbone counts.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::FragmentClass);   # Sets the parent class.


# List of the atoms found in fatty acid compounds and subsequently considerred
# for natural abundance:
my @atomList = ('C','H','O');

# The atom counts in the following data structures are arrays of the number of
# each corresponding atom in the atomList, i.e. C, H and O.

# Hash of compound data, (first two array elements are required).
# 1. The compound names (fatty acids), in lower case (for standard comparison).
# 2. The atom count for each fatty acid compound.
my %compounds = (
   c14_0 => ["c14:0", [14,28,2]],
   c16_0 => ["c16:0", [16,32,2]],
   c18_0 => ["c18:0", [18,36,2]],
   c18_1 => ["c18:1", [18,34,2]],
   c18_2 => ["c18:2", [18,32,2]],
   c18_3 => ["c18:3", [18,30,2]],
   c20_0 => ["c20:0", [20,40,2]],
   c20_1 => ["c20:1", [20,38,2]],
   c22_0 => ["c22:0", [22,44,2]],
   c22_1 => ["c22:1", [22,42,2]],
   );
   
# The name and atom count for the methylation of the fatty acid.
my $mAgentName = "Methyl Ester";
my @mAgent     = (1,2,0);

# In addition to the molecular ion, each compound may contain McLafferty
# fragments. These fragments are cleaved from the end of the FA chain, and as 
# such have identical compositions for all compounds.
my %frags = (
   m69 => [[4,7,0], [1,4]],   # McLafferty Fragment M69.
   C2  => [[2,4,2], [1,2]],   # McLafferty Fragment C2.
   C3  => [[3,5,2], [1,3]],   # McLafferty Fragment C3.
   );

my $lElement  = "C";                   # The labeled element.
my $className = "Fatty Acids (FA)";    # The name of the data class.
my $printFile = "FA_Data_Table.txt";   # The name of the info table file.

my $fullFrag  = "M-0";   # The fragment identifier for the unfragmented compound.


################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
# - This class treats the methylation differently then derivatization, as such
#   no derivatizing agent is passed as an argument and there is a placeholder
#   used in the call to the parent constructor.
# - By convention the masses are rounded (instead of being truncated), this is
#   indicated by setting the flag in the parameters passed to the parent
#   constructor.
#
# The detailed data is determined by two private subroutine using the specific 
# compound, fragment and methylation information. This must be done before
# calling the constructor of the parent class, as this is required by the parent
# class.
#
################################################################################
sub new {
   my ($class) = @_;
   
   # Initialize the total atom list.
   my %totAtomCount = GenerateTotalAtomCount();
   my %lBackbone    = GenerateCBPosition();
   my %cBackbone    = GenerateCBNum();

   # Call the constructor of the parent class (FragmentClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement, \%totAtomCount, \%cBackbone, \%lBackbone, "", 1);

   bless $self, $class;

   return $self;
}


###########################
#   Overridden methods.   #
###########################

################################################################################
# Subroutine: PrintInfoTable
#
# Prints a table containing the atom count / fragment mass for each Methylated
# Fatty Acid, and for the McLafferty fragment.
#
# Overridden to provide custom (non-repetative) layout.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;

   open (FILE, ">" . $printFile) or FileSystemError(900, $printFile, $!);
   
   print FILE "Fatty Acid " . $mAgentName . " Data Table:\n\n\t";
   
   print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\n";

   # Full Fatty Acid:
   foreach my $compound ($self->GetCompoundList) {
      my @totAtoms         = $self->GetTotalAtomCount($compound, $fullFrag);
		my ($firstC, $lastC) = $self->GetLabeledBackbone($compound, $fullFrag);
      my $mass             = $self->GetMass($compound, $fullFrag);

      print FILE $compound . "\t\t" . join("\t", @totAtoms) . "\t" . $mass . "\t" . $firstC . "-" . $lastC . "\n";
   }
   
   print FILE "\nMcLafferty Fragments (the same for each compound):\n\n";

   # One of the McLafferty Fragments.
   foreach my $frag (sort keys %frags) {
      my $compound         = ($self->GetCompoundList)[0];
      my @totAtoms         = $self->GetTotalAtomCount($compound, $frag);
		my ($firstC, $lastC) = $self->GetLabeledBackbone($compound, $frag);
      my $mass             = $self->GetMass($compound, $frag);
      
      print FILE "\t" . $frag . "\t" . join("\t", @totAtoms) . "\t" . $mass . "\t" . $firstC . "-" . $lastC . "\n";
   }

   close(FILE)                 or FileSystemError(901, $printFile, $!);
}


########################
#   Private methods.   #
########################

################################################################################
# Subroutine: GenerateTotalAtomCount
#
# Generates a list of atom arrays in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
#
################################################################################
sub GenerateTotalAtomCount {
   my %totalAtomList = ();
   foreach my $compound (sort keys %compounds) {
      my %list = ();
      
      my @totAtoms = @{ $compounds{$compound}[1] };
      for(my $i=0; $i<@totAtoms; $i++) {
         $totAtoms[$i] += $mAgent[$i];
      }
      
      $list{$fullFrag} = \@totAtoms;
      
		foreach my $frag (sort keys %frags) {      
         my @totAtoms    = @{ $frags{$frag}[0] };
         for(my $i=0; $i<@totAtoms; $i++) {
            $totAtoms[$i] += $mAgent[$i];
         }
         
         $list{$frag} = \@totAtoms;
      }
      
      $totalAtomList{$compound} = \%list;
   }
   
   return %totalAtomList;
}


################################################################################
# Subroutine: GenerateCBPosition
#
# Generates a list of carbon backbone position arrays in a hash (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBPosition {
   my %cbList = ();
   foreach my $compound (sort keys %compounds) {
		my %list = ();

      my @lBackbone = (1, $compounds{$compound}[1][0]);
      $list{$fullFrag} = \@lBackbone;
     
		foreach my $frag (sort keys %frags) {
			$list{$frag} = $frags{$frag}[1];
		}
      
		$cbList{$compound} = \%list;
	}
   
   return %cbList;
}


################################################################################
# Subroutine: GenerateCBNum
#
# Generates a list of the number of carbons in the backbone fragment (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBNum {
   my %numCList = ();
   foreach my $compound (sort keys %compounds) {
		my %list = ();

      $list{$fullFrag} = $compounds{$compound}[1][0];
     
		foreach my $frag (sort keys %frags) {
         my @temp = @{ $frags{$frag}[1] };
      
			$list{$frag} = $temp[1] - $temp[0] + 1;
		}
      
		$numCList{$compound} = \%list;
	}
   
   return %numCList;
}

1;