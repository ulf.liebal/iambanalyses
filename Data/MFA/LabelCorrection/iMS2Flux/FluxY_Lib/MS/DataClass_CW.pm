package FluxY_Lib::MS::DataClass_CW;

################################################################################
# This class provides support for cell wall compounds.
# - These compounds are neither derivatized nor fragmented.
#
# This class inherits basic functionality from the CompoundClass parent class,
# initializing it with the compound name, cell wall compounds, and the atom list
# which are defined by this class.
# - It overrides the PrintInfoTable() methods to specify the output file to be 
# used by the parent method.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::CompoundClass);   # Sets the parent class.


# List of the atoms found in cell wall compounds and subsequently considerred
# for natural abundance.
my @atomList = ('C','H','O','N','P');

# Hash of compound data, (first two array elements are required).
# 1. The compound names (cell wall), in lower case (for standard comparison).
# 2. The atom count for each cell wall compound.
#    - Each of the atom counts is an array of the number of each corresponding
#    atom in the atomList, i.e. C, H, O, N and P.
my %compounds = (
   adpglc   => ["adp-glucose",               [16,25,15,5,2]],
   dhap     => ["Dihydroxyacetone phosphate",[3,7,6,0,1]],
   e4p      => ["erythrose 4-phosphate",     [4,9,7,0,1]],
   fru6p    => ["fructose 6-phosphate",      [6,13,9,0,1]],
   frubp    => ["fructose 1,6-bisphosphate", [6,14,12,0,2]],
   glc1p    => ["glucose 1-phosphate",       [6,13,9,0,1]],
   glc6p    => ["glucose 6-phosphate",       [6,13,9,0,1]],
   gal1p    => ["galactose 1-phosphate",     [6,13,9,0,1]],
   gdpfuc   => ["gdp-fucose",                [16,25,15,5,2]],
   gdpglc   => ["gdp-glucose",               [16,25,16,5,2]],
   gdpman   => ["gdp-mannose",               [16,25,16,5,2]],
   gdphex   => ["gdp-hexose",                [16,25,16,5,2]],
   gmp      => ["guanosine monophosphate",   [10,14,8,5,1]],
   hex1p    => ["hexose 1-phosphate",        [6,13,9,0,1]],
   man1p    => ["mannose 1-phosphate",       [6,13,9,0,1]],
   man6p    => ["mannose 6-phosphate",       [6,13,9,0,1]],
   pg2_3    => ["2/3-phosphoglycerate",      [3,7,7,0,1]],
   pga      => ["3-phosphoglycerate",        [3,7,7,0,1]],
   pg6      => ["6-phosphoglucate",          [6,13,10,0,1]],
   r5p      => ["ribose 5-phosphate",        [5,11,8,0,1]],
   ribup    => ["ribose 5-phosphate",        [5,11,8,0,1]],
   ru5p     => ["Ribulose-5-phosphate",      [5,11,8,0,1]],
   rubp     => ["ribose-1,5-bisphosphate",   [5,12,11,0,2]],
   s7p      => ["sedoheptulose 7-phosphate", [7,15,10,0,1]],
	sbp      => ["sedoheptulose-1,7-bisphosphate", [7,16,13,0,2]],
   sucp     => ["sucrose 6-phosphate",       [12,23,14,0,1]],
   udpapi   => ["udp-apiose",                [14,22,16,2,2]],
   udpara   => ["udp-arabinose",             [14,22,16,2,2]],
   udpgal   => ["udp-galactose",             [15,24,17,2,2]],
   udpgaln  => ["udp-acetylgalactosamine",   [17,27,17,3,2]],
   udpgalte => ["udp-galacturonate",         [15,22,18,2,2]],
   udpglc   => ["udp-glucose",               [15,24,17,2,2]],
   udpglcn  => ["udp-acetylglucosamine",     [17,27,17,3,2]],
   udpglcte => ["udp-glucuronate",           [15,22,18,2,2]],
#   udprham  => ["udp-rhamnose",              [15,24,16,2,2]],
   udprha   => ["udp-rhamnose",              [15,24,16,2,2]],
   udpxyl   => ["udp-xylose",                [14,22,16,2,2]],
   ump      => ["uridine monophosphate",     [9,13, 9,2,1]],
   x5p      => ["xylulose 5-phosphate",      [5,11,8,0,1]],
   );

my $className = "Cell Wall (CW)";      # The name of the data class.
my $printFile = "CW_Data_Table.txt";   # The name of the info table file.

################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class, $lElement) = @_;
   
   # Call the constructor of the parent class (CompoundClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement);

   bless $self, $class;

   return $self;
}


############################
#   Overridden  methods.   #
############################

################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# compound, including its atomic make up, mass, labeled backbone and full name.
#
# Overridden to specify the output file and title to the parent method.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;
   
   $self->SUPER::PrintInfoTable($printFile);
}

1;