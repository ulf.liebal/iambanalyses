package FluxY_Lib::MS::GenerateOpenFlux;

################################################################################
# This class provides for generating Mass Spectrometry output ready for
# inclusion in OpenFlux MFA models.
#
# This class inherits basic functionality from the Generate parent class,
# initializing it with the model specific headers. It extends the basic Generate
# functionality to add model specific support. As such it implements the
# Generate() and GenerateModels() methods.
#
################################################################################

use strict;
use warnings;


use FluxY_Lib::IO::IO_Utils;   # User Lib - Provides I/O routines.
use FluxY_Lib::MS::Generate;   # User Lib - The common parent class.

our @ISA = qw(FluxY_Lib::MS::Generate);   # Inherits from DataClass


# Section headers for OpenFlux model file:
my @headers = (
      "RxnID",
      "excludedMetabolites",
      "simulatedMDVs",
      "inputSubstrates",
      "measurements",
      "error");

            
################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class) = @_;

   my $self = $class->SUPER::new(\@headers, "OpenFlux");

   bless $self, $class;

   return $self;
}


######################################
#                                    #
#   Implemented DataClass methods.   #
#                                    #
######################################

################################################################################
# Subroutine: Generate
#
# For a given set of measurements and errors, this will print out the 
# measurements and error sections, one per averaged set of data, ready for
# insertion into an OpenFLUX tab seperated network configuration (CSV) file.
#
################################################################################
sub Generate{
   my ($self, $baseName, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS) = @_;
   my @data   = &GenerateData($refIDX, $refRH1, $refAVG, $refFAS);
   my @errors = &GenerateErrors($refIDX, $refRH1, $refSTD, $refFAS);
   
   my @colHeads = @{$refCH};

   for (my $i=0; $i<@data ; $i++) {
      my $name  = $baseName . "-OF_" . $colHeads[$i] . ".txt";
      my @dLines = @{$data[$i]};
      my @eLines = @{$errors[$i]};

      open (FILE, ">" . $name) or FileSystemError(900, $name, $!);
      
      foreach my $line (@dLines) { print FILE $line . "\n"; }
      print FILE "\n";
      foreach my $line (@eLines) { print FILE $line . "\n"; }
      
      close(FILE)              or FileSystemError(901, $name, $!);
   }
}


################################################################################
# Subroutine: GenerateModels
#
# Generates a set of OpenFLUX model files, one for each combination of model
# and measurement.
#
################################################################################
sub GenerateModels{
   my ($self, $refMod, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS, $appendFlag) = @_;

   my @modelFiles = @{$refMod};
   my @colHeads   = @{$refCH};

   my @data   = &GenerateData($refIDX, $refRH1, $refAVG, $refFAS);
   my @errors = &GenerateErrors($refIDX, $refRH1, $refSTD, $refFAS);

   my $measSection = 4;
   my $errSection  = 5;
   foreach my $model (@modelFiles) {
      my ($modelBase, $modelExt) = split (/\./, $model);

      # Read in model file.
      my @mLines = &ReadFile($model);
      
      # Index of start line for each section.
      my @sectionIdx  = $self->GetSectionIndex(@mLines);
      
      # Check for unmatched metabolites in the model file.
      &CheckMetabolites(\@mLines, $sectionIdx[0]+1, $sectionIdx[1], $refRH1);

      
      for (my $i=0; $i<@data; $i++) {
         my $outFile = $modelBase . "-OF_" . $colHeads[$i] . "." . $modelExt;
         my @dLines = @{$data[$i]};
         my @eLines = @{$errors[$i]};
                  
         open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
         
         for (my $i=0; $i<$sectionIdx[$measSection]; $i++) { print FILE $mLines[$i] . "\n"; }

         # If appending, first print out the measurement section and remove the extra heading line.
         if ( $appendFlag ) {
            for (my $i=$sectionIdx[$measSection]; $i<$sectionIdx[$measSection+1]; $i++) { print FILE $mLines[$i] . "\n"; }
         
            shift @dLines;
         } else {
            print FILE "\n";
         }
         
         foreach my $line (@dLines) { print FILE $line . "\n"; }
         print FILE "\n";
         for (my $i=$sectionIdx[$measSection+1]; $i<$sectionIdx[$errSection]; $i++) { print FILE $mLines[$i] . "\n"; }
         
          # If appending, first print out the error section and remove the extra heading line.
         if ( $appendFlag ) {
            for (my $i=$sectionIdx[$errSection]; $i<@mLines; $i++) { print FILE $mLines[$i] . "\n"; }
         
            shift @eLines;
         } else {
            print FILE "\n";
         }
        
         foreach my $line (@eLines) { print FILE $line . "\n"; }
         
         close(FILE)                 or FileSystemError(901, $outFile, $!);
      }
   }
}


#######################
#                     #
#   Public methods.   #
#                     #
#######################

################################################################################
# Subroutine: GenerateData
#
# For a given set of measurements, this will generate and return the
# measurements section(s) ready for insertion into an OpenFLUX tab seperated
# network configuration (CSV) file.
# - The results are returned in a 2D array.
#
################################################################################
sub GenerateData{
   my ($refIDX, $refRH1, $refAVG, $refFAS) = @_;
   my @fIndex   = @{$refIDX};
   my @rowHead1 = @{$refRH1};
   my @averages = @{$refAVG};
   my @atomStrs = @{$refFAS};

   my @data = ();
   for (my $i=0; $i<@averages ; $i++) {            
      my @lines = ("##	measurements"); 

      my @val = @{$averages[$i]};
      for (my $j=0; $j < $#fIndex; $j++) {      
         for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {
            my $diff = $k-$fIndex[$j];
            if ( $diff == 0 ) {
               push(@lines, "#\t" . $val[$k] . "\t" . uc($rowHead1[$k]) . "_" . $atomStrs[$j] . "\tM");
            } else {
               push(@lines, "#\t" . $val[$k] . "\t\tM+" . $diff);
            }
         }
         push(@lines, "");
      }
      
      push(@data, [ @lines ]);
   }
   
   return @data;
}


################################################################################
# Subroutine: GenerateErrors
#
# For a given set of errors, this will generate and return the error section(s)
# ready for insertion into an OpenFLUX tab seperated network configuration (CSV)
# file.
# - The results are returned in a 2D array.
#
################################################################################
sub GenerateErrors{
   my ($refIDX, $refRH1, $refSTD, $refFAS) = @_;
   my @fIndex   = @{$refIDX};
   my @rowHead1 = @{$refRH1};
   my @stdDevs  = @{$refSTD};
   my @atomStrs = @{$refFAS};

   my @errors = ();
   for (my $i=0; $i<@stdDevs ; $i++) {            
      my @lines = ("##	error");

      my @dev = @{$stdDevs[$i]};
      for (my $j=0; $j < $#fIndex; $j++) {      
         for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {
            my $diff = $k-$fIndex[$j];
            if ( $diff == 0 ) {
               push(@lines, "#\t" . $dev[$k]. "\t" . uc($rowHead1[$k]) . "_" . $atomStrs[$j] . "\tM");
            } else {
               push(@lines, "#\t" . $dev[$k] . "\t\tM+" . $diff);
            }
         }
         push(@lines, "");
      }
      
      push(@errors, [ @lines ]);
   }
   
   return @errors;
}


################################################################################
# Subroutine: CheckMetabolites
#
# Checks the metabolite names of the measurement data against those in the
# OpenFlux CSV model file. Prints warning if matching name is not found.
# 
################################################################################
sub CheckMetabolites{
   my ($refLines, $start, $end, $refRH1) = @_;
  
   my @lines   = @{$refLines};
   my @rowHead = @{$refRH1};

   # Get list of metabolites in the model file.
   my @metabs = ();
   for (my $i=$start; $i<$end; $i++) {
      my @list = split('\t', $lines[$i]);
      
      if ( !defined($list[1]) ) { next; }
      if ( $list[1] =~ /^\s*$/ ) { next; }

      my @params = split('[\s=+]', $lines[$i]);      
      for (my $j=0; $j<@params; $j++ ) {
         if ( !defined($params[$j]) ) { next; }

         if ( $params[$j] =~ /^\s*$/ ) { next; }

         my $found = 0;
         foreach my $metab (@metabs) {
            if ( $metab eq $params[$j] ) {
               $found = 1;
               last;
            }
         }
         
         if ( !$found ) { 
            push(@metabs, $params[$j]);
         }
      }
   }         

   
foreach my $metab (@metabs) {
print $metab . "\n";
}

   # Compare measurement names to the metabolite list.
   my @missing = ();
   my $current = "";
   foreach my $name (@rowHead) {
      $name = uc($name);
      
      if ( $current eq $name ) { next; }
      
      $current = $name;
      
      my $found = 0;
      foreach my $metab (@metabs) {
         if ( $name eq $metab ) {
            $found = 1;
            last;
         }
      }
      
      if ( !$found ) { push(@missing, $name); }
   }
   
   if ( @missing > 0 ) {
      print "\n\nINFO:\n=====\n";
      print "\tThe following metabolites are specified in the measurement\n"  .
            "\tsection, but not found in the model stoichiometry specified\n" .
            "\tin the NETWORK section:\n\n";
      foreach my $name (@missing) { print "\t\t" . $name . "\n"; }
      print "\n\tThese metabolite(s) are either named differently in the model,\n"  .
            "\tor they are not used in the model.\n\n" . 
            "\tPlease check the model file(s) before use.\n\n\n";
   }
}

1;