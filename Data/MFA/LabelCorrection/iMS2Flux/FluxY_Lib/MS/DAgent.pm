package FluxY_Lib::MS::DAgent;

################################################################################
# This module provides support for derivatizing agents.
#
# Information about the name and molecular makeup of different derivatizing 
# agents is maintained in three hashes.
# - atomList: each array contains the atoms in the DA molecule, and specifies
# the order in which the counts are provided.
# - dCounts: each array specifies the number of each atom in the DA molecule.
# - dNames: each element is the long form name of the derivatizing agent.
#
################################################################################

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw(GetDAgent GetDAgentList);

# Hash of supported derivatizing agents. For each derivatizing agent there are
# three pieces of information.
# 1. The list of atoms found in each derivatizing agent.
# 2. The atom count for each derivatizing agent. Each atoms' count is for the
#    corresponding atom specified in the atom list.
# 3. The name for each derivatizing agent.
my %dAgentList = (
   tbdms => [['C','H','Si'],    [6,15,1],  "t-butyl-trimethylsilyl"],
   tms   => [['C','H','Si'],    [3,9,1],   "trimethylsilyl"],
   meox  => [['C','H','O','N'], [1,3,1,1], "methoxyamine"],
   ac2o  => [['C','H','O'],     [2,3,1],   "2,3,4,5,6-Penta-O-acytl-D-glucose-O-methyloxime"],
   tfaa  => [['C','H','O','F'], [2,0,1,3], "Acetic acid, trifluoro-,1,2,3-propanetriyl ester"],
   );

################################################################################
# Subroutine: GetDAgent
#
# Returns the name, atom count array and atom list array for the specified
# derivatizing agent.
#
# An optional parameter, $refNAL, may be passed specifying the desired array
# order of the atom count.
#
################################################################################
sub GetDAgent {
   my ($dAgent, $refNAL) = @_;
   
   # Check that the key is specified:
   if ( !(defined $dAgent) ) { return (""); }

   $dAgent = lc($dAgent);
   
   # Check that the specified compound is a valid key:
   if ( !(exists  $dAgentList{$dAgent}) ) { return (""); }
   
   my $atomListPtr  = "";
   my $atomCountPtr = "";
   # Check if a new atom list (NAL) has been provided to prearrange the atom count.
   if ( ref($refNAL) eq 'ARRAY') { 
      $atomListPtr  = $refNAL;
      $atomCountPtr = SortAtomCounts($dAgent, $refNAL);
   } else {
      $atomListPtr  = $dAgentList{$dAgent}[0];
      $atomCountPtr = $dAgentList{$dAgent}[1];   
   }
    
   my $dName        = $dAgentList{$dAgent}[2];

   return ($dName, $atomCountPtr, $atomListPtr);
}


################################################################################
# Subroutine: GetDAgentList
#
# Returns the currently supported list of derivatizing agents.
#
################################################################################
sub GetDAgentList {
   return (sort keys %dAgentList);
}


################################################################################
# Subroutine: SortAtomCounts
#
# Given a list of atom counts and their order, sort the atom counts into the
# same order as the in class atom list.
# - This assumes that all atoms already exist in the in class atom list.
#
################################################################################
sub SortAtomCounts {
   my ($dAgent, $refNAL) = @_;
   
   my @newAtomList = @{ $refNAL };
   my @atomList    = @{ $dAgentList{$dAgent}[0] };
   my @atomCount   = @{ $dAgentList{$dAgent}[1] };
   
   my @newAtomCount = ();
   foreach my $atom (@newAtomList) {
      my $found = 0;
      for (my $i=0; $i<@atomList; $i++) {
         if ( $atom eq $atomList[$i] ) {
            $found = 1;
            push(@newAtomCount, $atomCount[$i]);
            last;
         }
      }
      if ( !$found ) { push(@newAtomCount, 0); }
   }
   
   return \@newAtomCount;
}

1;