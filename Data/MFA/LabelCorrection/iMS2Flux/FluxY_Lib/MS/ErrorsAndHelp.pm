package FluxY_Lib::MS::ErrorsAndHelp;
#################################################################################
# Error and Warning numbers are reserved as follows:
#    000-019 - General errors (ErrorsAndHelp.pm)
#    020-099 - General Warnings (ErrorsAndHelp.pm)
#    100-119 - General errors (ErrorsAndHelp.pm)
#    120-199 - Data Check Errors (Data_Check.pm)
#    200-399 - General errors (ErrorsAndHelp.pm)
#    400-879 - Unassigned
#    880-899 - Import GE Data Class Errors (ImportDataClass.pm)
#    900-999 - IO Errors (IO_Utils.pm)
################################################################################

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw(Error Warning PrintSyntax);

#################################################################################
# Subroutine: Error
#
# Prints out appropriate error message and terminates the program.
#
################################################################################
sub Error {
   my ($errorCode, @values) = @_;
   
   print "\n\n\tError: " . $errorCode . " - called by:\n";

   my $i = 0;
   my @calledBy = ();
   while ( @calledBy = caller($i++) ) {
      print "\tLine " . $calledBy[2] . " - " . $calledBy[3] . "\n";
   }
   print "\n";
   
   if ($errorCode == 10) {
      print "Missing " . $values[2] . " Data Values:";
      print "\n\nThe data file, " . $values[0] .", is missing " . $values[2] . 
            " data entries. Please review and " .
            "\ncorrect this problem before re-running this program." . 
            "\n\nFor the complete list of missing data see the file:" .
            "\n\n\t" . $values[1];
   } elsif ($errorCode == 101) {
      print "The following list of arguments was used: " . @values;
      print "\n\nThe correct syntax is:";
      print "\n\n\t1) iMS2Flux.pl <INPUT_FILE_NAME>";
      print "\n\t2) iMS2Flux.pl -P1";
      print "\n\t3) iMS2Flux.pl -help";
   } elsif ($errorCode == 110) {
      print "The configuration file, " . $values[0] . ", does not exist."; 
   } elsif ($errorCode == 111) {
      print "The configuration file does not have the required number of data lines (" . $values[0] . ")."; 
   } elsif ($errorCode == 112) {
      print "You must specify at least one data file."; 
   } elsif ($errorCode == 113) {
      print "No valid value was found for the number of replicates in the configuration file."; 
   } elsif ($errorCode == 115) {
      print "Inconsistent Flags" . 
            "\n\nThe configuration file has the 'correction for proton loss' flag set, but not the 'M-1 data present' flag. This correction option requires M-1 data."; 
   } elsif ($errorCode == 116) {
      print "Inconsistent Flags" . 
            "\n\nThe configuration file has the 'correction for proton gain' flag set, but not the 'M+1 data present' flag. This correction option requires M+1 data."; 
   } elsif ($errorCode == 117) {
      print "Inconsistent Flags" . 
            "\n\nThe configuration file has both the correction for proton gain and loss flags set. Only one of these corrections may be applied at a time."; 
   } elsif ($errorCode == 119) {
      print "This program cannot perform natural abundance correction for:\n\n\t" . join(", ", @values)
   } elsif ($errorCode == 200) {
      print "The input file, " . $values[0] . " is not a valid " . $values[1] . " summary report."; 
   } elsif ($errorCode == 201) {
      print "The input file, " . $values[0] . " does not contain any data sets."; 
   } elsif ($errorCode == 202) {
      print "Invalid data:" .
            "\n\nThe input file, " . $values[0] . ", has an inconsistent number of samples" .
            "\nstarting with compound " . $values[1] . ".";
   } elsif ($errorCode == 203) {
      print "Invalid data:" .
            "\n\nThe retention time file, " . $values[0] . ", does not contain the same" .
            "\ndimensions as the data file, " . $values[1] . "." . 
            "\n\n The number of " . $values[2] . " (mass readings or samples) are different.";
   } elsif ($errorCode == 205) {
      print "No valid compounds names.";
      print "\n\nNo compounds matched the specified data type, " . $values[0];
      print "\n\n\tCommon causes include when the 'MS Data Type', 'MS Data File Type' or" .
            "\n\t'Derivatizing Agent' is set incorrectly in the configuration file.\n";
   } elsif ($errorCode == 206) {
      print "Inconsistent Starting Fragment mass.";
      print "\n\nTheir is no 'M+0' match for the derivatized compound fragment mass:";
      for(my $i=0; $i<@values; $i+=2) {
         print "\n\t" . $values[$i] . " - " . $values[$i+1];
      }
      print "\n\nYou may print the fragment mass tables (option -P1) to generate the list" . 
            "\nof fragments (and their masses) that this program can identify.";
   } elsif ($errorCode == 215) {
      print "Invalid data:" .
            "\n\nThe number of experimental data sets, " . $values[0] . ", is inconsistent with the number of" .
            "\nreplicates, " . $values[1] . ".";
   } elsif ($errorCode == 216) {
      print "Invalid data:" .
            "\n\nThe number of experimental/replicate headers (" . $values[1] . ") found in, " . $values[0] . "," . 
            "\ndoes not match the expeted (" . ($values[2]/$values[3]). ") number based on:" .
            "\n\n\t\t" . $values[2] . " experimental data sets and " . $values[3] . " replicates";
   } elsif ($errorCode == 221) {
      print "Invalid data:" .
            "\n\nThe original biomass file should have one set of biomass data for each data set." .
            "\nThere are  " . $values[0] . " datasets but " . $values[1] . " bio-mass sets.";
   } elsif ($errorCode == 300) {
      print "Invalid argument:" .
            "\n\nYou have requested to extract the 'M-" . $values[0] . "' mass isotope data," .
            "\nwhile in the configuration file you specified there were only " . $values[1] . 
            "  measurement values preceding the 'M+0' mass isotope.";
   } elsif ($errorCode == 301) {
      print "Invalid argument:" .
            "\n\nYou have requested to extract the 'M+" . $values[0] . "' mass isotope data," .
            "\nwhile in the configuration file you specified there were only " . $values[1] . 
            "  measurement values following the 'M+NumC' mass isotope.";
   } elsif ($errorCode == 400) {
      print "One or more data check flags set (value = " . $values[2] ."):";
      print "\n\nThe data in the file, " . $values[0] .
            ", has thrown one or more data check flags" .
            "\nbased on the parameters you have provided. Please review and if necessary" .
            "\ncorrect any data problem before re-running this program." . 
            "\n\nFor the list of problematic data see the file:" .
            "\n\n\t" . $values[1];
   }

   print "\n\n";
   
   exit -1;
}


################################################################################
# Subroutine: Warning
#
# Generates appropriate warning messages.
#
################################################################################
sub Warning{
   my ($warningCode, @values) = @_;
   
   print "\n\n\tWARNING: (" . $warningCode . ") - ";
   
   if ($warningCode == 20) {
      print "Inconsistent Compound Names.";
      print "\n\nThe following list of compound names does not conform to internal use:\n";
      foreach my $value (@values) {
         print "\n\t\t" . $value;
		   if ( $value =~ s/\s// ) {
            print " - this name contains a space in it, which may \n\t\t\t   be the cause of this problem.";
         }
      }
      print "\n\nThese compounds and all associated data have been automatically removed." . 
            "\nAll remaining compounds will be processed as normal.";
      print "\n\n   You may print the fragment mass tables (option -P1) to generate" . 
            "\n   the list of currently supported fragments (and their naming).";
   } elsif ($warningCode == 25) {
      print "Inconsistent measurement data.\n";
      for(my $i=0; $i<@values; $i+=3) {
         print "\n\t" . $values[$i] . " has " . $values[$i+1] . " labeled atoms, but only has measurements for M+0 to M+" . $values[$i+2] . ".";
      }
      print "\n\n\tCommon causes include when the 'M-1' or 'M+numC+1'" .
            "\n\tis set incorrectly in the configuration file.\n";
   } elsif ($warningCode == 30) {
      print "The specified compound type, " . $values[0] . ", does not make\n\t\t\tuse of a derivatizing agent." .
            "\n\n\tThe derivatizing agent specified in the configuration file, " . $values[1] . ", has\n\tbeen ignored and the rest of the processing will continue as normal." .
            "\n\n\tFor consistency and clarity it is suggested to use the value 'NONE' as\n\tthe derivatizing agent for this compound in the configuration file.\n";
   } elsif ($warningCode == 50) {
      print "Correction for Proton Loss/Gain: (" . $values[0] . ")";
      if ( $values[0] == -1 ) {
         print "Negative scaling parameter, no correction performed.\n";
      } elsif ( $values[0] == -2 ) {
         print "Non-converging scaling parameter, no correction performed.\n";
      } elsif ( $values[0] == -3 ) {
         print "A stable oscilating, non-decreasing scaling parameter was reached, correction was performed.\n";
      } elsif ( $values[0] == -4 ) {
         print "A solution was found with negative metabolites, no correction performed.\n";
      } elsif ( $values[0] == -5 ) {
         print "The scaling factor reached 100%, no correction performed.\n";
      } elsif ( $values[0] == -6 ) {
         print "Maximum correction reached, early termination.\n";
      } elsif ( $values[0] == -7 ) {
         print "Exceeded maximum number of iterations, no correction performed.\n";
      }
      
      print "\t\tExperiment: " . $values[1] . ", fragment: " . $values[2] . "-" . $values[3];
   }
   
   print "\n\n";
}

################################################################################
# Subroutine: PrintSyntax
#
# Prints out the command line syntax (i.e. the help screen).
#
################################################################################
sub PrintSyntax {
   print <<ENDHELP;

DESCRIPTION:
This program automatically corrects Mass Spectrometry (MS) data and outputs it
to a format ready for inclusion in either the Mass Spectrometry or Label 
Measurements section for 13C Flux, the measurement and error sections for 
OpenFLUX, or the model (labelling measurements) and data sections for 13CFLUX2. 
This program currently implements MS corrections for proton loss (M-1), Proton 
Gain (M+numC+1), natural labelling abundance and original bio-mass.

This program requires at least two additional files, the MS data file and a 
configuration file. By default the configuration file is named config.txt, but 
may optionally be given a different (more meaningful) name. The configuration
and data files must be in the same directory, but do not need to be located in
the same directory as this program.

The following describes the various command line forms for using this program:

USAGE:
   iMS2Flux.pl [CONFIGURATION_FILE_NAME] [-Z] [-P2] [-BB #]
   iMS2Flux.pl -P1 XX [YYYY]
   iMS2Flux.pl -I1 DATA_FILE_NAME
   iMS2Flux.pl -help

   [CONFIGURATION_FILE_NAME] is an optional parameter.
         By default the CONFIGURATION_FILE_NAME is "config.txt" and is not 
         required as a parameter to this program. Optionally you may provide 
         the configuration data in a file with a different name, and provide 
         the name of the configuration file to the program.

   -Z    With QL data type only - Overrides default behaviour for missing data
         - prints missing data table, replaces any missing data with zero, and
         continues processing.

   -BB   Specify whether to correct the fragments with a specified percentage 
         of the backbone and is currently limited to carbon labeling. The valid
         range for this value (#), given in decimal form, is from 0 to 1
         inclusive.
         - This option only affects the natural abundance correction.
         - If not specified the default is to correct with backbone carbon.
         - To exclude the backbone carbon set the value to 0.
         - while a fractional value is supported, the usage is currently
         experimental and not normally used. 

   -I1   Generates a new generic data (GE) class based on the specified file.

   -P1   Prints the fragment/compound mass tables and associated data related
         to supported fragments/compounds for the specified compound type (XX),
         and derivatizing agent (YYYY) if necessary.

   -P2   Prints the standard deviations associated with the average labelling 
	      data check. This option is ignored if the optional data check
         is not enabled in the configuration file.

   -help Prints this message.

CONFIGURATION FILE:
   This program requires a configuration file, by default named config.txt. 
Optionally the configuration file may have a different (more meaningful) name.
The format of this file is line oriented, with each line either blank, 
containing a comment or a single configuration entry. There are 33 configuration
lines, all 33 lines are required, even though some of the data is optional.

The rules for each line are described below: 

- Blank lines may be inserted anywhere.
- A line is a comment line if it starts with '//'.
- Comment lines and blank lines are ignored by the program.
- Configuration lines, as specified below, may contain either a single value or
a space seperated list of values. A colon is used to mark the end of descriptive
text (to the left) and the start of configuration data (to the right).

Configuration file structure:
line  1: The name of the data file(s) specified in a space separated list.
line  2: [TSV|QL|MAM]- The data file format.
         - TSV for Tab Separated Values
         - QL for QuanLynx
         - MAM for Mattis Agilent Macros
line  3: [AA|CW|FA|GE|GL|GY|SM]- The type of MS data.
         - AA: Amino Acids (Fragments)
         - CW: Cell Wall (Compounds)
         - FA: Fatty Acids (Full and McLafferty Fragment)
         - GE: Generic Analyte (based on structure of SM)
         - GL: Glucose (single compound with multiple fragments)
         - GY: Glycerol (single compound with multiple fragments)
         - SM: Soluble Metabolite (single compound, single fragment)
line  4: [NONE|TBDMS|TMS|TFAA|Ac2O]- The derivatizing agent.
         - TBDMS: t-butyl-trimethylsilyl (AA)
         - TMS:   trimethylsilyl (GL & GY)
         - TFAA:  Acetic acid, trifluoro-,1,2,3-propanetriyl ester (GY)
         - Ac2O:  2,3,4,5,6-Penta-O-acytl-D-glucose-O-methyloxinme (GL)
         - None:  No derivatizing agent is specified.
line  5: [A|R]- The type of data to process (A for area, R for response).
         - Applies only to data type QL, otherwise either value is OK.
line  6: [number(s) >= 1]- Number of consecutive replicates in the data file.
         - A single number, all experiments have the same number of replicates.
         - A space separated list, one for each experiment.
line  7: [Y|N]- Are you providing the replicates names/titles?
line  8: The name of the file with the replicate names (skipped if line 7 is N).
line  9: [number >= 0]- Number of measurement values preceding 'M+0'.
line 10: [number >= 0]- Number of measurement values post 'M+numC'.
line 11: [number >= 0]- The detector limit threshold (check skipped if 0).
line 12: [number >= 0]- The poor peak quality threshold (check skipped if 0).
line 13: [number >= 0]- The retention time threshold (check skipped if 0).
line 14: [Y|N] - Are you providing retention times in a separate file?
line 15: The name of the file with the retention times (skipped if line 14 is N).
line 16: [number 0-1]- The data value threshold (check skipped if 0).
line 17: [Y|N]- Generate the Average Labelling summary.
line 18: [Y|N]- Correct for proton loss, requires M-1 data measurements.
line 19: [Y|N]- Correct for proton gain, requires M+numC+1 data measurements.
line 20: [Y|N]- Correct for natural abundance.
line 21: [Y|N]- Correct for original bio-mass.
line 22: The name of the file with the original bio-mass percentages.
         (skipped if line 22 is N).
line 23: [Y|N]- Print the retention time matrix.
line 24: [Y|N]- Print the unprocessed (uncorrected data).
         For QL input also supports [A] - area, [R] - response and [Y|B] - both.
line 25: [Y|N]- Print the processed (corrected/scaled) data.
line 26: [Y|N]- Print the corrected data averaged over the number of replicates.
line 27: [Y|N]- Print the standard deviation over the number of replicates.
line 28: [Y|N]- Generate model data for supported MFA software.
line 29: [MS|LM|OF|C2]- Which type of data should be generated?
         - MS: 13CFLUX, MASS_SPECTROMETRY section.
         - LM: 13CFLUX, LABEL_MEASUREMENTS section.
         - OF: OpenFlux, measurement and error sections.
         - C2: 13CFLUX2, model (labelling measurements) and data sections.
line 30: [Y|N]- Combine the measurement data with one or more models?
line 31: [Y|N]- Append the data to existing measurement data, (default replace).
line 32: The name of the model file(s) specified in a space separated list.
         (skipped if line 31 is N).
line 33: [number >= 0]- The additive offset for use when calculating std. dev.
         (skipped if line 31 is N).
ENDHELP
}

1;
