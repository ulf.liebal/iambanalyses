package FluxY_Lib::MS::CorrectFragment;

################################################################################
# This module provides the subroutines involved in correcting MS data.
# - Exclusively called within the 'CorrectData' subroutine of the main program. 
# - Each subroutine works on one compound fragment.
#
# The subroutines are:
# - ThresholdFragment(): Preprocess a fragment, applying a threshold percentage.
# - CorrectNA(): The main correction method, corrects for Natural Abundance.
# - CorrectOB(): Corrects for original bio-mass, requires OBM information.
# - CorrectPL(): Corrects for a single proton loss, requires an M-1 data value.
# - CorrectPG(): Corrects for a single proton gain, requires an M+numC+1 data
#                value.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Chemistry::atomInfo; # User Lib - Natural Abundance Probability.
use FluxY_Lib::Data::Data_Check;    # User Lib - Basic data check functions.
use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple math functions.
use FluxY_Lib::MS::CalcNA_Prob;     # User Lib - Calculates probability of natural
                                  #            abundance for 'm' labels in 'n' atoms.
use Math::MatrixReal;             # Std. Lib - Matrix Math Library.

use base 'Exporter';

our @EXPORT = qw(CorrectNA CorrectOB CorrectPL CorrectPG ThresholdFragment);

################################################################################
# Subroutine: ThresholdFragment
#
# Checks each fragment to see if any values are below a threshold percentage of
# the total fragment count. If so, that value is preset to zero.
# If proton loss/gain is being considerred, the extra value is valid and is
# included in the fragment count and is checked against the threshold.
#
################################################################################
sub ThresholdFragment{
   my ($threshold, $extraValue, @fragment) = @_;

   my $sum = $extraValue;
   for(my $i=0; $i<@fragment; $i++) {
      $sum += $fragment[$i];
   }
   
   $sum *= $threshold;
   for(my $i=0; $i<@fragment; $i++) {
      if ( $fragment[$i] < $sum ) { $fragment[$i] = 0; }
   }
   
   if ( $extraValue < $sum ) { $extraValue = 0; }

   return ($extraValue, @fragment);
}


################################################################################
# Subroutine: CorrectPL
#
# Correct the fragment list for proton loss, based on an M-1 measurement.
# This correction requires finding a valid scaling factor, alpha, that satisfies
# the system of equations.
# The iterative method used to solve for alpha requires that through each 
# iteration alpha be positive and approach its true value (i.e. its limit). This
# means that the absolute value of the difference between successive values of 
# alpha should be decreasing.
#
# A negative or non-converging value of alpha results in no correction.
# An stable oscillating value of alpha yields a correction with a warning.
# A preset maximum number of iterations is used as a failsafe. If reached a
# warning is issued, and no correction takes place.
#
################################################################################
sub CorrectPL{
   my ($m_1, @fragList) = @_;
   
   my $alpha = 0.05;
   my $diff  = 100;
   my $limit = 0.000001;

   my @newList  = ();
   my $negCount = 0;
   my $maxIters = 10000;
   for (my $j=0; $j<$maxIters; $j++) {
      my $alpha0 = $alpha;
      my $diff0  = $diff;

      if ($alpha == 1) { return (-5, @fragList); }                   # Alpha has reached 100%.
   
      $newList[-1] = $fragList[1] / (1-$alpha);
      
      for (my $i=($#fragList-1); $i>=0; $i--) {
         $newList[$i] = ($fragList[$i] - ($alpha*$newList[$i+1])) / (1-$alpha);
      }
      
      if ($newList[0] == 0) { return (-6, @fragList); }              # Cannot update alpha.
      
      # Update and check alpha.
      $alpha = $m_1 / ($m_1 + $newList[0]);
      if ( $alpha < 0 ) { return (-1, @fragList); }                  # Negative factor.
      if ( abs($alpha-$alpha0) > $diff ) { return (-2, @fragList); } # non-converging factor.
      
      # Update and check difference.
      $diff = abs($alpha-$alpha0);
      if ( $diff == $diff0 ) {                                       # Stable oscilating, non-decreasing.
         my $negFlag = &CheckForNegative(@newList);

         if ( $negFlag ) {
            $negCount++;
         } else {
            return (-3, @newList);
         }
      } 
      if ($diff < $limit) {                                          # Solution reached.
         my $negFlag = &CheckForNegative(@newList);
         
         if ( $negFlag ) {
            $negCount++;
         } else {
            return (0, @newList);
         }
      }
      
      if ( $negCount > 2 ) { return (-4, @fragList); }               # Stable negative solution.
   }
   
   return (-7, @fragList);
}


################################################################################
# Subroutine: CorrectPG
#
# Correct the fragment list for proton gain, based on M+numC+1 measurement.
# This correction requires finding a valid scaling factor, alpha, that satisfies
# the system of equations.
# The iterative method used to solve for alpha requires that through each 
# iteration alpha be positive and approach its true value (i.e. its limit). This
# means that the absolute value of the difference between successive values of 
# alpha should be decreasing.
#
# A negative or non-converging value of alpha results in no correction.
# An stable oscillating value of alpha yields a correction with a warning.
# A preset maximum number of iterations is used as a failsafe. If reached a
# warning is issued, and no correction takes place.
#
################################################################################
sub CorrectPG{
   my ($mp1, @fragList) = @_;
	
   my $alpha = 0.05;
   my $diff  = 100;
   my $limit = 0.000001;

   my @newList  = ();
   my $negCount = 0;
   my $maxIters = 10000;
   for (my $j=0; $j<$maxIters; $j++) {
      my $alpha0 = $alpha;
      my $diff0  = $diff;

      if ($alpha == 1) { return (-5, @fragList); }                   # Alpha has reached 100%.

      $newList[0] = $fragList[0] / (1-$alpha);
      
      for (my $i=1; $i<@fragList; $i++) {
         $newList[$i] = ($fragList[$i] - ($alpha*$newList[$i-1])) / (1-$alpha);
      }
      
      if ($newList[-1] == 0) { return (-6, @fragList); }             # Cannot update alpha.

      # Update and check alpha.
      $alpha = $mp1 / ($mp1+$newList[-1]);
      if ( $alpha < 0 ) { return (-1, @fragList); }                  # Negative factor.
      if ( abs($alpha-$alpha0) > $diff ) { return (-2, @fragList); } # Non-converging factor.
      
      # Update and check difference.
      $diff = abs($alpha-$alpha0);
      if ( $diff == $diff0 ) {                                       # Stable oscilating, non-decreasing.
         my $negFlag = &CheckForNegative(@newList);

         if ( $negFlag ) {
            $negCount++;
         } else {
            return (-3, @newList);
         }
      } 
      if ($diff < $limit) {                                          # Solution reached.
         my $negFlag = &CheckForNegative(@newList);
         
         if ( $negFlag ) {
            $negCount++;
         } else {
            return (0, @newList);
         }
      }
      
      if ( $negCount > 2 ) { return (-4, @fragList); }               # Stable negative solution.
   }
   
   return (-7, @fragList);
}

################################################################################
# Subroutine: CheckForNegative
#
# Given a list of values, returns true if any one of the values is negative.
#
################################################################################
sub CheckForNegative{
   my @list = @_;
	
   # Check for significant negative values.
   foreach my $val (@list) {
      if ( $val < 0 ) { return 1; }
   }
   
   return 0;
}


################################################################################
# Subroutine: CorrectOB
#
# Correct the fragment list for original Bio-mass.
# For a given fragment, the probability of each mass isotopomer naturally 
# occuring (based on only the isotopes of the specified atom of interest, by 
# default Carbon) is calculated (using only the first two stable isotopes) and
# used to remove the corresponding portion of the original bio-mass.
# - The corrected data is scaled to 100%.
#
################################################################################
sub CorrectOB{
   my ($numC, $ob, $refFragList, $atomType) = @_;

   my @fragList = @{$refFragList};
	
	if ( !defined($atomType) ) { $atomType = "C"; }

   my @probTable = @{ &GenerateProbTable2($atomType) };
   my @Probs = @{$probTable[0]};

   for (my $i=0; $i<@fragList; $i++) {
      $fragList[$i] -= (&Binomial($numC, $i)*($Probs[0]**($numC-$i))*($Probs[1]**($i))*$ob*$fragList[$i]);
      
      if ( $fragList[$i]<0 ) { 
         # If very negative, say more than 1%, add warning indicate relative mass???
         $fragList[$i] = 0;
      }
   }

   return &Scale(@fragList);
}


################################################################################
# Subroutine: CorrectNA
#
# Corrects the measured data for natural abundance of labelled material.
# The current implementation accounts for the natural occurance of M+0, M+1 and
# M+2 isotopes of C, H, O, N, Si and S. in each fragment. For additional details
# see the included modules: AA_Data_Utils.pm and CalcNA_Prob.pm.
# - The corrected data is scaled to 100%.
#
################################################################################
sub CorrectNA {
   my ($compoundName, $compoundMass, $msType, $refFragList, $correctBackbone, $backboneAtom) = @_;

   my @fragList    = @{$refFragList};
   	
   my $size = @fragList;
   my $Vfs  = new Math::MatrixReal($size,1);
   my $Vcor = new Math::MatrixReal($size,1);
   my $Mcor = new Math::MatrixReal($size,$size);
   my $Minv = new Math::MatrixReal($size,$size);

   # Initialize the scaled fragment Vector.
   my $i;
   for($i=0; $i<@fragList; $i++) {
       $Vfs->assign($i+1,1,$fragList[$i]);
   }
   
   # Generate the Correction Matrix.
   $Mcor = &GenerateCorrectionMatrix($size, $compoundName, $compoundMass, $Mcor, $msType, $correctBackbone, $backboneAtom);
	
   # Calculate the corrected abundace vector.
   $Minv = $Mcor->inverse;
   $Vcor = $Minv * $Vfs;

   # Build the list of corrected abundances.
   my @corList = ();
   for($i=1; $i<=@fragList; $i++) {
      push(@corList, 0);
      
      # Check the scaled fragment value is >0 before & after correction.
      if ( ($Vfs->element($i,1) > 0) && ($Vcor->element($i,1) > 0) ) {
         $corList[$i-1] = $Vcor->element($i,1);
      }
   }

   #Scale the corrected abundances.   
   return &Scale(@corList);
}


################################################################################
# Subroutine: GenerateCorrectionMatrix
#
# Generates the full correction matrix from the individual atom correction 
# matrices / natural abundance probabilities.
#
################################################################################
sub GenerateCorrectionMatrix{
   my ($size, $compoundName, $compoundMass, $Mcor, $msType, $correctBackbone, $backboneAtom) = @_;
   
	if ( !defined($correctBackbone) ) { $correctBackbone = 1; }
	if ( !defined($backboneAtom) )    { $backboneAtom = "C"; }
	
   $Mcor **= 0;   # Initialize to identity matrix.

   my @atomCounts = $msType->GetTotalAtomCount($compoundName, $msType->Mass2Frag($compoundName, $compoundMass));
   
	# The user may specify a percentage of the labelled atom from the backbone 
	# to correct for natural abundance. At 100% no modification is required, 
	# otherwise the fraction specified to not correct will be subtracted from the
	# number of the specified atom before correction.
	# Typically this is a binary choice, i.e. 0% or 100%, however this 
	# implementation allows for future flexability, possibly to account for
	# labelling experiments using less than 100% labeling in combination with 
	# other correction methods.
	if ( $correctBackbone != 1 ) {
		$atomCounts[$msType->GetElementPosition($backboneAtom)] -= (1 - $correctBackbone)*($size-1)
	}
	
   my @probTable = @{ &GenerateProbTable2($msType->GetAtomList) };
   
   for(my $i=0; $i < @atomCounts; $i++) {

   #1. Calculate the probability of natural abundance for m labels in n atoms,
   #   from unlabelled (M+0) to the desired degree of labelling (M+size-1).
      my @Vprob = ();
      for(my $j=0; $j < $size; $j++) {
         push(@Vprob, CalcNA_Prob($atomCounts[$i],$j, $probTable[$i]));
      }
      
   #2. From each correction vector calculate the SQ/LD atom correction matrix.
      my $Matom = new Math::MatrixReal($size,$size);
      for(my $col=1; $col <= $size; $col++) {
         for(my $row=1; $row <= $size; $row++) {
            if ($row >= $col) {
               $Matom->assign($row,$col,$Vprob[$row-$col]);
            }
         }
      }

   #3. Calculate the total correction matrix.
      $Mcor *= $Matom;
   }
   
   return $Mcor;
}


1;
