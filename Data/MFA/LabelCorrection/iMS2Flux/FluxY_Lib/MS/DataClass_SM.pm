package FluxY_Lib::MS::DataClass_SM;

################################################################################
# This class provides support for Soluble Metabolites.
# - This class represents an explicit set of derivatized fragments, listed
#   uniquely in the compounds list, with derivatizing information directly
#   integrated into the atom count. Additionally a third parameter is added to
#   each compound to directly specify the first and last position of the carbon
#   in the fragmented backbone.
# - Although a derivatized class, by explicitly listing the complete fragment
#   information it is implemented with the simpler compound class.
# - The class data is specific to a carbon labelling experiment.
#
# This class inherits basic functionality from the CompoundClass parent class,
# initializing it with the compound name, soluble metabolite compounds, and the 
# atom list, which are defined by this class.
# - It overrides the GetNumFragmentedLabeledElement(), GetNumFragmentedCarbons(),
#   GetLabeledPositionString() and PrintInfoTable() methods to make use of the
#   specified first and last backbone positions in each compound array.
# - It overrides the GetNumLabeledElement() method to make use of the specified
#   number of labeled elements in the backbone of the unfragmented compound.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::CompoundClass);   # Sets the parent class.


# List of the atoms found in the soluble metabolites and subsequently considerred
# for natural abundance. 
my @atomList = ('C','H','O','N','S','Si','P');

# Hash of compound data, (first two array elements are required).
# 1. The compound names (Soluable Metabolites), in lower case (for standard comparison).
# 2. The atom count for the soluable metabolite.
#    - Each of the atom counts is an array of the number of each corresponding
#    atom in the atomList, i.e. C, H, 0, N, S, Si and P.
# 3. The starting and ending carbon position in the fragment backbone.
# 4. The number of carbon atoms in the backbone of the unfragmented compound.
# NOTE: The 4'th field is used to support Generate13C.pm (LM) and need to check
#       values, especially sucrose.
my %compounds = (
	ala3_b1 => ["alanine_beta-_(3TMS)",          [ 7,20,0,1,0,2,0], [3,3], 3],
	ala3_b2 => ["alanine_beta-_(3TMS)",          [11,28,2,1,0,3,0], [1,3], 3],
	ala2    => ["alanine_(2TMS)",                [ 5,14,0,1,0,1,0], [2,3], 3],
	ala3_b  => ["alanine_(3TMS)",                [11,28,2,1,0,3,0], [1,3], 3],
	asp3_1  => ["aspartic_acid_(3TMS)",          [ 8,20,2,1,0,2,0], [1,2], 4],
	asp3_2  => ["aspartic_acid_(3TMS)",          [ 9,22,2,1,0,2,0], [2,4], 4],
	but_1   => ["butyric_acid,_4-amino-_(3TMS)", [ 7,20,0,1,0,2,0], [4,4], 4],
	but_2   => ["butyric_acid,_4-amino-_(3TMS)", [12,30,2,1,0,3,0], [1,4], 4],
	glu_1   => ["glutamic_acid_(3TMS)",          [ 4, 6,1,1,0,0,0], [2,5], 5],
	glu_1   => ["glutamic_acid_(3TMS)",          [10,24,2,1,0,2,0], [2,5], 5],
	glu_1   => ["glutamic_acid_(3TMS)",          [13,30,4,1,0,3,0], [1,5], 5],
	gly2_1  => ["glycine_(2TMS)",                [ 4,12,0,1,0,1,0], [2,2], 2],
	gly2_2  => ["glycine_(2TMS)",                [ 7,18,2,1,0,2,0], [1,2], 2],
	gly3_1  => ["glycine_(3TMS)",                [ 7,20,0,1,0,2,0], [2,2], 2],
	gly3_2  => ["glycine_(3TMS)",                [10,26,2,1,0,3,0], [1,2], 2],
	his3_1  => ["histidine_(3TMS)",              [ 8,20,2,1,0,2,0], [1,2], 6],
	his3_2  => ["histidine_(3TMS)",              [11,24,0,3,0,2,0], [2,6], 6],
	ile2_1  => ["isoleucine_(2TMS)",             [ 8,20,0,1,0,1,0], [2,6], 6],
	ile2_2  => ["isoleucine_(2TMS)",             [ 8,20,2,1,0,2,0], [1,2], 6],
	leu2_1  => ["leucine_(2TMS)",                [ 8,20,0,1,0,1,0], [2,6], 6],
	leu2_2  => ["leucine_(2TMS)",                [ 8,20,2,1,0,2,0], [1,2], 6],
	lys4_1  => ["lysine_(4TMS)",                 [ 7,20,0,1,0,2,0], [6,6], 6],
	lys4_2  => ["lysine_(4TMS)",                 [14,37,0,2,0,3,0], [2,6], 6],
	met2    => ["methionine_(2TMS)",             [ 7,18,0,1,1,1,0], [2,5], 5],
	orn3_1  => ["ornithine_(3TMS)",              [ 4,12,0,1,0,1,0], [5,5], 5],
	orn3_2  => ["ornithine_(3TMS)",              [14,36,2,2,0,3,0], [1,5], 5],
	orn4_1  => ["ornithine_(4TMS)",              [ 7,20,0,1,0,2,0], [5,5], 5],
	orn4_2  => ["ornithine_(4TMS)",              [13,35,0,2,0,3,0], [2,5], 5],
	phe2_1  => ["phenylalanine_(2TMS)",          [ 7, 7,0,0,0,0,0], [3,9], 9],
	phe2_2  => ["phenylalanine_(2TMS)",          [11,18,0,1,0,1,0], [2,9], 9],
	phe2_3  => ["phenylalanine_(2TMS)",          [ 8,20,2,1,0,2,0], [1,2], 9],
	pro2    => ["proline_(2TMS)",                [ 7,16,0,1,0,1,0], [2,5], 5],
	ser3_1  => ["serine_(3TMS)",                 [ 8,22,1,1,0,2,0], [2,3], 3],
	ser3_2  => ["serine_(3TMS)",                 [ 8,20,2,1,0,2,0], [1,2], 3],
	ser3_3  => ["serine_(3TMS)",                 [11,28,3,1,0,3,0], [1,3], 3],
	thr3_1  => ["threonine_(3TMS)",              [ 5,13,1,0,0,1,0], [3,4], 4],
	thr3_2  => ["threonine_(3TMS)",              [12,30,3,1,0,3,0], [1,4], 4],
	try3_1  => ["tryptophan_(3TMS)",             [12,16,0,1,0,1,0], [3,11], 11],
	try3_2  => ["tryptophan_(3TMS)",             [ 8,20,2,1,0,2,0], [1,2], 11],
	tyr3_1  => ["tyrosine_(3TMS)",               [10,15,1,0,0,1,0], [3,9], 9],
	tyr3_2  => ["tyrosine_(3TMS)",               [ 8,20,2,1,0,2,0], [1,2], 9],
	tyr3_3  => ["tyrosine_(3TMS)",               [14,26,1,1,0,2,0], [2,9], 9],
	val2_1  => ["valine_(2TMS)",                 [ 7,18,0,1,0,1,0], [2,5], 5],
	val2_2  => ["valine_(2TMS)",                 [ 8,20,2,1,0,2,0], [1,2], 5],
	ca4_1   => ["citric_acid_(4TMS)",            [14,27,6,0,0,3,0], [1,6], 6],
	ca4_2   => ["citric_acid_(4TMS)",            [17,37,7,0,0,4,0], [1,6], 6],
	ica4_1  => ["iso-citric_acid_(4TMS)",        [14,27,6,0,0,3,0], [1,6], 6],
	ica4_2  => ["iso-citric_acid_(4TMS)",        [17,37,7,0,0,4,0], [1,6], 6],
	fa2     => ["fumaric_acid_(2TMS)",           [ 9,17,4,0,0,2,0], [1,4], 4],
	ga2     => ["glutaric_acid,_2-oxo-_(2TMS)",  [11,22,5,1,0,2,0], [1,5], 5],
	gla3    => ["glyceric_acid_(3TMS)",          [11,27,4,0,0,3,0], [1,3], 3],
	gla2    => ["glycolic_acid_(2TMS)",          [ 7,17,3,0,0,2,0], [1,2], 3],
	la2     => ["lactic_acid_(2TMS)",            [ 8,19,3,0,0,2,0], [1,3], 3],
	ma3_1   => ["malic_acid_(3TMS)",             [ 9,17,4,0,0,2,0], [1,4], 4],
	ma3_2   => ["malic_acid_(3TMS)",             [12,27,5,0,0,3,0], [1,4], 4],
	sa2_1   => ["succinic_acid_(2TMS)",          [ 7,12,3,0,0,1,0], [1,4], 4],
	sa2_2   => ["succinic_acid_(2TMS)",          [ 9,19,4,0,0,2,0], [1,4], 4],
	fru5_1  => ["fructose_(5TMS)",               [ 4,11,1,0,0,1,0], [6,6], 6],
	fru5_2  => ["fructose_(5TMS)",               [ 8,21,2,0,0,2,0], [5,6], 6],
	fru5_3  => ["fructose_(5TMS)",               [ 9,21,2,0,0,2,0], [4,6], 6],
	fru5_4  => ["fructose_(5TMS)",               [10,24,3,1,0,2,0], [1,3], 6],
	fru5_5  => ["fructose_(5TMS)",               [12,31,3,0,0,3,0], [4,6], 6],
	fru5_6  => ["fructose_(5TMS)",               [14,34,4,1,0,3,0], [1,4], 6],
	glyc3_1 => ["glycerol_(3TMS)",               [12,32,3,0,0,3,0], [1,3], 3],
	glyc3_2 => ["glycerol_(3TMS)",               [11,29,3,0,0,3,0], [1,3], 3],
	glyc3_3 => ["glycerol_(3TMS)",               [ 9,22,2,0,0,2,0], [1,3], 3],
	glc5_1  => ["glucose_(5TMS)",                [ 4,11,1,0,0,1,0], [6,6], 6],
	glc5_2  => ["glucose_(5TMS)",                [ 6,14,2,1,0,1,0], [1,2], 6],
	glc5_3  => ["glucose_(5TMS)",                [ 8,21,2,0,0,2,0], [5,6], 6],
	glc5_4  => ["glucose_(5TMS)",                [ 9,21,2,0,0,2,0], [4,6], 6],
	glc5_5  => ["glucose_(5TMS)",                [10,24,3,1,0,2,0], [1,3], 6],
	glc5_6  => ["glucose_(5TMS)",                [13,31,3,0,0,3,0], [3,6], 6],
	glc5_7  => ["glucose_(5TMS)",                [14,34,4,1,0,3,0], [1,4], 6],
	suc8_1  => ["sucrose_(8TMS)",                [ 4,11,1,0,0,1,0], [6,6], 12],
	suc8_2  => ["sucrose_(8TMS)",                [ 9,21,2,0,0,2,0], [4,6], 12],
	suc8_3  => ["sucrose_(8TMS)",                [13,31,3,0,0,3,0], [3,6], 12],
	suc8_4  => ["sucrose_(8TMS)",                [15,33,4,0,0,3,0], [1,6], 12],
	suc8_5  => ["sucrose_(8TMS)",                [18,43,5,0,0,4,0], [1,6], 12],
	sorb_1  => ["sorbitol_(6TMS)",               [ 4,11,1,0,0,1,0], [6,6], 6],
	sorb_3  => ["sorbitol_(6TMS)",               [ 8,21,2,0,0,2,0], [5,6], 6],
	sorb_4  => ["sorbitol_(6TMS)",               [ 9,21,2,0,0,2,0], [4,6], 6],
	sorb_6  => ["sorbitol_(6TMS)",               [13,31,3,0,0,3,0], [3,6], 6],
	fru6p6  => ["fructose-6-phosphate_(6TMS)",   [ 9,21,2,0,0,2,0], [4,6], 6],
	glc6p6_1 => ["glucose-6-phosphate_(6TMS)",   [ 4,11,1,0,0,1,0], [6,6], 6],
	glc6p6_2 => ["glucose-6-phosphate_(6TMS)",   [ 6,14,2,1,0,1,0], [1,2], 6],
	glc6p6_3 => ["glucose-6-phosphate_(6TMS)",   [ 9,21,2,0,0,2,0], [4,6], 6],	
	hex6p6_1 => ["hexose-phosphate_(6TMS)",      [ 4,11,1,0,0,1,0], [6,6], 6],
	hex6p6_2 => ["hexose-phosphate_(6TMS)",      [ 6,14,2,1,0,1,0], [1,2], 6],
	hex6p6_3 => ["hexose-phosphate_(6TMS)",      [ 9,21,2,0,0,2,0], [4,6], 6],
	pyr3	  => ["pyruvic_acid_(3tms)",           [ 6,12,3,1,0,1,0], [1,3], 3],
	ga3p4_1 => ["glyceric_acid-3-phosphate_(4TMS)", [9,21,2,0,0,2,0], [1,3], 3],
	ga3p4_2 => ["glyceric_acid-3-phosphate_(4TMS)", [14,36,7,0,0,4,1], [1,3], 3],
	pep3_1  => ["phosphoenolpyruvate_(3TMS)",    [ 9,21,2,0,0,2,0], [1,3], 3],
	pep3_2  => ["phosphoenolpyruvate_(3TMS)",    [11,26,6,0,0,3,1], [1,3], 3],
   );

my $lElement  = "C";                          # The labeled element.
my $className = "Soluble Metabolites (SM)";   # The name of the data class.
my $printFile = "SM_Data_Table.txt";          # The name of the info table file.

   
################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class) = @_;
   
   # Call the constructor of the parent class (CompoundClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement);
   
   bless $self, $class;

   return $self;
}


############################
#   Overridden  methods.   #
############################

################################################################################
# Subroutine: GetNumLabeledElement
#
# Returns the number of labeled (carbon) backbone atoms for the specified
# (unfragmented) compound.
#
# Overridden to use the number of labeled backbone atoms specified in each
# compound array.
#
################################################################################
sub GetNumLabeledElement {
   my ($self, $compound, $element) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   return ${ $self->{_compounds} }{$compound}[3];
}


################################################################################
# Subroutine: GetNumFragmentedLabeledElement
#
# Returns the number of fragmented backbone atoms for the labeled element of the
# specified compound and fragment.
# - If the element is not found in the atom list, returns the flag: -1.
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedLabeledElement {
   my ($self, $compound) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   my ($first,$last) = @{${ $self->{_compounds} }{$compound}[2]};

   return ($last - $first + 1);
}


################################################################################
# Subroutine: GetNumFragmentedCarbons
#
# Returns the number of backbone carbon atoms for the specified compound and
# fragment. This is used for the OBM correction.
# - In this carbon labeled class it returns the same as
# GetNumFragmentedLabeledElement().
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedCarbons{
   my ($self, $compound) = @_;

   return ( $self->GetNumFragmentedLabeledElement($compound) );
}


################################################################################
# Subroutine: GetLabeledPositionString
#
# Returns the relative positional string of the labeled (carbon) element for the
# given compound.
#
# Overridden to use the first and last position of the labeled element in the 
# fragment bacbone, as specified in each compound array.
#
################################################################################
sub GetLabeledPositionString {
   my ($self, $compound) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   my ($first,$last) = @{${ $self->{_compounds} }{$compound}[2]};
	   
   return($self->GetPositionString($first, $last));
}


################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# compound, including its atomic make up, mass, labeled backbone and full name.
#
# Overridden to use the first and last position of the labeled element in the 
# fragment bacbone, as specified in each compound array.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;

   open (FILE, ">" . $printFile) or FileSystemError(900, $printFile, $!);

   print FILE $self->GetDataType . " Data Table:\n\n\t";

   print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\tFull Name\n";
  
   foreach my $compound ($self->GetCompoundList) {   
      my $fullName = $self->GetCompoundName($compound);
      my @totAtoms = $self->GetTotalAtomCount($compound);
      my $mass     = $self->GetMass($compound);
		my ($first, $last) = @{${ $self->{_compounds} }{$compound}[2]};

      print FILE $compound . "\t\t" .  join("\t", @totAtoms) . "\t" . $mass . "\t" . $first . "-" . $last . "\t" . $fullName . "\n";
   }
   
   close(FILE)                 or FileSystemError(901, $printFile, $!);
}

1;