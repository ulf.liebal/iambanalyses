package FluxY_Lib::MS::DataClass_GE;

################################################################################
# This class provides support for Generic Analytes.
# - This class represents an explicit set of derivatized fragments, listed
#   uniquely in the compounds list, with derivatizing information directly
#   integrated into the atom count. Additionally a third parameter is added to
#   each compound to directly specify the first and last position of the labeled
#   element for the fragmented backbone, a fourth to specify the number of
#   labeled elements in the unfragmented backbone and lastly the number of
#   carbons in the fragmented backbone.
# - The last item is redundant, but still required if the labeling is carbon.
# - Although a derivatized class, by explicitly listing the complete fragment
#   information it is implemented with the simpler compound class.
#
# This class inherits basic functionality from the CompoundClass parent class,
# initializing it with the compound name, the compounds table, the atom list and
# the specific labeling element, as defined by this class.
# - It overrides the GetNumFragmentedLabeledElement(), GetLabeledPositionString()
#   and PrintInfoTable() methods to make use of the specified first and last
#   labeled backbone positions in each compound array.
# - It overrides the GetNumLabeledElement() method to make use of the specified
#   number of labeled elements in the backbone of the unfragmented compound.
# - It overrides the GetNumFragmentedCarbons() method to make use of the 
#   specified number of backbone carbons in each fragment.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::CompoundClass);   # Sets the parent class.


# List of the atoms found in the starch monomer and subsequently considerred for
# natural abundance.
my @atomList = ('C','H','O','N','S','SI','P');

# Hash of compound data, (first two array elements are required).
# 1. The compound names (Soluable Metabolites), in lower case (for standard comparison).
# 2. The atom count for the soluable metabolite.
#    - Each of the atom counts is an array of the number of each corresponding
#    atom in the atomList, i.e. C, H, 0, N, S, Si and P.
# 3. The starting and ending position of the labeled element in the fragment backbone.
# 4. The number of labeled atoms in the backbone of the unfragmented compound.
# 5. The number of carbon atoms in the backbone of the fragmented compound.
# NOTE: The 3'rd field is used to support Generate.pm child classes.
# NOTE: The 4'th field is used to support Generate13C.pm (LM - cumomers).
# NOTE: The 5'th field is used to support the OBM correction.
my %compounds = (
   ala2 => ["alanine_(2TMS)", [5,14,0,1,0,1,0], [2,3], 3, 2],
   ala3_b => ["alanine_(3TMS)", [11,28,2,1,0,3,0], [1,3], 3, 3],
   ala3_b1 => ["alanine_beta-_(3TMS)", [7,20,0,1,0,2,0], [3,3], 3, 1],
   ala3_b2 => ["alanine_beta-_(3TMS)", [11,28,2,1,0,3,0], [1,3], 3, 3],
   );

my $lElement  = "C";   # The labeling element used in the experiment.
my $className = "Soluble Metabolites";   # The name of the data class.
my $printFile = "GE-SM_Data_Table.txt";   # The name of the info table file.


################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class) = @_;
   
   # Call the constructor of the parent class (CompoundClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement);
   
   bless $self, $class;

   return $self;
}


############################
#   Overridden  methods.   #
############################

################################################################################
# Subroutine: GetNumLabeledElement
#
# Returns the number of labeled backbone atoms for the specified (unfragmented)
# compound.
#
# Overridden to use the number of labeled backbone atoms specified in each
# compound array.
#
################################################################################
sub GetNumLabeledElement {
   my ($self, $compound, $element) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   return ${ $self->{_compounds} }{$compound}[3];
}


################################################################################
# Subroutine: GetNumFragmentedLabeledElement
#
# Returns the number of fragmented backbone atoms for the labeled element of the
# specified compound and fragment.
# - If the element is not found in the atom list, returns the flag: -1.
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedLabeledElement {
   my ($self, $compound) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   my ($first,$last) = @{${ $self->{_compounds} }{$compound}[2]};

   return ($last - $first + 1);
}


################################################################################
# Subroutine: GetNumFragmentedCarbons
#
# Returns the number of backbone carbon atoms for the specified compound and
# fragment. This is used for the OBM correction.
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedCarbons{
   my ($self, $compound) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   return ${ $self->{_compounds} }{$compound}[4];
}


################################################################################
# Subroutine: GetLabeledPositionString
#
# Returns the relative positional string of the labeled element for the given
# compound.
#
# Overridden to use the first and last position of the labeled element in the 
# fragment bacbone, as specified in each compound array.
#
################################################################################
sub GetLabeledPositionString {
   my ($self, $compound) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }
   
   my ($first,$last) = @{${ $self->{_compounds} }{$compound}[2]};
	   
   return($self->GetPositionString($first, $last));
}


################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# compound, including its atomic make up, mass, labeled backbone and full name.
#
# Overridden to use the first and last position of the labeled element in the 
# fragment bacbone, as specified in each compound array.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;

   open (FILE, ">" . $printFile) or FileSystemError(900, $printFile, $!);

   print FILE $self->GetDataType . " Data Table:\n\n\t";

   print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\tFull Name\n";
  
   foreach my $compound ($self->GetCompoundList) {   
      my $fullName = $self->GetCompoundName($compound);
      my @totAtoms = $self->GetTotalAtomCount($compound);
      my $mass     = $self->GetMass($compound);
		my ($first, $last) = @{${ $self->{_compounds} }{$compound}[2]};

      print FILE $compound . "\t\t" .  join("\t", @totAtoms) . "\t" . $mass . "\t" . $first . "-" . $last . "\t" . $fullName . "\n";
   }
   
   close(FILE)                 or FileSystemError(901, $printFile, $!);
}

1;