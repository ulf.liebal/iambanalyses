package FluxY_Lib::MS::OptionalChecks;

################################################################################
# This module provides the subroutines involved in the optional checking of MS
# data.
# - Exclusively called within the 'CheckData' subroutine of the main program. 
#
# The subroutines are:
# - CheckDL(): Checks that data is below the maximum reliable value.
# - CheckPQ(): Checks that each fragment meets a minimum resolution.
# - CheckRT(): Checks for retention times outside of given range.
# - ProcessChecks(): Processes and outputs the results of the data checks.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Data::Data_Check;      # User Lib - Performs basic data checking.
use FluxY_Lib::Math::Stats_Utils;     # User Lib - Simple stats functions.

use base 'Exporter';

our @EXPORT = qw(CheckDL CheckPQ CheckRT ProcessChecks);

################################################################################
# Subroutine: CheckDL
#
# Checks each data value against the detector limit.
# Generate the overLimitData array (passed by reference as check):
# - a '1' signifies the corresponding measured value is over the limit.
#
# Returns the number of data values that exceed the detector limit.
#
################################################################################
sub CheckDL{
   my ($threshold, $refData, $refCheck) = @_;
   my @data2D = @{$refData};
   my @check  = @{$refCheck};

   my $count = 0;
   
   for (my $i=0; $i<@data2D ; $i++) {
      my @data = @{$data2D[$i]};
      
      for(my $j=0; $j<@data; $j++) {
         if ( $data[$j] > $threshold ) {
            $check[$i][$j] = 1;
            $count++
         } else {
            $check[$i][$j] = 0;
         }
      }
   }

   return $count;
}


################################################################################
# Subroutine: CheckPQ
#
# Checks each fragment to see if the peak quality provides a minimum resolution.
# May check based on sum or average of fragment measurements.
# Generate the poorPeakQual array (passed by reference as check):
# - a '1' signifies the corresponding measured peak is of poor quality.
#
# Returns the number of fragments with poor peak quality.
#
################################################################################
sub CheckPQ{
   my ($threshold, $refData, $refIDX, $refCheck) = @_;
   my @data2D = @{$refData};
   my @index  = @{$refIDX};
   my @check  = @{$refCheck};

   my $count = 0;

   for (my $i=0; $i<@data2D ; $i++) {
      my @data = @{$data2D[$i]};

      my @ppq = ();
      for(my $j=0; $j<(@index-1); $j++) {
         my $flag = 0;
         my $sum  = 0;
         for(my $k=$index[$j]; $k<$index[$j+1]; $k++) {
            if ( !CheckIfNumber($data[$k], "D") ) { next; }
            $sum += $data[$k];
         }
         
         if ( $sum < $threshold ) {
            $flag = 1;
            $count++;
         }
         
         for(my $k=$index[$j]; $k<($index[$j+1]); $k++) {
            $check[$i][$k] = $flag;
         }
      }
   }
   
   return $count;
}


################################################################################
# Subroutine: CheckRT
#
# Process the peak retention times to find anomolous peaks.
# - Uses all data for a given compound. To do this it starts by deriving the
# list of the compound index starting points. This is different than the
# fragment index, if there is more than one fragment per compound.
#
# Re-uses the dataRT array (passed by reference):
# - a '-1' signifies that the retention time data is missing.
# - a '1' signifies the corresponding retention time is more than the specified 
# number of standard deviations from the mean retention time for this compound, 
# calculated on a per compound basis over all fragments and all data sets.
#
# Returns the number of peak retention times that exceed the set number of
# standard deviations.
#
################################################################################
sub CheckRT{
   my ($threshold, $refData, $refNames) = @_;
   my @dataRT = @{$refData};
   my @cNames = @{$refNames};

   my $count = 0;

   my @index = (0);
   my $name  = $cNames[0];
   for(my $i=1; $i<@cNames; $i++) {
      if ($name eq $cNames[$i]) { next; }
   
      $name = $cNames[$i];
      push(@index, $i);
   }
   push(@index, scalar(@cNames));

   for(my $j=0; $j<$#index; $j++) {
      my @fragment = ();
   
      for (my $i=0; $i<@dataRT ; $i++) {
         my @data = @{$dataRT[$i]};

         push(@fragment, @data[$index[$j]..($index[$j+1]-1)]);
      }
      
      my $avg   = &Average(@fragment);
      my $stdev = &StdDev(@fragment);
      
      for (my $i=0; $i<@dataRT ; $i++) {
         for (my $k=$index[$j]; $k<$index[$j+1]; $k++) {
            if ( !CheckIfNumber($dataRT[$i][$k], "D") ) {
               $dataRT[$i][$k] = -1;
               next;
            }
            if ( (abs($avg - $dataRT[$i][$k]) / $stdev) > $threshold) {
               $dataRT[$i][$k] = 1;
               $count++;
            } else {
               $dataRT[$i][$k] = 0;
            }
         }
      }
   }
   
   return $count;
}


################################################################################
# Subroutine: ProcessChecks
#
# Summarize and print the results of the data checks.
#
################################################################################
sub ProcessChecks{
   my ($flag, $refOLD, $refPPQ, $refRT, $refNames, $refMasses) = @_;
   my @overLimitData = @{$refOLD};
   my @poorPeakQual  = @{$refPPQ};
   my @dataRT        = @{$refRT};
   my @sNames        = @{$refNames};
   my @cMasses       = @{$refMasses};
   my @checkData = ();

   # Explicitly initialize the checkData matrix:
   for (my $i=0; $i<@sNames ; $i++) {
      for(my $j=0; $j<@cMasses; $j++) {
         $checkData[$i][$j] = "0";
      }
   }
   
   # process RT Check
   if ( $flag >= 4) {
      $flag -= 4;
      
      for (my $i=0; $i<@dataRT ; $i++) {
         my @data = @{$dataRT[$i]};
         
         for(my $j=0; $j<@data; $j++) {
            if ( $data[$j] == 1 ) { 
               $checkData[$i][$j] = "RT"; 
            } elsif ( $data[$j] == -1 ) {
               $checkData[$i][$j] = "RT_M"; 
            }
         }
      }
   }

   # process PPQ Check
   if ( $flag >= 2) {
      $flag -= 2;
      
      for (my $i=0; $i<@poorPeakQual ; $i++) {
         my @data = @{$poorPeakQual[$i]};
         
         for(my $j=0; $j<@data; $j++) {
            if ( $data[$j] == 1 ) {
               if ( $checkData[$i][$j] eq "0" ) {
                  $checkData[$i][$j] = "PPQ";
               } else {
                  $checkData[$i][$j] .= "-PPQ";
               }
            }
         }
      }
   }

   # process ODL Check
   if ( $flag >= 1) {
      for (my $i=0; $i<@overLimitData ; $i++) {
         my @data = @{$overLimitData[$i]};
         
         for(my $j=0; $j<@data; $j++) {
            if ( $data[$j] == 1 ) {
               if ( $checkData[$i][$j] eq "0" ) {
                  $checkData[$i][$j] = "ODL";
               } else {
                  $checkData[$i][$j] .= "-ODL";
               }
            }
         }
      }

   }

   return @checkData;
}


1;
