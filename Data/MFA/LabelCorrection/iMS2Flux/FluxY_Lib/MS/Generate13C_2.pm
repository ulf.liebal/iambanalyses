package FluxY_Lib::MS::Generate13C_2;

################################################################################
# This class provides for generating Mass Spectrometry output ready for
# inclusion in 13CFlux2 Steady State MFA models (FML format).
#
# This class inherits basic functionality from the Generate parent class,
# initializing it with the model specific headers. It extends the basic Generate
# functionality to add model specific support. As such it implements the
# Generate() and GenerateModels() methods.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::IO::IO_Utils;      # User Lib - Provides I/O routines.
use FluxY_Lib::MS::Generate;      # User Lib - The common parent class.
#use FluxY_Lib::Math::Math_Utils;  # User Lib - Simple arithmetic functions.

use base 'Exporter';

our @ISA = qw(FluxY_Lib::MS::Generate);   # Inherits from DataClass

# Section headers of interest for 13CFLUX2 FML file:
my @headers = (
      "labelingmeasurement",
      "fluxmeasurement",
      "data",
      "simulation",
      );

            
################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class) = @_;
   
   my $self = $class->SUPER::new(\@headers, "13CFlux2");

   bless $self, $class;

   return $self;
}


######################################
#                                    #
#   Implemented DataClass methods.   #
#                                    #
######################################

		

################################################################################
# Subroutine: Generate
#
# For a given set of measurements and errors, this will print out the
# labelingmeasurement section of the model and data sections (one per averaged
# set of data), ready for insertion into a 13CFLUX2 network configuration (FML)
# file.
#
################################################################################
sub Generate{
   my ($self, $baseName, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS) = @_;
   
   my @model = &GenerateLinesModel($refIDX, $refRH1, $refFAS);
   my @data  = &GenerateLinesData($refIDX, $refAVG, $refSTD);
   
   my @colHeads = @{$refCH};

   for (my $i=0; $i<@colHeads ; $i++) {
      my $name  = $baseName . "-13C2_" . $colHeads[$i] . ".txt";

      open (FILE, ">" . $name) or FileSystemError(900, $name, $!);
      
		print FILE "\t\t\t<model>\n";
		foreach my $line (@model) { print FILE $line . "\n"; }
		print FILE "\t\t\t</model>\n";
		
      my @lines = @{$data[$i]};
		print FILE "\t\t\t<data>\n";
      foreach my $line (@lines) { print FILE $line . "\n"; }
		print FILE "\t\t\t</data>\n";
      
      close(FILE)              or FileSystemError(901, $name, $!);
   }
}


################################################################################
# Subroutine: GenerateModels
#
# Generates a set of 13CFLUX2 model files, one for each combination of model
# and measurement.
#
################################################################################
sub GenerateModels{
   my ($self, $refMod, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS, $appendFlag) = @_;

   my @modelFiles = @{$refMod};
   my @colHeads   = @{$refCH};

   my @model = ();
   my @data  = ();

   my $offset = -1;
   foreach my $model (@modelFiles) {
      my ($modelBase, $modelExt) = split (/\./, $model);

      # Read in model file.
      my @modelLines = &ReadFile($model);
      
      # Index of start line for each section.
      my @sectionIdx  = $self->GetSectionIndex(@modelLines);
      
      # If appending, first find largest existing MS group ID, then reindex Model and Data sections.
      my $newOffset = 0;
      if ( $appendFlag ) {
         my @subLines = @modelLines[$sectionIdx[2]..$sectionIdx[3]];
      
         $newOffset = &GetMSoffset(\@subLines);
      }
      
      # Generate new model and data sections if offset has changed.
      if ( $newOffset != $offset ) {
         $offset = $newOffset;
         
         @model = &GenerateLinesModel($refIDX, $refRH1, $refFAS, $offset);
         @data  = &GenerateLinesData($refIDX, $refAVG, $refSTD, $offset);         
      }
      
      for (my $j=0; $j<@data; $j++) {
         my $outFile = $modelBase . "-OF_" . $colHeads[$j] . "." . $modelExt;
         my @dLines  = @{$data[$j]};

         open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
         
         for (my $i=0; $i<$sectionIdx[0]; $i++) { print FILE $modelLines[$i] . "\n"; }
         
         # If appending, print out the existing sections first, but not the end of section (last line),
         # and remove the extra heading line from the model.
         if ( $appendFlag ) {
            for (my $i=$sectionIdx[0]; $i<$sectionIdx[1]-1; $i++) { print FILE $modelLines[$i] . "\n"; }
         
            shift @model;
         } else {
            print FILE "\n";
         }

         #Print the model <labelingmeasurement> section:
         foreach my $line (@model) { print FILE $line . "\n"; }
          
         # Print the model <fluxmeasurement> section
         for (my $i=$sectionIdx[1]; $i<$sectionIdx[2]; $i++) { print FILE $modelLines[$i] . "\n"; }

         #Print the data section, replacing MS data:
         for (my $i=$sectionIdx[2]; $i<$sectionIdx[3]; $i++) {
            # Skip existing MS data, unless appending:        
            if ( $modelLines[$i] =~ "ms_group"  && !($appendFlag) ) {
               next;
            }
            
            # Print out data immediately prior to the end of the data section.
            if ($modelLines[$i] =~ "/data") {
               foreach my $line (@dLines) { print FILE $line . "\n"; }
            }
            
            print FILE $modelLines[$i] . "\n";
         }
         
         #Print remaining model sections.
         for (my $i=$sectionIdx[3]; $i<@modelLines; $i++) { print FILE $modelLines[$i] . "\n"; }
         
         close(FILE)                 or FileSystemError(901, $outFile, $!);
      }
   }
}


#######################
#                     #
#   Public methods.   #
#                     #
#######################

################################################################################
# Subroutine: GenerateLinesModel
#
# This will generate and return the labelingmeasurement section of the model,
# ready for insertion into a 13CFLUX2 network configuration (FML) file.
# - The results are returned is a 1D array.
#
################################################################################
sub GenerateLinesModel{
   my ($refIDX, $refRH1, $refFAS, $offset) = @_;
   my @fIndex   = @{$refIDX};
   my @rowHead1 = @{$refRH1};
   my @atomStrs = @{$refFAS};

   if ( !defined($offset) ) { $offset = 0; }

	my @lines = ("\t\t\t\t<labelingmeasurement>");

	for (my $j=0; $j < $#fIndex; $j++) {
	
	   my $massStr = "#M";
		for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {
			my $diff = $k-$fIndex[$j];
			
			$massStr .= $diff . ",";
      }
	   chop($massStr);
	
		push(@lines, "\t\t\t\t\t<group id=\"ms_group_" . ($offset+$j+1) . "\" scale=\"auto\">");
	
		push(@lines, "\t\t\t\t\t\t<textual>" . uc($rowHead1[$fIndex[$j]]) . "[" . $atomStrs[$j] . "]" . $massStr . "</textual>");

		push(@lines, "\t\t\t\t\t</group>");
	}

	push(@lines, "\t\t\t\t</labelingmeasurement>");
	   
   return @lines;
}


################################################################################
# Subroutine: GenerateLinesData
#
# This will generate and return the set of data sections, one per averaged set
# of data, ready for insertion into a 13CFLUX2 network configuration (FML) file.
# - The results are returned is a 2D array.
#
################################################################################
sub GenerateLinesData{
   my ($refIDX, $refAVG, $refSTD, $offset) = @_;
   my @fIndex   = @{$refIDX};
   my @averages = @{$refAVG};
   my @stdDevs  = @{$refSTD};

   if ( !defined($offset) ) { $offset = 0; }

   my @msSections = ();
   for (my $i=0; $i<@averages ; $i++) {
   
      my @val = @{$averages[$i]};
      my @dev = @{$stdDevs[$i]};

      my @lines = ();

      for (my $j=0; $j < $#fIndex; $j++) {      
         for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {
            my $diff = $k-$fIndex[$j];
            push(@lines, "\t\t\t\t<datum id=\"ms_group_" . ($offset+$j+1) . "\" stddev=\"" . $dev[$k] . "\" weight=\"" . $diff . "\">" . $val[$k] . "</datum>");
         }
      }
 
      push(@msSections, [ @lines ]);
   }
   
   return @msSections;
}


################################################################################
# Subroutine: GetMSoffset
#
# This will find and return the value of the largest MS group ID.
#
################################################################################
sub GetMSoffset{
   my ($refLines) = @_;

   my @lines = @{$refLines};
   
   my $groupID = 0;
   foreach my $line (@lines) {
      if ( $line =~ "ms_group" ) {
         my @temp = split(/\"/,$line);
   
         @temp = split(/_/,$temp[1]);
      
         if ( $temp[2] > $groupID ) { $groupID = $temp[2]; }
      }
   }
   
   return $groupID;
}

1;