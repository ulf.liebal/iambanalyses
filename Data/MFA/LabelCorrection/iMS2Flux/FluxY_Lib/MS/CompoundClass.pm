package FluxY_Lib::MS::CompoundClass;

################################################################################
# This module provides support to the specific compound data classes. Each 
# specific data class provides the details of a given compound and overrides,
# as necessary, the provided methods - in particular:
#   GetFragsList(),
#   GetMass(),
#   Mass2Frag(),
#   GetTotalAtomCount(),
#   GetNumFragmentedLabeledElement();
#   GetNumFragmentedCarbons(),
#   GetLabeledPositionString(),
#   GenerateMassList(), and
#   PrintInfoTable.
#
# The methods in this class are a generalized implementation of an underivatized
# unfragmented compound. Thus, where required for compatibility, they are 
# treated as a compound with a single fragment using the specified full fragment
# identifier for full molecular ion.
################################################################################

use strict;
use warnings;

use FluxY_Lib::Chemistry::atomInfo; # User Lib - Atomic Masses.
use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple math functions.

use base 'Exporter';


################################################################################
# Constants
################################################################################
my $fullFrag = "M-0";   # The fragment identifier for the unfragmented compound.


################################################################################
# Class constructor
#
# Used to instantiate an object of the compound class, consisting of the 
# following data,
# _labeledElement # STRING: The labeled element.
# _dataType  # STRING: The name of the set of compounds in the specific class.
# _compounds # HASH:   A list of the compound data.
#    Each compound datum is an array which must contain at least two elements,
#    and in the following order:
#    1. The compound names, in lower case for standard comparison.
#    2. The atom count, in the order specified by atomList.
# _atomList  # ARRAY:  A list of the atoms considerred for natural abundance.
# _massList  # HASH:   A 2D Hash of compound masses.
#    Indexed by compunds key and frags keys.
#    Each element is the mass value for the given compound/fragment.
#
# The constructor is passed the first three data as parameters and after the
# object is instantiated (i.e. blessed) the last data type is initialized using
# the provided data and the optional flag.
#
# param: the $roundFlag, if set, indicates that the fragment masses should be
#        rounded instead of truncated (by default).
#
################################################################################
sub new {
   my ($class, $type, $cRef, $aRef, $lElement, $roundFlag) = @_;
   
   if ( !defined($lElement) ) { $lElement = 'C'; };
   
   # Initiallize class variables.
   my $self = {
      _labeledElement => $lElement,
      _dataType       => $type,
      _compounds      => $cRef,
      _atomList       => $aRef,
   };

   bless $self, $class;
   
   # Initialize the compound mass list.
   $self->{_massList} = $self->GenerateMassList($roundFlag);
       
   return $self;
}


################################################################################
# Subroutine: GetCompound
#
# Returns the requested compound array.
#
################################################################################
sub GetCompound {
   my ($self, $compound) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   return @{ ${ $self->{_compounds} }{$compound} };
}


################################################################################
# Subroutine: GetCompoundName
#
# Returns the requested compound array.
#
################################################################################
sub GetCompoundName {
   my ($self, $compound) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   return ${ $self->{_compounds} }{$compound}[0];
}


################################################################################
# Subroutine: GetAtomCount
#
# Returns the atom count corresponding to the given compound.
#
################################################################################
sub GetAtomCount {
   my ($self, $compound) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   return @{${ $self->{_compounds} }{$compound}[1]};
}


################################################################################
# Subroutine: GetCompoundList
#
# Returns the array of hash keys for the compound list.
#
################################################################################
sub GetCompoundList {
   my ($self) = @_;

   return (sort keys %{ $self->{_compounds} });
}


################################################################################
# Subroutine: GetFragsList
#
# Returns the array of hash keys for the fragment list.
#    - This implementation is for an unfragmented compound, and as such uses the
#      specified default fragment identifier.
#
# This class must be overridden for data types that have multiple fragments of
# the same compound.
#
################################################################################
sub GetFragsList {
   my ($self) = @_;

   return [$fullFrag];
}


################################################################################
# Subroutine: GetAtomList
#
# Returns the atomList array.
#
################################################################################
sub GetAtomList {
   my ($self) = @_;

   return @{ $self->{_atomList} };
}


################################################################################
# Subroutine: GetDataType
#
# Returns the dataType string.
#
################################################################################
sub GetDataType {
   my ($self) = @_;

   return $self->{_dataType};
}


################################################################################
# Subroutine: GetLabeledElement
#
# Returns the labeled element.
#
################################################################################
sub GetLabeledElement {
   my ($self) = @_;

   return $self->{_labeledElement};
}


################################################################################
# Subroutine: GetMass
#
# Returns the mass corresponding to the given compound.
#    - This implementation is for an unfragmented compound, and as such uses the
#      specified default fragment identifier.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GetMass {
   my ($self, $compound) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   return ${ $self->{_massList} }{$compound}{$fullFrag};
}


################################################################################
# Subroutine: Mass2Frag
#
# Returns the key corresponding to the specified mass of the given compound.
#    - This implementation is for an unfragmented compound, and as such uses the
#      specified default fragment identifier.
#    - If the mass does not match then no value is returned.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub Mass2Frag {
   my ($self, $compound, $fragMass) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   if ($fragMass == $self->GetMass($compound) ) { return $fullFrag; }
   
   return;
}




################################################################################
# Subroutine: GetTotalAtomCount
#
# Returns the total atom count corresponding to the derivatized compound or 
# compound fragment.
# - In this underivatized full fragment class it is the same as GetAtomCount().
#
# This class must be over-ridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GetTotalAtomCount {
   my ($self, $compound) = @_;
   
   return $self->GetAtomCount($compound);
}


################################################################################
# Subroutine: GetElementPosition
#
# Returns the position of the specified element in the atomList array.
# - if not found returns the flag: -1.
#
################################################################################
sub GetElementPosition {
   my ($self, $element) = @_;
   
   my @atomList = $self->GetAtomList;

   # Find the index corresponding to the specified element in the atom list:
   for (my $i=0; $i < @atomList; $i++) {
      if ($atomList[$i] eq $element) { return $i }
   }
   
   return -1;
}


################################################################################
# Subroutine: GetNumElement
#
# Returns the number of backbone atoms of the given element as specified in the
# compound list for the specified compound.
# - if the element is not found in the atom list returns the flag: -1.
#
################################################################################
sub GetNumElement {
   my ($self, $compound, $element) = @_;

   if ( !($self->CheckCompound($compound)) ) { return; }

   # Find the index corresponding to the specified element in the atom list:
   my $index = $self->GetElementPosition($element);
   
   if ( $index == -1 ) { return -1; }
   
   return ${${ $self->{_compounds} }{$compound}[1]}[$index];
}


################################################################################
# Subroutine: GetNumLabeledElement
#
# Returns the number of unfragmented labeled backbone atoms for the specified
# compound.
# - if the element is not found in the atom list, returns the flag: -1.
#
# This class must be overridden for data types where:
#    a) The unfragmented backbone is not stored in the compound list.
#
################################################################################
sub GetNumLabeledElement {
   my ($self, $compound) = @_;
   
   return $self->GetNumElement($compound, $self->{_labeledElement});
}


################################################################################
# Subroutine: GetNumFragmentedLabeledElement
#
# Returns the number of fragmented backbone atoms for the labeled element of the
# specified compound.
# - If the element is not found in the atom list, returns the flag: -1.
# - In this underivatized full fragment class it returns the same as
# GetNumLabeledElement() for carbon.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GetNumFragmentedLabeledElement {
   my ($self, $compound) = @_;

   
   return ($self->GetNumLabeledElement($compound));
}


################################################################################
# Subroutine: GetNumFragmentedCarbons
#
# Returns the number of fragmented backbone carbon atoms for the specified
# compound. This is used for the OBM correction.
# - If the element is not found in the atom list, returns the flag: -1.
# - In this underivatized full fragment class it returns the same as
# GetNumElement() for carbon.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GetNumFragmentedCarbons {
   my ($self, $compound) = @_;
   
   return $self->GetNumElement($compound, "C");
}


################################################################################
# Subroutine: GetPositionString
#
# Returns the positional string given the starting and ending positions.
# - the string is a comma seperated list of the numeric positions
#
################################################################################
sub GetPositionString {
   my ($self, $first, $last) = @_;
   
   my $atomStr = "$first";
   for (my $i=$first+1; $i<=$last; $i++) { $atomStr .= ",$i"; }

   return $atomStr;
}


################################################################################
# Subroutine: GetLabeledPositionString
#
# Returns the default positional string of the labeled element for the given 
# compound.
# - the default positional string is complete, i.e. 1 .. numElements.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GetLabeledPositionString {
   my ($self, $compound) = @_;

   my $numLabeled = $self->GetNumLabeledElement($compound);  
   
   # Check that the compound contains the specified element:
   if ( $numLabeled < 1 ) { return (-1,""); }
   
   return ($self->GetPositionString(1,$numLabeled));
}


################################################################################
# Subroutine: GenerateMassList
#
# Generates a list of mass values in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
#    - For compatibility this structure is indexed first by compunds key and 
#      then by fragment key.
#    - In the case of an unfragmented compound the default fragment identifier.
#    - Each element is the truncated mass (by default, but may be rounded if 
#      the round flag is set) of the full compound.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
#
################################################################################
sub GenerateMassList {
   my ($self, $roundFlag) = @_;

   my @atomMass = GetElementMasses($self->GetAtomList);

   my %massList;
   foreach my $compound ($self->GetCompoundList) {   
      my %list = ();
      
      my @totAtoms = $self->GetTotalAtomCount($compound);
      
      my $mass = 0;
      for(my $i=0; $i<@totAtoms; $i++) {
         $mass += ($totAtoms[$i] * $atomMass[$i]);
      }
      
       if ( !$roundFlag ) {
         $list{$fullFrag} = &Trunc($mass);
      } else {
         $list{$fullFrag} = &Round($mass);
      }
      
      $massList{$compound} = \%list;
   }
   
   return \%massList;
}


################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# compound, including its atomic make up, mass, labeled backbone and full name.
#    - As an unfragmented compound the labeled backbone is expected to be the 
#      same as the number of labeled atoms in the compound hash.
#
# This class must be overridden for data types that are:
#    a) Derivatized, and/or
#    b) fragmented.
# It may be over-ridden for other reasons as well.
################################################################################
sub PrintInfoTable{
   my ($self, $outFile, $title) = @_;   
   
   if ( !defined($outFile) ) { $outFile = "Data_Table.txt"; }
   if ( !defined($title) )   { $title   = $self->GetDataType; }
   
   open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);

   print FILE $title . " Data Table\n\n\t";

   print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\tFull Name\n";
  
   foreach my $compound ($self->GetCompoundList) {   
      my $fullName = $self->GetCompoundName($compound);
      my @totAtoms = $self->GetTotalAtomCount($compound);
      my $mass     = $self->GetMass($compound);

      print FILE $compound . "\t\t" .  join("\t", @totAtoms) . "\t" . $mass . "\t" . 1 . "-" . $self->GetNumLabeledElement($compound) . "\t" . $fullName . "\n";
   }
   
   close(FILE)                 or FileSystemError(901, $outFile, $!);
}


################################################################################
# Subroutine: CheckCompound
#
# Checks that the compound is specified and is in the _compounds data hash.
#
################################################################################
sub CheckCompound {
   my ($self, $compound) = @_;

   # Check that a value for the coumpound key is specified:
   if ( !(defined $compound) ) { return 0; }
   
   # Check that the specified compound is a valid key:
   if ( !(exists  ${$self->{_compounds}}{$compound}) ) { return 0; }

   return 1;
}

1;