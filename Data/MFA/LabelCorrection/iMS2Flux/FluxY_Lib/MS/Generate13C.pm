package FluxY_Lib::MS::Generate13C;

################################################################################
# This class provides for generating Mass Spectrometry output ready for
# inclusion in 13CFlux Steady State MFA models (FTBL format).
#
# This class inherits basic functionality from the Generate parent class,
# initializing it with the model specific headers. It extends the basic Generate
# functionality to add model specific support. As such it implements the
# Generate() and GenerateModels() methods.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::IO::IO_Utils;      # User Lib - Provides I/O routines.
use FluxY_Lib::MS::Generate;      # User Lib - The common parent class.
use FluxY_Lib::Math::Math_Utils;  # User Lib - Simple arithmetic functions.

use base 'Exporter';

our @ISA = qw(FluxY_Lib::MS::Generate);   # Inherits from DataClass

# Section headers for 13CFLUX FTBL file:
my @headers = (
      "PROJECT",
      "NETWORK",
      "FLUXES",
      "EQUALITIES",
      "INEQUALITIES",
      "FLUX_MEASUREMENTS",
      "LABEL_INPUT",
      "LABEL_MEASUREMENTS",
      "PEAK_MEASUREMENTS",
      "MASS_SPECTROMETRY",
      "OPTIONS");

my $format = "MS";   # The output format to generate.

################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class, $str) = @_;
   
	if ( defined($str) ) { $format =  $str; }
	
   my $self = $class->SUPER::new(\@headers, "13CFlux");

   bless $self, $class;

   return $self;
}


######################################
#                                    #
#   Implemented DataClass methods.   #
#                                    #
######################################

################################################################################
# Subroutine: Generate
#
# For a given set of measurements and errors, this will print out the specified
# (MASS_SPECTROMETRY or LABEL_MEASUREMENTS) section(s), one per averaged set of
# data, ready for insertion into a 13CFLUX network configuration (FTBL) file.
#
################################################################################
sub Generate{
   my ($self, $baseName, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS, $msType) = @_;
   	
	my @sections = ();
	if ($format eq "MS") {
      @sections = &GenerateLinesMS($refIDX, $refRH1, $refAVG, $refSTD, $refFAS);
	} elsif ($format eq "LM") {
      @sections = &GenerateLinesLM($refIDX, $refRH1, $refAVG, $refSTD, $refFAS, $msType);
	} else {
	   return -1;
	}
   
   my @colHeads = @{$refCH};

   for (my $i=0; $i<@sections ; $i++) {
      my $name  = $baseName . "-" . $format . "_" . $colHeads[$i] . ".txt";
      my @lines = @{$sections[$i]};

      open (FILE, ">" . $name) or FileSystemError(900, $name, $!);
      
      foreach my $line (@lines) { print FILE $line . "\n"; }
      
      close(FILE)              or FileSystemError(901, $name, $!);
   }
	
}


################################################################################
# Subroutine: GenerateModels
#
# For a given set of measurements and errors, in addition to a list of FTBL
# model files, this will print out new FTBL files, one per averaged set of data,
# with each set of measurement and error data incorporated in the corresponding
# MASS_SPECTROMETRY or LABEL_MEASUREMENTS section.
#
################################################################################
sub GenerateModels{
   my ($self, $refMod, $refIDX, $refCH, $refRH1, $refAVG, $refSTD, $refFAS, $appendFlag, $msType) = @_;

	my @sections = ();
	my $section  = "";
	if ($format eq "MS") {
      @sections = &GenerateLinesMS($refIDX, $refRH1, $refAVG, $refSTD, $refFAS);
      $section = 9;
	} elsif ($format eq "LM") {
      @sections = &GenerateLinesLM($refIDX, $refRH1, $refAVG, $refSTD, $refFAS, $msType);
		$section = 7;
	} else {
	   return -1;
	}
   
   &GenerateModel($self, $refMod, $refCH, $refRH1,$appendFlag, \@sections, $section);
}


#######################
#                     #
#   Public methods.   #
#                     #
#######################

################################################################################
# Subroutine: GenerateLinesMS
#
# For a given set of measurements and errors, this will generate and return the
# MASS_SPECTROMETRY section ready for insertion into a 13CFLUX network
# configuration (FTBL) file.
# - The results are returned in a 2D array.
#
################################################################################
sub GenerateLinesMS{
   my ($refIDX, $refRH1, $refAVG, $refSTD, $refFAS) = @_;
   my @fIndex   = @{$refIDX};
   my @rowHead1 = @{$refRH1};
   my @averages = @{$refAVG};
   my @stdDevs  = @{$refSTD};
   my @atomStrs = @{$refFAS};

   my @msSections = ();
   for (my $i=0; $i<@averages ; $i++) {
   
      my @val = @{$averages[$i]};
      my @dev = @{$stdDevs[$i]};

      my @lines = ("MASS_SPECTROMETRY");
      push(@lines, "\tMETA_NAME\tFRAGMENT\tWEIGHT\tVALUE\tDEVIATION");

      for (my $j=0; $j < $#fIndex; $j++) {      
         for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {
            my $diff = $k-$fIndex[$j];
            if ( $diff == 0 ) {
               push(@lines, "\t" . uc($rowHead1[$k]) . "\t" . $atomStrs[$j] . "\t" . $diff . "\t" . $val[$k] . "\t" . $dev[$k]);
            } else {
               push(@lines, "\t\t\t" . $diff . "\t" . $val[$k] . "\t" . $dev[$k]);
            }
         }
         push(@lines, "");
      }
 
      push(@msSections, [ @lines ]);
   }
   
   return @msSections;
}


################################################################################
# Subroutine: GenerateLinesLM
#
# For a given set of measurements and errors, this will generate and return the
# LABEL_MEASUREMENTS section ready for insertion into a 13CFLUX network
# configuration (FTBL) file.
# - The results are returned in a 2D array.
#
################################################################################
sub GenerateLinesLM{
   my ($refIDX, $refRH1, $refAVG, $refSTD, $refFAS, $msType) = @_;
   my @fIndex   = @{$refIDX};
   my @rowHead1 = @{$refRH1};
   my @averages = @{$refAVG};
   my @stdDevs  = @{$refSTD};
   my @atomStrs = @{$refFAS};
   
   my @lmSections = ();
   for (my $i=0; $i<@averages ; $i++) {
   
      my @val = @{$averages[$i]};
      my @dev = @{$stdDevs[$i]};

      my @lines = ("LABEL_MEASUREMENTS");
      push(@lines, "\tMETA_NAME\tCUM_GROUP\tVALUE\tDEVIATION\tCUM_CONSTRAINTS");

      for (my $j=0; $j < $#fIndex; $j++) {
         for (my $k=$fIndex[$j]; $k<$fIndex[$j+1]; $k++) {         
            my $diff = $k-$fIndex[$j];
            
            my $cumGrp = &GenerateConstraints($atomStrs[$j], $msType->GetNumLabeledElement($rowHead1[$k]), $diff);
            
            if ( $diff == 0 ) {
               push(@lines, "\t" . uc($rowHead1[$k]) . "\t" . ($diff+1) . "\t" . $val[$k] . "\t" . $dev[$k] . "\t" . $cumGrp);
            } else {
               push(@lines, "\t\t" . ($diff+1) . "\t" . $val[$k] . "\t" . $dev[$k] . "\t" . $cumGrp);
            }
         }
         push(@lines, "");
      }
      
      push(@lmSections, [ @lines ]);
   }
   
   return @lmSections;
}


################################################################################
# Subroutine: GenerateConstraints
#
# Generates the cumomer group constraints as isotopomers that contribute to 
# different fragment mass isotopomers.
#
################################################################################
sub GenerateConstraints{
   my ($atomStr, $total, $mass) = @_;   
   
   # atomStr is a comma seperated list of numbers, such as: 2,3,4
   my @cList = split(',', $atomStr);   
   
   # Get the first and last position of the labeled element for the fragment.
   my $first = $cList[0];
   my $last  = $cList[-1];

   my $num   = @cList;
      
   my $constr = "";   
   for (my $i=0; $i<2**$num; $i++) {
   
      my $str = "";
      my $sum  = 0;
      for (my $j=1; $j<=$total; $j++) {
         if ( $j == $first ) {
            my @bins = &Int2Bin($i, $num);
            foreach my $bin (@bins) {
               $sum += $bin;
               $str .= $bin;
            }
            $j = $last;
         } else {
            $str .= 'x';
         }
      }
      if ($sum == $mass) { $constr .= "#" . $str . "+"; }
   }
   chop $constr;
      
   return $constr;
}


################################################################################
# Subroutine: GenerateModel
#
# Generates a set of 13CFLUX FTBL model files, one for each combination of model
# and measurement (either MS or LM, as specified).
#
################################################################################
sub GenerateModel{
   my ($self, $refMod, $refCH, $refRH1,$appendFlag, $refData, $sectionNum) = @_;

   my @modelFiles = @{$refMod};
   my @colHeads   = @{$refCH};
   my @data       = @{$refData};

   foreach my $model (@modelFiles) {
      my ($modelBase, $modelExt) = split (/\./, $model);

      # Read in model file.
      my @mLines = &ReadFile($model);
      
      # Index of start line for each section.
      my @sectionIdx  = $self->GetSectionIndex(@mLines);
      
      # Check for unmatched metabolites in the model file.
      &CheckMetabolites(\@mLines, $sectionIdx[1]+2, $sectionIdx[2], $refRH1);
      
      for (my $i=0; $i<@data; $i++) {
         my $outFile = $modelBase . "-" . $format . "_" . $colHeads[$i] . "." . $modelExt;
         my @lines   = @{$data[$i]};
                  
         open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
         
         for (my $i=0; $i<$sectionIdx[$sectionNum]; $i++) { print FILE $mLines[$i] . "\n"; }
         
         # If appending, print out the section and remove the extra heading lines.
         if ( $appendFlag ) {
            for (my $i=$sectionIdx[$sectionNum]; $i<$sectionIdx[$sectionNum+1]; $i++) { print FILE $mLines[$i] . "\n"; }
         
            shift @lines;
            shift @lines;
         }
         
         for (my $i=0; $i<@lines; $i++) { print FILE $lines[$i] . "\n"; }         
         print FILE "\n";
         for (my $i=$sectionIdx[$sectionNum+1]; $i<@mLines; $i++) { print FILE $mLines[$i] . "\n"; }
         
         close(FILE)                 or FileSystemError(901, $outFile, $!);
      }
   }
}


################################################################################
# Subroutine: CheckMetabolites
#
# Checks the metabolite names of the measurement data against those in the
# 13CFLUX FTBL model file. Prints warning if matching name is not found.
# 
################################################################################
sub CheckMetabolites{
   my ($refLines, $start, $end, $refRH1) = @_;
  
   my @lines   = @{$refLines};
   my @rowHead = @{$refRH1};

   # Get list of metabolites in the model file.
   my @metabs = ();
   for (my $i=$start; $i<$end; $i++) {
      my @list = split('\t', $lines[$i]);
      
      if ( !defined($list[1]) ) { next; }
      if ( $list[1] =~ /^\s*$/ ) { next; }

      for (my $j=2; $j<@list; $j++ ) {
         if ( !defined($list[$j]) ) { next; }
         
         if ( $list[$j] =~ /^\s*$/ ) { next; }
         
         if ( $list[$j] =~ /\// ) { next; }

         my $found = 0;
         foreach my $metab (@metabs) {
            if ( $metab eq $list[$j] ) {
               $found = 1;
               last;
            }
         }
         
         if ( !$found ) { 
            $list[$j] =~ s/\s+$//;
            push(@metabs, $list[$j]);
         }
      }
   }         

   # Compare measurement names to the metabolite list.
   my @missing = ();
   my $current = "";
   foreach my $name (@rowHead) {
      $name = uc($name);
      
      if ( $current eq $name ) { next; }
      
      $current = $name;
      
      my $found = 0;
      foreach my $metab (@metabs) {
         if ( $name eq $metab ) {
            $found = 1;
            last;
         }
      }
      
      if ( !$found ) { push(@missing, $name); }
   }
   
   if ( @missing > 0 ) {
      print "\n\nINFO:\n=====\n";
      print "\tThe following metabolites are specified in the measurement\n"  .
            "\tsection, but not found in the model stoichiometry specified\n" .
            "\tin the NETWORK section:\n\n";
      foreach my $name (@missing) { print "\t\t" . $name . "\n"; }
      print "\n\tThese metabolite(s) are either named differently in the model,\n"  .
            "\tor they are not used in the model.\n\n" . 
            "\tPlease check the model file(s) before use.\n\n\n";
   }
}

1;