package FluxY_Lib::MS::ParseTSV;

################################################################################
# This module provides support for parsing Mass Spectrometry data saved in
# a Tab Separated Values (TSV) text file.
#
# This method of input is for data to be processed, and negates many of the 
# configuration settings and flags - those dealing with retention time, and 
# selecting/printing Area/Response data. In this format there is only one value
# provided per mass isotopomer, this is stored in 'dataResps'. The 'dataAreas'
# array is left uninitialized, and the dataFlags array is filled with dummy
# values. Optionally retention time data may be supplied as a second TSV file,
# otherwise the 'dataRT' array is also left uninitialized.
#
# Two variations are provided, one each for vertically and horizontally oriented
# data sets.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Data::Data_Utils;    # User Lib - Provides Transpose TSL.
use FluxY_Lib::IO::IO_Utils;        # User Lib - Provides I/O routines.
use FluxY_Lib::MS::ErrorsAndHelp;   # User Lib - Prints GCMS related messages.

use base 'Exporter';

our @EXPORT = qw(ParseTabbedInput_H ParseTabbedInput_V);

################################################################################
# Subroutine: ParseTabbedInput_H
#
# Extracts data from a tab separated list file. Data is oriented horizontally,
# i.e. each row represents a data set.
# - The first row contains the compound/fragment names.
# - The second row the compound/fragment masses.
# - The remaining rows contain the experiment name, followed by a list of 
# measurements, one for each compound/fragment mass isotopomer.
#
################################################################################
sub ParseTabbedInput_H{
   my ($dataFile, $msType, $rtFile) = @_;

   # Read in data file and remove blank lines.
   my @lines = &ReadFileSkipB($dataFile);
   
   return ( &ProcessTabbedInput($dataFile, $msType, $rtFile, @lines) );

}


################################################################################
# Subroutine: ParseTabbedInput_V
#
# Extracts data from a tab separated list file. Data is oriented vertically,
# i.e. each column represents a data set.
# - The first column contains the amino acid names.
# - The second column the amino acid fragment masses.
# - The remaining columns contain the experiment name, followed by a list of 
# measurements, one for each fragment isotopomer.
#
# This method of input is for data to be processed, and negates many of the 
# configuration settings and flags - those dealing with retention time, and 
# selecting/printing area/response data.
#
################################################################################
sub ParseTabbedInput_V{
   my ($dataFile, $msType, $rtFile) = @_;
   
   # Read in data file and remove blank lines.
   my @lines = &ReadFileSkipB($dataFile);
   
   # Transpose 2D data array to match horizontal orientation.
   @lines = &TransposeTSL(@lines);

   return ( &ProcessTabbedInput($dataFile, $msType, $rtFile, @lines) );
}


################################################################################
# Subroutine: ProcessTabbedInput
#
# Performs the actual data extraction for tabbed input assuming horizontal
# orientation.
#
################################################################################
sub ProcessTabbedInput{
   my ($dataFile, $msType, $rtFile, @lines) = @_;

   my @cNames    = ();   # The array of compund names.
   my @cMasses   = ();   # The array of compund masses.
   my @sNames    = ();   # The array of sample names.
   my @dataResps = ();   # The array of MS measurements.
   my @dataAreas = ();   # Second MS quantification method (not used in simple TSV, left empty).
   my @dataRT    = ();   # The array of retention times (used if a valid secondary file is provided).
   my @dataFlags = ();   # Detection flags for each data set (not used in simple TSV, dummy filled).

   # Parse the compound name and mass. The name may or may not be repeated with 
   # each corresponding mass.
   @cNames  =  split (/\t/, $lines[0]);
   @cMasses =  split (/\t/, $lines[1]);

   # First value is not used (corresponds to column with title).
   shift @cNames;
   shift @cMasses;
   # Second value is blank (corresponds to column with dataset names).
   shift @cNames;
   shift @cMasses;

   # Convert compound names to lower case, & insert intermediate skipped names.
   my $curName = "";
   for (my $i=0; $i<@cNames; $i++) {
      if ( defined $cNames[$i] ) {
         if ( $cNames[$i] ne "" ) {
            $curName = lc($cNames[$i]);
         }
      }
            
      $cNames[$i] = $curName;
   }
   
   # Fill the compound name list if smaller than the masses list.
   my $diff = scalar(@cMasses) - scalar(@cNames);
   for (my $i=0; $i<$diff; $i++) {
      push(@cNames, $curName);
   }

   # Checks that all data sets contain the same number of samples.
   my $numSamples = @cNames;
   
   # For each data set extract the name and measurement values for all masses.
   for (my $i=2; $i<@lines; $i++) {
      my (@data)  = split(/\t/, $lines[$i]);
      # First value is not used (corresponds to column with title).
      shift @data;
      # Second value contains dataset name.
      my $name = shift @data;

      # If there are more data than columns throw error.
      if ( $numSamples < scalar(@data) ) { &Error(202, $dataFile, $i-1); }

      # If there are less data than columns pad data.
      if ( $numSamples > scalar(@data) ) {
         $diff = $numSamples - scalar(@data);         
         for (my $k=0; $k<$diff; $k++) {
            push(@data, "");
         }
      }
         
      # Required to set dummay flags.
      my @flags = ();
      for (my $j=0; $j<$numSamples; $j++) { push (@flags, ""); }
      
      push (@sNames, $name);
      push (@dataResps, [ @data ]);
      push (@dataFlags, [ @flags ]);
   }
   
   if ( $rtFile ne "" ) {
      my @results = &ParseTabbedInput_V($rtFile, $msType, "");
      
      @dataRT    = @{$results[3]};
      
      # Check the number of samples match.
      if ( scalar(@sNames) != scalar(@dataRT) ) { &Error(203, $rtFile, $dataFile, "columns"); }
      
      # Check the number of masses match.
      if ( scalar(@cMasses) != scalar(@{$dataRT[0]}) ) { &Error(203, $rtFile, $dataFile, "rows"); }
   }
   
   return(\@cNames, \@cMasses, \@sNames, \@dataResps, \@dataAreas, \@dataRT, \@dataFlags);
}

1;