package FluxY_Lib::MS::OptionalChecks2;

################################################################################
# This module provides the subroutines involved in the optional checking of MS
# data post correction.
#
# The subroutines are:
# - calcAvgLabelling(): Calculates the average 13C labelling for each fragment
# and each fragment for the whole.
#
################################################################################

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw(calcAvgLabelling);


################################################################################
# Subroutine: calcAvgLabelling
#
# Calculates the average labelling of each fragment and each experiment. The
# calculations are performed over all replicates for both the fragment and the
# experimental averages.
#
# This returns three arrays, a 2D array of the average fragment labelling, each 
# element contains an array of averages, one per fragment for a specific
# experiment, a 1D array containing the overall average labelling for each
# experiment and a 2D array of the standard deviations corresponding to each
# element in the average fragment labelling 2D array.
#
################################################################################
sub calcAvgLabelling{
   my ($refDataR, $refDataC, $refFragIndex, $refReps) = @_;
   my @rawData    = @{$refDataR};
   my @corData    = @{$refDataC};
   my @fIndex     = @{$refFragIndex};
   my @replicates = @{$refReps};

   my @avgFrag = ();
   my @sdFrag  = ();
   my @avgExp  = ();

	my $startIdx = 0;
   foreach my $numReps (@replicates) {	
		my $cTotal = 0;  # Total number of atoms / experiment.
		my $lTotal = 0;  # Total number of labeled atoms / experiment.

		my @avgTmp = ();
		my @sdTmp  = ();
		for (my $i=0; $i<(@fIndex-1); $i++) {
			my @cTotal_frag = ();  # List of total number of atoms / fragment per replica.
			my @lTotal_frag = ();  # List of total number of labeled atoms / fragment per replica.

			# The number of atoms of interest in the fragment.
			my $numAtoms = $fIndex[$i+1] - $fIndex[$i] - 1;
			
			for (my $rep=0; $rep<$numReps ; $rep++) {
				my $offset = $startIdx + $rep;
				my @rData  = @{$rawData[$offset]};
				my @cData  = @{$corData[$offset]};
						
				my @rFragList = @rData[$fIndex[$i]..($fIndex[$i+1]-1)];
				my @cFragList = @cData[$fIndex[$i]..($fIndex[$i+1]-1)];
								
				# Calculate the total number of measured fragments.
				my $rSum = 0;
				for (my $j=0; $j<@rFragList; $j++) { $rSum += $rFragList[$j]; }

				# Calculate the total number of atoms/fragment.
				push(@cTotal_frag, ($rSum * $numAtoms));
				
				# Calculate the number of labelled atoms.
				# No need to add the M+0 measurement ($j=0) as that can have no labelled atoms.
				my $sum = 0;
				for (my $j=1; $j<@cFragList; $j++) {
					$sum +=  ($rSum * $j * $cFragList[$j]);
				}
				push(@lTotal_frag, $sum);
			}
			
			my $cTotal_frag = &Sum(@cTotal_frag);  # Total number of atoms / fragment.
			my $lTotal_frag = &Sum(@lTotal_frag);  # Total number of labeled atoms / fragment.
			
			# Calculate the weighted standard deviation:
			if ($cTotal_frag > 0) {
				my $mean = $lTotal_frag/$cTotal_frag;
				
				my $sdNumerator = 0;
				for (my $rep=0; $rep<$numReps ; $rep++) {
				   if ($cTotal_frag[$rep] > 0) {
						$sdNumerator += $cTotal_frag[$rep] * ((($lTotal_frag[$rep]/$cTotal_frag[$rep]) - $mean)**2);
					}
				}				

				push (@avgTmp, $mean);
				push (@sdTmp, $sdNumerator/$cTotal_frag);
			} else { 
				push (@avgTmp, -1);
				push (@sdTmp,  -1);
			}

			$cTotal +=  $cTotal_frag;
			$lTotal +=  $lTotal_frag;
		}

		push (@avgFrag, [ @avgTmp ]);
		push (@sdFrag,  [ @sdTmp ]);

		if ($cTotal > 0) {
			push (@avgExp, $lTotal/$cTotal);
		} else { 
			push (@avgExp, -1);
		}

   	$startIdx += $numReps;
   }

   return (\@avgFrag, \@avgExp, \@sdFrag);
}


################################################################################
# Subroutine: Sum
#
# Returns the sum of an array.
#
################################################################################
sub Sum{
   my @data = @_;
	
	my $sum = 0;
	foreach my $val (@data) { $sum += $val; }
	
   return ($sum);
}
