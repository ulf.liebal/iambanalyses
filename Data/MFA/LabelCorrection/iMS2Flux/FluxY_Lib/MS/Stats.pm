package FluxY_Lib::MS::Stats;

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw(MS_Average MS_StdDev);

################################################################################
# Subroutine: MS_Average
#
# Calculate a new list of data sets based on averaging each value in a number of
# consecutive MS data sets.
#
################################################################################
sub MS_Average {
   my($refN, $refD) = @_;
   
   my @numReps  = @{$refN};
   my @data     = @{$refD};
   my @averages = ();
   
	my $startIdx = 0;
   foreach my $num (@numReps) {
      my $size = @{$data[$startIdx]};
      
      my @avg = ();
      for (my $i=0; $i<$size; $i++) {
         my $sum = 0;
         for (my $j=0; $j<$num; $j++) {            
            $sum += $data[$startIdx+$j][$i];
         }
         
         push(@avg, $sum/$num);
      }
      
      push(@averages, [ @avg ]);

	   $startIdx += $num;
	}
	   
   return @averages;
}


################################################################################
# Subroutine: MS_StdDev
#
# Calculate a new list of data sets based on the standard deviation over a
# number of consecutive MS data sets (experimental replicates). An optional
# constant additive offset may be specified to weight the deviation.
#
# - for datasets with only one data value use the formula:  5% (Value) + offset
#
################################################################################
sub MS_StdDev {
   my($refN, $refD, $refA, $offset) = @_;
   
   my @numReps  = @{$refN};

   my @data    = @{$refD};
   my @avg     = @{$refA};
   my @stdDevs = ();
   
   if ( !defined($offset) ) { $offset = 0; }
   
   my $index    = 0;
	my $startIdx = 0;
   foreach my $num (@numReps) {
      
      my @aList =  @{$avg[$index++]};
      my @sList = ();
      for (my $i=0; $i<@aList; $i++) {
		   if ( $num == 1 ) {
            push(@sList, (0.05*$data[$startIdx][$i]) + $offset);
		   } else {
				my $sum = 0;
				for (my $j=0; $j<$num; $j++) {
					$sum += ($data[$startIdx+$j][$i] - $aList[$i])**2;
				}
				
				push(@sList, sqrt($sum/($num-1)) + $offset);
			}
      }
      
      push(@stdDevs, [ @sList ]);
		
	   $startIdx += $num;
   }
   
   return @stdDevs;
}


1;