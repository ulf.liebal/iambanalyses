package FluxY_Lib::MS::DataClass_GY;

################################################################################
# This class provides support for singly derivatized glycerol fragments
# - This class supports derivatization by either TMS or TFAA.
# - The class data is specific to a carbon labelling experiment.
#
# This class inherits basic functionality from the FragmentClass parent
# class, which extends CompoundClass, initializing it with the compound name, 
# glycerol fragments, and the atom list which are defined by this class.
# - It overrides the PrintInfoTable() method to specify the output file to be 
#   used by the parent method.
#
# Additional helper methods are implemented;
# - ExtractFragmentData(): Generates a Hash from one datum in the fragment list.
# - GenerateCBNum(): Generates a per fragment list of carbon backbone counts.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::FragmentClass);   # Sets the parent class.


# List of the atoms found in glycerol and subsequently considerred for natural
# abundance.
# Note: F & Si are included for convenience (commonly found in derivatizing agents).
my @atomList = ('C','H','O','F','Si');

# The atom counts in the following data structures are arrays of the number of
# each corresponding atom in the atomList, i.e. C, H, 0, F and Si.

# Hash of compound data, (first two array elements are required).
# 1. The compound names (glycerol), in lower case (for standard comparison).
# 2. The atom count for the glycerol compound.
my %compounds = (
   glyc => ["glycerol", [3,8,3,0,0]],
   );

# Hash of fragment info for each TMS derivatised glycerol fragment:
# - array 1: atom counts, and
# - array 2: the first and last positions of experimentally labelled atoms.
my %tmsFrags = (
   m000  => [[12,32,3,0,3], [1,3]], # M-0  - original mass, added for convenience.
	m015  => [[11,29,3,0,3], [1,3]], # M-15 - loss of a CH3.
	m090  => [[9,22,2,0,2], [1,3]],  # M-90 - loss of a (CH3)3SiOH.
   );

# Hash of fragment info for each TFAA derivatised glycerol fragment:
# - array 1: atom counts, and
# - array 2: the first and last positions of experimentally labelled atoms.
my %tfaaFrags = (
   m000  => [[9,5,6,9,0], [1,3]],   # M - original mass, added for convenience.
   m113  => [[7,5,4,6,0], [1,3]],   # M-113.
   m127  => [[6,3,4,6,0], [1,2]],   # M-127.
   );

my $lElement  = "C";                   # The labeled element.
my $className = "Glycerol (GY)";       # The name of the data class.
my $printFile = "GY_Data_Table.txt";   # The name of the info table file.

   
################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
# The detailed data is extracted by a private subroutine using the specific 
# fragment information from the fragment list identified by the passed
# derivatizing agent. This list contains the total atom list for each fragment,
# including derivatization by the specified agent. This must be done before
# calling the constructor of the parent class, as this is required by the parent
# class.
#
################################################################################
sub new {
   my ($class, $dAgent) = @_;

   # If not specified, set dAgent to TMS by default, otherwise check that the
   # derivatizing agent is supported (TMS, TFAA) for glycerol, or terminate.
   my $fragRef = "";
   if ( !(defined($dAgent)) ) {
      $dAgent  = "TMS";
      $fragRef = \%tmsFrags;
   } elsif ( lc($dAgent) eq 'tms' ) { 
      $fragRef = \%tmsFrags;
   } elsif ( lc($dAgent) eq 'tfaa' ) {
      $fragRef = \%tfaaFrags;
   } else {
      print "\n\n\tThe specified derivatizing agent, '" . $dAgent . "', could not be used.\n\n";
      print "\tCurrently only TMS and TFAA are supported for derivatized glycerol.\n\n";
   
      exit -1;
   }

   # Initialize the total atom and labeled backbone lists.
   my %totAtomCount = ExtractFragmentData($fragRef, 0);
   my %lBackbone    = ExtractFragmentData($fragRef, 1);
   my %cBackbone    = GenerateCBNum($fragRef);
   
   # Call the constructor of the parent class (FragmentClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement, \%totAtomCount, \%cBackbone, \%lBackbone, $dAgent);
   
   bless $self, $class;

   return $self;
}


###########################
#   Overridden methods.   #
###########################

################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# fragments of each compound, including its atomic make up, mass, labeled
# backbone and full name.
#
# Overridden to specify the output file to the parent method.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;

   $self->SUPER::PrintInfoTable($printFile);
}


########################
#   Private methods.   #
########################

################################################################################
# Subroutine: ExtractFragmentData
#
# Generates a single data list in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
# - The data is specified as one of the elements from the given derivatized
#   fragment list.
#
################################################################################
sub ExtractFragmentData {
   my ($fragsRef, $fragIndex) = @_;   
   
   my %frags = %{$fragsRef};
   
   my %outerList = ();
   foreach my $compound (sort keys %compounds) {
		my %innerlist = ();
		foreach my $frag (sort keys %frags) {      
			$innerlist{$frag} = $frags{$frag}[$fragIndex];
		}
				
		$outerList{$compound} = \%innerlist;
	}
   
   return %outerList;
}


################################################################################
# Subroutine: GenerateCBNum
#
# Generates a list of the number of carbons in the backbone fragment (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBNum {
   my ($fragsRef) = @_;   
   
   my %frags = %{$fragsRef};
   
   my %numCList = ();
   foreach my $compound (sort keys %compounds) {
		my %list = ();
		foreach my $frag (sort keys %frags) {
        my @temp = @{ $frags{$frag}[1] };
      
			$list{$frag} = $temp[1] - $temp[0] + 1;
		}
      
		$numCList{$compound} = \%list;
	}
   
   return %numCList;
}

1;