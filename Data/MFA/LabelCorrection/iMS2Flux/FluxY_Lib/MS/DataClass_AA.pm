package FluxY_Lib::MS::DataClass_AA;

################################################################################
# This class provides support for amino acid compound fragments, derivatized
# using TBDMS.
# - The class data is specific to a carbon labelling experiment.
#
# This class inherits basic functionality from the FragmentClass parent 
# class, which extends CompoundClass. 
# - It overrides the PrintInfoTable() method to add conflict and other info.
#
# Additional helper methods are implemented;
# - GenerateTotalAtomCount(): Generates a per fragment atom list, 
# - GenerateConflictList(): Generates a per fragment conflict list,
# - GenerateCBNum(): Generates a per fragment list of carbon backbone counts, and
# - GenerateCBPosition(): Generates a per fragment carbon backbone position list.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::MS::DAgent;          # User Lib - Provides Derivatizing Agent Info.

use parent qw(FluxY_Lib::MS::FragmentClass);   # Sets the parent class.


# List of the atoms found in amino acids and subsequently considerred for
# natural abundance. 
my @atomList = ('C','H','N','O','S','Si');

# The atom counts in the following data structures are arrays of the number of
# each corresponding atom in the atomList, i.e. C, H, N, O, S and Si.

# Hash of compound data, (first two array elements are required).
# 1. The compound names (amino acids), in lower case (for standard comparison).
# 2. The atom count for each Amino Acid (unfragmented backbone) compound.
# 3. The number of silyl groups for each Amino Acid, i.e. the number of dAgents
#    that attach to the Amino Acid.
# 4. Number of hydrogens added to the m302s side chain.
my %compounds = (
   ala => ["alanine",       [3,7,1,2,0,0],   2, 0],
   gly => ["glycine",       [2,5,1,2,0,0],   2, 0],
   val => ["valine",        [5,11,1,2,0,0],  2, 0],
   leu => ["leucine",       [6,13,1,2,0,0],  2, 0],
   ile => ["isoleucine",    [6,13,1,2,0,0],  2, 0],
   pro => ["proline",       [5,9,1,2,0,0],   2, 0],
   cys => ["cysteine",      [3,7,1,2,1,0],   3, 0],
   met => ["methionine",    [5,11,1,2,1,0],  2, 0],
   ser => ["serine",        [3,7,1,3,0,0],   3, 0],
   thr => ["threonine",     [4,9,1,3,0,0],   3, 0],
   phe => ["phenylalanine", [9,11,1,2,0,0],  2, 0],
   asp => ["aspartic acid", [4,7,1,4,0,0],   3, 0],
   glu => ["glutamic acid", [5,9,1,4,0,0],   3, 0],
   asn => ["asparagine",    [4,8,2,3,0,0],   3, 0],
   lys => ["lysine",        [6,14,2,2,0,0],  3, 0],
   gln => ["glutamine",     [5,10,2,3,0,0],  3, 0],
   arg => ["arginine",      [6,14,4,2,0,0],  5, 0],
   his3 => ["histidine",    [6,9,3,2,0,0],   3, 1],   # 3 refers to the number of silyl groups.
#   his2 => ["histidine",    [6,10,3,2,0,0],  2, 0],   # 2 refers to the number of silyl groups.
   tyr => ["tyrosine",      [9,11,1,3,0,0],  3, 0],
   trp => ["tryptophan",    [11,12,2,2,0,0], 3, 0],
   );

# Hash of fragment data for the major fragmented mass groups:
# 1. The array of relative fragment atom reductions.
# 2. The starting position of the fragment's carbon backbone (as an array for consistency).
# - current implementation is for TBDMS only
my %frags = (
   m000  => [[ 0, 0,0,0,0,0], [1]], # M - molecular mass, added for convenience.
   m015  => [[ 1, 3,0,0,0,0], [1]], # M-15
   m057  => [[ 4, 9,0,0,0,0], [1]], # M-57
   m085  => [[ 5, 9,0,1,0,0], [2]], # M-85
   m159  => [[ 7,15,0,2,0,1], [2]], # M-159
   m302s => [[14,32,1,2,0,2], [3]], # M-302 (loss of side chain)
   );

# M=302 is a common fragment to all AA (loss of R). It is seperated here as it
# is exactly the same for all compounds. In this case the first element is the
# actual atom count for this fragment, and the second element contains both the
# starting an ending position of the backbone carbons.
my %m302 = ( m302  => [[14,32,1,2,0,2], [1,2]] );

# A 2D Hash of fragment conflicts, indexed by compunds key and frags keys.
my %conflictList = ();

my $lElement  = "C";                   # The labeled element.
my $className = "Amino Acids (AA)";    # The name of the data class.
my $printFile = "AA_Data_Table.txt";   # The name of the info table file.


################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class, $dAgent) = @_;

   # If not specified, set dAgent to TBDMS by default, otherwise check that the
   # derivatizing agent is TBDMS, or terminate.
   if ( !(defined($dAgent)) ) {
      $dAgent = "TBDMS";
   } elsif ( lc($dAgent) ne 'tbdms' ) {
      print "\n\n\tThe specified derivatizing agent, '" . $dAgent . "', could not be used.\n\n";
      print "\tCurrently only TBDMS is supported for derivatized amino acids.\n\n";
   
      exit -1;
   }

   # Get the derivatizing agent info:
   my ($dName, $acRef) = &GetDAgent($dAgent, \@atomList);

   # Initialize the total atom and carbon backbone lists.
   my %totAtomCount = GenerateTotalAtomCount($acRef);
   my %lBackbone    = GenerateCBPosition();
   my %cBackbone    = GenerateCBNum(%lBackbone);

   # Call the constructor of the parent class (FragmentClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement, \%totAtomCount, \%cBackbone, \%lBackbone, $dAgent);
   
   bless $self, $class;
   
   # Initialize the compound conflict list(requires the mass list).
   $self->GenerateConflictList;

   return $self;
}


###########################
#   Overridden methods.   #
###########################

################################################################################
# Subroutine: PrintInfoTable
#
# Prints a table containing the atom count / fragment mass for each Amino Acid.
# The table contains a row for the derivatized amino acid and each of the major 
# fraction groups.
#
# Overridden to add extra information about conflicts and special cases.
#
################################################################################
sub PrintInfoTable {
   my ($self) = @_;

   open (FILE, ">" . $printFile) or FileSystemError(900, $printFile, $!);
   
   print FILE "Amino Acid Data Table:\n\n";
   print FILE "The derivatizing agent is: " . $self->GetDA_Name(0) . "\n";
   
   foreach my $compound ($self->GetCompoundList) {
      print FILE "\nFor: " . $compound . "\n";
   
      print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\n";

      foreach my $frag ($self->GetFragsList($compound)) {
         my @totAtoms         = $self->GetTotalAtomCount($compound, $frag);
		   my ($firstC, $lastC) = $self->GetLabeledBackbone($compound, $frag);
         my $mass             = $self->GetMass($compound,$frag);

         print FILE $frag;
         if ( ($frag eq "m302s") && (${ $self->{_compounds} }{$compound}[3]) ) { 
            print FILE "**";
         }
         
         print FILE "\t" .  join("\t", @totAtoms) . "\t" . $mass;
         if ( $conflictList{$compound}{$frag} ) { print FILE "*"; }
         
			print FILE "\t" . $firstC . "-" . $lastC . "\n";
      }
   }
   
   print FILE "\n*  Conflicting mass. Cannot uniquely identify labels in backbone.\n\t- Can correct for NA but not for OBM.\n\t- Should not be used when generating MFA measurement data.\n";
   print FILE "** Side chain binds to a free H, increasing the mass accordingly.\n";
   
   close(FILE)                 or FileSystemError(901, $printFile, $!);
}


########################
#   Private methods.   #
########################

################################################################################
# Subroutine: GenerateTotalAtomCount
#
# Generates a list of atom arrays in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
#
################################################################################
sub GenerateTotalAtomCount {
   my ($daRef) = @_;
   
   my @dAgent  = @{ $daRef };
   
   my %totalAtomList = ();
   foreach my $compound (sort keys %compounds) {
         
		my @totAtoms   = @{$compounds{$compound}[1]};
		my $multiplier =   $compounds{$compound}[2]; 

		for(my $i=0; $i<@totAtoms; $i++) {      
			$totAtoms[$i] += ($dAgent[$i] * $multiplier);

			# Correct for Hydrogen loss, 1 per dAgent:
			if ( $atomList[$i] eq "H" ) { $totAtoms[$i] -= $multiplier; }
		}
		
		my %list = ();
		foreach my $frag (sort keys %frags) {      
			my @newTotAtoms = ();
			for(my $i=0; $i<@totAtoms; $i++) {
            $newTotAtoms[$i] = $totAtoms[$i] - $frags{$frag}[0][$i];
				
				# For the m302s side chain, add extra binding hydrogen(s):
				if ( $frag eq "m302s") {
					if ( $atomList[$i] eq "H" ) { $newTotAtoms[$i] += ${$compounds{$compound}}[3]; }
				}
			}
		 
			$list{$frag} = \@newTotAtoms;
		}
		
      # Add the m302 atom count.
		my $frag = (keys %m302)[0];
      $list{$frag} = $m302{$frag}[0];
		
		$totalAtomList{$compound} = \%list;
	}
   
   return %totalAtomList;
}


################################################################################
# Subroutine: GenerateCBPosition
#
# Generates a list of carbon backbone position arrays in a hash (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBPosition {
   my %cbList = ();
   foreach my $compound (sort keys %compounds) {
   
		my %list = ();
		foreach my $frag (sort keys %frags) {
         my $first = $frags{$frag}[1][0];
         my $last  = $compounds{$compound}[1][0];

         my @cBackbone = ($first, $last);
			$list{$frag} = \@cBackbone;
		}
      
      # Add the m302 atom count.
		my $frag = (keys %m302)[0];
      $list{$frag} = $m302{$frag}[1];

		$cbList{$compound} = \%list;
	}
   
   return %cbList;
}


################################################################################
# Subroutine: GenerateCBNum
#
# Generates a list of the number of carbons in the backbone fragment (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBNum {
   my %positionList = @_;   
   
   my %numCList = ();
   foreach my $compound (sort keys %compounds) {
      my %fragList = %{ $positionList{$compound} };

		my %list = ();
		foreach my $frag (sort keys %fragList) {
			$list{$frag} = $fragList{$frag}[1] - $fragList{$frag}[0] + 1;
		}
      
		$numCList{$compound} = \%list;
	}
   
   return %numCList;
}


################################################################################
# Subroutine: GenerateConflictList
#
# Generates a list of fragments with conflicting mass entries for each compound.
# The list is a hash (indexed by compound key) of hashes (indexed by fragment 
# key).
#
################################################################################
sub GenerateConflictList {
   my ($self) = @_;

   foreach my $compound ($self->GetCompoundList) {
		
      my @masses = ();
      foreach my $frag ($self->GetFragsList($compound)) {
         push(@masses, $self->GetMass($compound, $frag));
      }
      
      my %list = ();
      foreach my $frag ($self->GetFragsList($compound)) {
         my $mass = $self->GetMass($compound,$frag);
         my $found = 0;
			for (my $i=0; $i<@masses; $i++) {
			   if ( $mass == $masses[$i] ) { $found++; }
			}
         $list{$frag} = $found - 1;
      }
      
      $conflictList{$compound} = \%list;
   }
}

1;