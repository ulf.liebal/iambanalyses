package FluxY_Lib::MS::GenerateTSV;

################################################################################
# This module provides support for generating TSV data files for easy access in
# spreadsheets and to third party tools.
# - These subroutines are not specific to MS values, but any data in the 2D array
# format used internally by the main program.
#
# The subroutines are:
# - PrintMatrix():    Generates one set of 2D tabbed seperated matrix output.
# - PrintMatrixNZR(): Similar to PrintMatrix, but only printing rows containing
# non-zero data.
# - PrintMatrixOPF(): Similar to Printmatrix but with only One value Per
# Fragment plus an optional summary value.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::IO::IO_Utils;          # User Lib - Provides I/O routines.

use base 'Exporter';

our @EXPORT = qw(PrintMatrix PrintMatrixNZR PrintMatrixOPF);

################################################################################
# Subroutine: PrintMatrix
#
# Generates one set of matrix output.
#
################################################################################
sub PrintMatrix{
   my ($title, $outFile, $refCH, $refRH1, $refRH2, $refData) = @_;
   my @colHeads = @{$refCH};
   my @rowHead1 = @{$refRH1};
   my @rowHead2 = @{$refRH2};
   my @data     = @{$refData};

   open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
   
   print FILE $title . "\n\t\t" . join("\t", @colHeads) . "\n";
   
   my $tmpStr = "";
   for (my $j=0; $j<@rowHead1; $j++) {
      if ($tmpStr ne $rowHead1[$j]) {
         print FILE $rowHead1[$j];
         $tmpStr = $rowHead1[$j];
      }
      print FILE "\t" . $rowHead2[$j];
      
      for (my $i=0; $i<@data; $i++) {
         if ( defined($data[$i][$j]) ) {
            print FILE "\t" . $data[$i][$j];
         } else {
            print FILE "\t";
         }
      }
      print FILE "\n";
   }

   close(FILE)                 or FileSystemError(901, $outFile, $!);
}


################################################################################
# Subroutine: PrintMatrixNZR
#
# Generates one set of matrix output, only printing rows that contain non-zero 
# data.
#
################################################################################
sub PrintMatrixNZR{
   my ($title, $outFile, $refCH, $refRH1, $refRH2, $refData) = @_;
   my @colHeads = @{$refCH};
   my @rowHead1 = @{$refRH1};
   my @rowHead2 = @{$refRH2};
   my @data     = @{$refData};

   open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
   
   print FILE $title . "\n\t\t" . join("\t", @colHeads) . "\n";
   
   my $tmpStr = "";
   for (my $j=0; $j<@rowHead1; $j++) {
      my $found = 0;
      for (my $i=0; $i<@data; $i++) {
         if ( !(defined($data[$i][$j])) ) { next; }
         if ( $data[$i][$j] ne "0") {
            $found = 1;
            last;
         }
      }      
      
      if ( !$found ) { next; }
      
      if ($tmpStr ne $rowHead1[$j]) {
         print FILE $rowHead1[$j];
         $tmpStr = $rowHead1[$j];
      }
      print FILE "\t" . $rowHead2[$j];

      for (my $i=0; $i<@data; $i++) {
         if ( defined($data[$i][$j]) ) {
            print FILE "\t" . $data[$i][$j];
         } else {
            print FILE "\t";
         }
      }
      print FILE "\n";
   }

   close(FILE)                 or FileSystemError(901, $outFile, $!);
}

################################################################################
# Subroutine: PrintMatrixOPF
#
# Generates one set of matrix output, printing only one value per fragment, plus
# an optional summary value at the end.
#
################################################################################
sub PrintMatrixOPF{
   my ($title, $outFile, $refCH, $refRH1, $refRH2, $refFragIndex, $refData, $refSummary) = @_;
   my @colHeads = @{$refCH};
   my @rowHead1 = @{$refRH1};
   my @rowHead2 = @{$refRH2};
   my @fIndex   = @{$refFragIndex};
   my @data     = @{$refData};
   my @summary  = @{$refSummary};

   open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);
   
   print FILE $title . "\n\t\t" . join("\t", @colHeads) . "\n";

   for (my $j=0; $j<(@fIndex-1); $j++) {

      print FILE $rowHead1[$fIndex[$j]] . "\t" . $rowHead2[$fIndex[$j]];

      for (my $i=0; $i<@data; $i++) {
         if ( defined($data[$i][$j]) ) {
            print FILE "\t" . $data[$i][$j];
         } else {
            print FILE "\t";
         }
      }

      print FILE "\n";
   }

   if (@summary) {
      print FILE "\n\t\t" . join("\t", @summary) . "\n";
   }

   close(FILE)                 or FileSystemError(901, $outFile, $!);
}

1;
