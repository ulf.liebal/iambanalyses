package FluxY_Lib::MS::CalcNA_Prob;

################################################################################
# This module provides the subroutines for calculating Natural Abundance using
# the naturally occuring probabilities of M+0 through M+2 stable isotopes for
# a given atom. Two forms of this function are provided, depending on whether
# there are more or less atoms than labels.
# - This calculation is only correct for masses without M-n stable isotopes.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Math::Math_Utils; # User Lib - Simple math functions.

use base 'Exporter';

our @EXPORT = qw(CalcNA_Prob);

################################################################################
# Subroutine: CalcNA_Prob
#
# Calculate the probability of natural abundance of 'm' labels in 'n' atoms.
# - if 'n'< 'm' a second function is called. 
# - Natural abundances probabilities of M+0 to M+2 are considerred.
#
################################################################################
sub CalcNA_Prob {
   my($n, $m, $probRef) = @_;
   
   # The case of having more labels than atoms is handled separately.
   if ( $m > $n ) {
      # The probability of more than one labelling with no atoms is always zero.
      if ( $n == 0 )   { return 0; }
      # A consequence of considerring natural abundances of isotopes upto M+2.
      if ( $m/$n > 2 ) { return 0; }
      
      return &Calc2($n, $m, $probRef);
   }
   
   my @Probs = (@{$probRef}, 0, 0);   # Pad for atoms with <3 stable isotopes.
   
   if ($m == 0) { return $Probs[0]**$n; }
   
   my $m2 = 0;
   my $m1 = $m;
   my $m0 = $n-$m;
   
   my $val = 0;
   for (; $m1>=0; $m0++, $m1-=2, $m2++) {
      $val += &FactDiv($n, $m0) * ($Probs[0]**$m0) * (($Probs[1]**$m1)/&Fact($m1)) * (($Probs[2]**$m2)/&Fact($m2));
   }

   return $val;
}


################################################################################
# Subroutine: Calc2
#
# Calculate the probability of natural abundance of 'm' labels in 'n' atom, 
# where 'n'< 'm'. 
# - Natural abundances probabilities of upto M+2 are considerred.
#
################################################################################
sub Calc2 {
   my($n, $m, $probRef) = @_;

   my @Probs = @$probRef;

	# To ensure there are probability values for M+1 and M+2, even if 0:
	push(@Probs,(0,0));
	
   # $m == 2:
   if ($m == 2) { return $Probs[2]; }

   # $m == 3:
   if ($m == 3) { return 2 * $Probs[1] * $Probs[2]; }
   
   # $m >= 4:
   my $m2 = 0;
   if ( ($m%2) != 0 ) { 
      $m2 = ($m-1)/2;
   } else {
      $m2 = $m/2;
   }
   my $m1 = $m - (2 * $m2);
   my $m0 = $n - $m1 - $m2;
      
   my $val = 0;
   for (; $m0>=0; $m0--, $m1+=2, $m2--) {
      $val += &FactDiv($n, $m1) * (($Probs[0]**$m0)/&Fact($m0)) * ($Probs[1]**$m1) * (($Probs[2]**$m2)/&Fact($m2));
   }
   
   return $val;
}


1;
