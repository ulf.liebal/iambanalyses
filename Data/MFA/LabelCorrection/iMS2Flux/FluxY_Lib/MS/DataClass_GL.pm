package FluxY_Lib::MS::DataClass_GL;

################################################################################
# This class provides support for doubly derivatized glucose compound fragments.
# - This class supports derivatization first by Meox (required) and then by a
#   second derivatizing agent, either TMS or AC2O.
# - This class expects to be initialized by specifying the second derivatizing
#   agent.
# - The class data is specific to a carbon labelling experiment.
#
# This class inherits basic functionality from the FragmentClass parent
# class, which extends CompoundClass, initializing it with the compound name, 
# glucose fragments, and the atom list which are defined by this class.
# - It overrides the PrintInfoTable() method to specify the output file to be 
#   used by the parent method.
#
# Additional helper methods are implemented;
# - ExtractFragmentData(): Generates a Hash from one datum in the fragment list.
# - GenerateCBNum(): Generates a per fragment list of carbon backbone counts.
#
################################################################################

use strict;
use warnings;

use parent qw(FluxY_Lib::MS::FragmentClass);   # Sets the parent class.


# List of the atoms found in glucose and subsequently considerred for
# natural abundance. 
my @atomList = ('C','H','O','N','Si');

# The atom counts in the following data structures are arrays of the number of
# each corresponding atom in the atomList, i.e. C, H, 0, N and Si.

# Hash of compound data, (first two array elements are required).
# 1. The compound names (glucose), in lower case (for standard comparison).
# 2. The atom count for the glucose compound.
my %compounds = (
   glc => ["glucose", [6,12,6,0,0]],
   );

# Hash of fragment info for each TMS derivatised glucose fragment:
# - array 1: atom counts, and
# - array 2: the first and last positions of experimentally labelled atoms.
my %tmsFrags = (
   m000  => [[22,55,6,1,5], [1,6]], # M - original mass, added for convenience.
   m250  => [[13,31,3,0,3], [3,6]], # M-250
   m352  => [[9,21,2,0,2], [4,6]],  # M-352
   m364  => [[8,21,2,0,2], [5,6]],  # M-364
   m409  => [[6,14,2,1,1], [1,2]],  # M-409
   m466  => [[4,11,1,0,1], [6,6]],  # M-466
   );

# Hash of fragment info for each Ac2O derivatised glucose fragment:
# - array 1: atom counts, and
# - array 2: the first and last positions of experimentally labelled atoms.
my %ac2oFrags = (
   m000  => [[17,25,11,1,0], [1,6]],  # M - original mass, added for convenience.
   m059  => [[15,22,9,1,0], [1,6]],   # M-59.
   m130  => [[12,17,8,0,0], [3,6]],   # M-130.
   m330  => [[3,7,2,1,0], [1,2]],     # M-330.
   );

my $lElement  = "C";                   # The labeled element.
my $className = "Glucose (GL)";        # The name of the data class.
my $printFile = "GL_Data_Table.txt";   # The name of the info table file.

	
################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
# The detailed data is extracted by a private subroutine using the specific 
# fragment information from the fragment list identified by the passed
# derivatizing agent. This list contains the total atom list for each fragment,
# including derivatization by both Meox and the second specified agent. This
# must be done before calling the constructor of the parent class, as this is
# required by the parent class.
#
################################################################################
sub new {
   my ($class, $dAgent) = @_;

   # If not specified, set dAgent to TMS by default, otherwise check that the
   # derivatizing agent is supported (TMS, Ac2O) for glucose, or terminate.
   my $fragRef = "";
   if ( !(defined($dAgent)) ) {
      $dAgent = "TMS";
      $fragRef = \%tmsFrags;
   } elsif ( lc($dAgent) eq 'tms' ) {
      $fragRef = \%tmsFrags;
   } elsif ( lc($dAgent) eq 'ac2o' ) {
		$fragRef = \%ac2oFrags;
	} else {
      print "\n\n\tThe specified derivatizing agent, '" . $dAgent . "', could not be used.\n\n";
      print "\tThis class always uses Meox as a first derivatizing agent, while the second \n\tderivatizing agent is specified in the configuration file.\n\n";
      print "\tCurrently only TMS and Ac2O are supported as the second derivatizing agent for \n\tuse with glucose.\n\n";
   
      exit -1;
   }

   my @dAgents = ("meox", $dAgent);
   
   # Initialize the total atom and labeled backbone lists.
   my %totAtomCount = ExtractFragmentData($fragRef, 0);
   my %lBackbone    = ExtractFragmentData($fragRef, 1);
   my %cBackbone    = GenerateCBNum($fragRef);

   # Call the constructor of the parent class (FragmentClass).
   my $self = $class->SUPER::new($className, \%compounds, \@atomList, $lElement, \%totAtomCount, \%cBackbone, \%lBackbone, \@dAgents);
   
   bless $self, $class;

   return $self;
}


###########################
#   Overridden methods.   #
###########################

################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# fragments of each compound, including its atomic make up, mass, labeled
# backbone and full name.
#
# Overridden to specify the output file to the parent method.
#
################################################################################
sub PrintInfoTable{
   my ($self) = @_;

   $self->SUPER::PrintInfoTable($printFile);
}


########################
#   Private methods.   #
########################

################################################################################
# Subroutine: ExtractFragmentData
#
# Generates a single data list in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
# - The data is specified as one of the elements from the given derivatized
#   fragment list.
#
################################################################################
sub ExtractFragmentData {
   my ($fragsRef, $fragIndex) = @_;   
   
   my %frags = %{$fragsRef};
   
   my %outerList = ();
   foreach my $compound (sort keys %compounds) {
		my %innerlist = ();
		foreach my $frag (sort keys %frags) {      
			$innerlist{$frag} = $frags{$frag}[$fragIndex];
		}
				
		$outerList{$compound} = \%innerlist;
	}
   
   return %outerList;
}


################################################################################
# Subroutine: GenerateCBNum
#
# Generates a list of the number of carbons in the backbone fragment (indexed by
# compound key) of hashes (indexed by fragment key).
#
################################################################################
sub GenerateCBNum {
   my ($fragsRef) = @_;   
   
   my %frags = %{$fragsRef};
   
   my %numCList = ();
   foreach my $compound (sort keys %compounds) {
		my %list = ();
		foreach my $frag (sort keys %frags) {
        my @temp = @{ $frags{$frag}[1] };
      
			$list{$frag} = $temp[1] - $temp[0] + 1;
		}
      
		$numCList{$compound} = \%list;
	}
   
   return %numCList;
}

1;