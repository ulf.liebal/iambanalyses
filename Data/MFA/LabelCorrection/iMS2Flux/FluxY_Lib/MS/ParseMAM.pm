package FluxY_Lib::MS::ParseMAM;

################################################################################
# This module provides support for parsing Mass Spectrometry data exported using
# the Mathias Agilent Macros (MAM) to Excel, and saved in a tab separated values
# text file.
#
# This input method allows for a one to many mapping whereby a single compound
# name can map to multiple keywords in the compound class (notably the soluable
# metabolites (SM) class. In this case an entry is created for all keywords,
# even if their specific mass is not present.
# TODO - check for missing masses and remove entries from all relevant lists.
#
# This method of input is for data to be processed, and currently negates many 
# of the configuration settings and flags - those dealing with retention time, 
# and selecting/printing Area/Response data. In this format there is only one
# value provided per mass isotopomer, this is stored in the 'dataResps' array.
# The 'dataAreas' and 'dataRT' arrays are left uninitialized, and the dataFlags
# array is filled with dummy values.
#
################################################################################

#use strict;
#use warnings;

use FluxY_Lib::Data::Data_Check;    # User Lib - Handles basic data checking.
use FluxY_Lib::Data::Data_Utils;    # User Lib - Provides Transpose TSL.
use FluxY_Lib::IO::IO_Utils;        # User Lib - Provides I/O routines.
use FluxY_Lib::MS::ErrorsAndHelp;   # User Lib - Prints GCMS related messages.

use base 'Exporter';

our @EXPORT = qw(ParseAgilentInput);

################################################################################
# Subroutine: ParseAgilentInput
#
# Extracts MAM data from a tab separated values file, where the data is oriented
# vertically, i.e. data is represented down columns. Each data set is requires
# two columns, and are separated by a single blank column.
# - The first column contains the compound name, followed by the list of
# fragment masses.
# - The second column contains the retention time, followed by a list of 
# measurements, one for each compound/fragment mass isotopomer.
#
# The final column of data contains the names of each of the experimental data
# sets. This is also seperated by a single blank column.
#
# Thus for n>=1 data sets there are < 2+3(n-1)+2 = 1+3n > columns of data.
#
# Any compounds that are missing are first removed.
# For easier processing, the data is then converted to a horizontal orientation.
# The resulting data is seperated into its components, and processed.
#
################################################################################
sub ParseAgilentInput{
   my ($dataFile, $msType, $m_1Flag, $mp1Flag, $scanFlag) = @_;
   
   my @cNames    = ();   # The array of compund names.
   my @cMasses   = ();   # The array of compund masses.
   my @sNames    = ();   # The array of sample names.
   my @dataResps = ();   # The array of MS measurements.
   my @dataAreas = ();   # Second MS quantification method (not used with MAM, left empty).
   my @dataRT    = ();   # The array of retention times (not used with MAM, left empty).
   my @dataFlags = ();   # Detection flags for each data set (not used in MAM, dummy filled).

   my $i = 0;
   my $j = 0;
   
   # Read in file from Matti, skipping blank lines.
   my @fLines = &ReadFileSkipB($dataFile);

   #Initialize the list of sample names:
   my @headers = split(/\s/, shift(@fLines));
   foreach my $item (@headers) {
      if ($item ne "") { push (@sNames, $item); }
   }

   # Transpose data for easier manipulation.
   @fLines = &TransposeTSL(@fLines);

   # Skip blank lines in new orientation (i.e. the spacer columns).
   my @lines = ();
   foreach my $fLine (@fLines) {
      if ( $fLine =~ /^\s*$/ ) { next; }            #Skip blank lines.
      push(@lines, $fLine);
   }
	
   # Find starting positions for each fragment (last entry is EOF+1), and
   # initialize the list of compound names.
	# The keyword index (kIndex) was added to support multiple compounds sharing
	# the same name in the simple format of the soluable metabolite (SM) class. 
	# This is not required for a fragment class such as amino acids (AA).
   # Note: the compound name is array element 0 in the compounds hash.
   my @cells   = split (/\t/, $lines[0]);
	my @keyList = ();
   my @fIndex  = ();
   my @kIndex  = ();
   my @unknown = ();
   for ($i=0; $i < @cells; $i++) {
		if ( &CheckIfNumber($cells[$i], "D") ) { next; }
		
      my $found = 0;
      foreach my $compound ($msType->GetCompoundList()) {		
         if ( lc($msType->GetCompoundName($compound)) eq lc($cells[$i]) ) {
				# Add to index the first time a compound name is identified.
   			if ( !$found ) {
					push(@fIndex, $i);
					push(@kIndex, scalar(@keyList));
				}
				# Add all key words corresponding to the compound name.
            push(@keyList, $compound);
            $found++;
         }			
      }
      
      if ( !$found && ($cells[$i] !~ /^\s*$/) ) { push(@unknown, $cells[$i]) };
   }	
	push(@fIndex, $i);
	push(@kIndex, scalar(@keyList));
	
	# Build a mass index set with one start and one stop value for each item in 
	# the keyword list. If there is more than one entry in the keyword index, 
	# then the mass range is repeated.
	my @massStart = ();
	my @massStop  = ();
   for ($i=0; $i<@kIndex-1; $i++) {
		for ($j=$kIndex[$i]; $j < $kIndex[$i+1]; $j++) {
			push(@massStart, $fIndex[$i]);
			push(@massStop,  $fIndex[$i+1]);
		}
	}
	
   if ( @unknown > 0 ) { &Warning(20, @unknown); }
   	
   # Separate data into related matrices: masses and data.
   my @masses = ();
   my @data   = ();
   for ($i=0; $i < @lines; $i+=2) {
      push(@masses, $lines[$i]);
      push(@data,   $lines[$i+1]);
   }

   # Find missing data points (M+0-M+numLabeled+offset).
   my @fIndex2 = ();
   foreach my $compound (@keyList) {
      push(@fIndex2, scalar(@cMasses));
      
      foreach my $mass (GetSortedMassList($msType, $compound)) {
         if ( &CheckExcluded($compound, $mass) ) { next; }
      
         my $numLabeled = $msType->GetNumFragmentedLabeledElement($compound, $mass) + $m_1Flag + $mp1Flag;
         for (my $k=0; $k<=$numLabeled; $k++) { push(@cMasses, $mass-$m_1Flag+$k); }
      }   
   }
   push(@fIndex2, scalar(@cMasses));

   #Initialize the list of compound names (1 per mass fragment):
   for ($i=0; $i < @fIndex2-1; $i++) {
      for ($j=$fIndex2[$i]; $j<$fIndex2[$i+1]; $j++) {  
         push (@cNames, $keyList[$i]);
      }
   }

   #Initialize the data values matrix to zero and data flags matrix to blanks.
   for ($i=0; $i<@sNames; $i++) {
      my @temp  = ();
      my @temp2 = ();

      for ($j=0; $j<@cNames; $j++) {
         push(@temp, 0);
         push(@temp2, "");
      } 
      
      push (@dataResps, [ @temp ]);
      push (@dataFlags, [ @temp2 ]);
   }

   #Set the data values matrix with non zero entries
   for ($j=0; $j<@fIndex2-1; $j++) { 	# For each set of masses
		
		for (my $k=$fIndex2[$j]; $k<$fIndex2[$j+1]; $k++) {    # For each mass in a set 
			for ($i=0; $i<@sNames; $i++) {                       # For each Experiment
				my @dCells = split (/\t/, $data[$i]);
				my @mCells = split (/\t/, $masses[$i]);



				for (my $l=$massStart[$j]; $l<$massStop[$j]; $l++) {  # For each mass measurement
					if ( $mCells[$l] == $cMasses[$k] ) {
						$dataResps[$i][$k] = $dCells[$l];
						last;
					}
				}
			}
		}
			
   }
   
   return(\@cNames, \@cMasses, \@sNames, \@dataResps, \@dataAreas, \@dataRT, \@dataFlags);
}


################################################################################
# Subroutine: GetSortedMassList
#
# Extracts the list of fragment masses for the given compound, and returns the
# list in ascending order.
#
################################################################################
sub GetSortedMassList {
   my ($msType, $compound) = @_;
      
   my @list = ();
   foreach my $frag ($msType->GetFragsList($compound)) {   
      push(@list, $msType->GetMass($compound, $frag));
   }
   @list = sort { $a <=> $b } @list;
      
   return @list
}


################################################################################
# Subroutine: CheckExcluded
#
# Checks the excluded list for each compound type.
#
################################################################################
sub CheckExcluded {
   my ($name, $mass) = @_;

   # Amino Acids: Molecular ion (M-0) and M-302 (except His3), plus all co-elluting M=302.
   my %excluded = (
   ala => [317, 302, 15],
   arg => [745, 442],
   asn => [474, 172],
   asp => [475, 173],
   cys => [463, 161],
   gln => [488, 186],
   glu => [489, 187],
   gly => [303, 302, 1],
   his3=> [497],
   ile => [359, 302, 57],
   leu => [359, 302, 57],
   lys => [488, 186],
   met => [377, 75],
   phe => [393, 91],
   pro => [343, 41],
   ser => [447, 145],
   thr => [461, 302, 159],
   trp => [547, 244],
   tyr => [523, 221],
   val => [345, 43],
   );

   if ( exists $excluded{$name} ) {
      @excluded = @{ $excluded{$name} };
    
      foreach $excludedMass (@excluded) {
         if ( $excludedMass == $mass ) { return 1; }
      }
   }
   
   return 0;
}

1;