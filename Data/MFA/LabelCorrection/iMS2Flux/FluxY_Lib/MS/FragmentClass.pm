package FluxY_Lib::MS::FragmentClass;

################################################################################
# This module provides support to the specific compound data classes with 
# multimle compounds each with multiple fragments. Each specific data class 
# provides the details of a given compound and overrides, as necessary, the
# provided methods.
# - The methods in this class are a fully generalized implementation of a 
#   fragmented compound where the 2D total atom list hash is provided by the
#   specific child data class.
# - This class also provides optional support for multiple derivatizing agents.
# - This class extends the basic single fragment class, i.e. CompoundClass, and 
#   overrides the following methods;
#   - GetFragsList(),
#   - GetMass(),
#   - Mass2Frag(),
#   - GetTotalAtomCount(),
#   - GetNumFragmentedLabeledElement();
#   - GetNumFragmentedCarbons(),
#   - GetLabeledPositionString(),
#   - GenerateMassList(), and
#   - PrintMassTables().
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Chemistry::atomInfo; # User Lib - Atomic Masses.
use FluxY_Lib::Data::Data_Check;    # User Lib - Basic configuration data check.
use FluxY_Lib::MS::DAgent;          # User Lib - Provides Derivatizing Agent Info.
use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple math functions.

use FluxY_Lib::MS::CompoundClass;       # User Lib - The common parent class.

our @ISA = qw(FluxY_Lib::MS::CompoundClass);    # Inherits from CompoundClass.


################################################################################
# Class constructor
#
# Used to instantiate an object of the fragment class, which extends the
# compound class adding the following data,
# _dAgentNames    # Array: The name(s) of the derivatizing agent(s).
# _totalAtomCount # HASH: A 2D Hash of the atom count.
#    Indexed by compunds key and frags keys.
#    Each element is an array, in the order specified by atomList.
# _cBackbone      # HASH: A 2D Hash of the fragment backbone for carbon.
#    Indexed by compunds key and frags keys.
#    Each element is a single value of the number of carbons in the fragmented
#    compound backbone.
# _lBackbone      # HASH: A 2D Hash of the fragment backbone of the labeled element.
#    Indexed by compunds key and frags keys.
#    Each element is an array, containing two elements representing the first
#    and last element positions relative to the original compound backbone.
#
# The constructor passes the first three parameters directly to the parent
# compound class, then uses the next three to initiallize the addedd three data.
# After the object is instantiated (i.e. blessed) the mass list, which is first
# set in the compound class, is reset to use the fragment data.
#
# param: to maintain compatibility $daRef may be either a scalar value or an
#        array reference.
# param: the $roundFlag, if set, indicates that the fragment masses should be
#        rounded instead of truncated (by default).
#
################################################################################
sub new {
   my ($class, $type, $cRef, $aRef, $lElement, $tacRef, $cbRef, $lbRef, $daRef, $roundFlag) = @_;
   
   # Call the constructor of the parent class (CompoundClass).
   my $self = $class->SUPER::new($type, $cRef, $aRef, $lElement);
   
   $self->{_totalAtomCount} = $tacRef;
   $self->{_cBackbone} = $cbRef;
   $self->{_lBackbone} = $lbRef;

   # Check if param $daRef is an Array reference, otherwise it is a scalar value
   my @dAgentList = (ref($daRef) eq 'ARRAY') ? @{ $daRef } : ($daRef);
   
   my @dAgentNames = ();
   foreach my $dAgent (@dAgentList) {
      my ($dName) = &GetDAgent($dAgent);
      if ( $dName ne "") { push(@dAgentNames, $dName); }
   }

   $self->{_dAgentNames} = \@dAgentNames;

   
   bless $self, $class;

   # Set the mass list to use fragment masses.
   $self->{_massList} = $self->GenerateMassList($roundFlag);
   
   return $self;
}


################################################################################
# Subroutine: GetLabeledBackbone
#
# Returns the first and last position in the labeled backbone corresponding to 
# the specified fragment of the compound.
#
################################################################################
sub GetLabeledBackbone {
   my ($self, $compound, $frag) = @_;
   
   if ( !($self->CheckFragment($compound, $frag)) ) { return; }
   
   return @{${ $self->{_lBackbone} }{$compound}{$frag}};
}


################################################################################
# Subroutine: GetDA_Names
#
# Returns the array of derivatizing agent names.
#
################################################################################
sub GetDA_Names {
   my ($self) = @_;

   return @{ $self->{_dAgentNames} };
}


################################################################################
# Subroutine: GetDA_Name
#
# Returns the requested derivatizing agent name.
#
################################################################################
sub GetDA_Name {
   my ($self, $i) = @_;

   return ${ $self->{_dAgentNames} }[$i];
}


################################################################################
# Subroutine: CheckFragment
#
# Checks that the specified fragment is valid.
# - First the compound key is independently verified and then that a value for
# the fragment is specified and it exists in the _totalAtomCount data hash.
#
################################################################################
sub CheckFragment {
   my ($self, $compound, $frag) = @_;

   # Check that the specified compound key is valid:
   if ( !($self->CheckCompound($compound)) ) { return; }

   # Check that the fragment key is specified:
   if ( !(defined $frag) ) { return 0; }
   
   # Check that the specified fragment is a valid key:
   if ( !(exists  ${ $self->{_totalAtomCount} }{$compound}{$frag}) ) { return 0; }

   return 1;
}


########################################################
#                                                      #
#   Overridden CompoundClass methods.                  #
#                                                      #
#   - to support the derivating agent and fragments.   #
#                                                      #
########################################################

################################################################################
# Subroutine: GetFragsList
#
# Returns the array of hash keys for the fragment list.
#
################################################################################
sub GetFragsList {
   my ($self, $compound) = @_;

   return (sort keys %{${ $self->{_totalAtomCount} }{$compound}});
}


################################################################################
# Subroutine: GetMass
#
# Returns the mass corresponding to the given compound.
#
# Overridden to use the atom count from the fragment instead of the compound.
#
################################################################################
sub GetMass {
   my ($self, $compound, $frag) = @_;
   
   if ( !($self->CheckFragment($compound, $frag)) ) { return; }

   return ${ $self->{_massList} }{$compound}{$frag};
}


################################################################################
# Subroutine: Mass2Frag
#
# Returns the key corresponding to the specified mass of the given compound.
# - In the case of two or more fragments having the same mass the first 
#   fragment in alphabetical order (based on the fragment identifier key sort) 
#   is returned.
#
################################################################################
sub Mass2Frag {
   my ($self, $compound, $fragMass) = @_;
   
   if ( !($self->CheckCompound($compound)) ) { return; }

   foreach my $frag ($self->GetFragsList($compound)) {
      if ($fragMass == $self->GetMass($compound, $frag) ) { return $frag; }
   }
   
   return;
}


################################################################################
# Subroutine: GetTotalAtomCount
#
# Returns the total atom count corresponding to the specified fragment of the 
# compound.
#
# Overridden to use the atom count from the fragment instead of the compound.
#
################################################################################
sub GetTotalAtomCount {
   my ($self, $compound, $frag) = @_;
   
   if ( !($self->CheckFragment($compound, $frag)) ) { return; }
   
   return @{ ${ $self->{_totalAtomCount} }{$compound}{$frag} };
}


################################################################################
# Subroutine: GetNumFragmentedLabeledElement
#
# Returns the number of fragmented backbone atoms for the labeled element of the
# specified compound and fragment.
# - If the element is not found in the atom list, returns the flag: -1.
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedLabeledElement {
   my ($self, $compound, $fragID) = @_;

   my $frag = $fragID;   
   if ( CheckIfNumber($fragID, "P") ) {
      $frag = $self->Mass2Frag($compound, $fragID);
   }
   
   # Check that the fragment mass was valid:
   if ( !(defined $frag) ) { return; } 
   
   my ($first,$last) = $self->GetLabeledBackbone($compound, $frag);
   
   return ( ($last - $first + 1) );
}


################################################################################
# Subroutine: GetNumFragmentedCarbons
#
# Returns the number of backbone carbon atoms for the specified compound and
# fragment. This is used for the OBM correction.
#
# Overridden to use the cabon backbone specified for each fragment.
#
################################################################################
sub GetNumFragmentedCarbons{
   my ($self, $compound, $fragID) = @_;

   my $frag = $fragID;   
   if ( CheckIfNumber($fragID, "P") ) {
      $frag = $self->Mass2Frag($compound, $fragID);
   }
   
   # Check that the fragment mass was valid:
   if ( !(defined $frag) ) { return; }

   return (${ $self->{_cBackbone} }{$compound}{$frag});
}


################################################################################
# Subroutine: GetLabeledPositionString
#
# Returns the relative positional string of the labeled element for the given
# compound and fragment.
#
# Overridden to use the labeled backbone specified for each fragment.
#
################################################################################
sub GetLabeledPositionString{
   my ($self, $compound, $fragID) = @_;

   my $frag = $fragID;   
   if ( CheckIfNumber($fragID, "P") ) {
      $frag = $self->Mass2Frag($compound, $fragID);
   }
   
   # Check that the fragment mass was valid:
   if ( !(defined $frag) ) { return; } 
   
   my ($first,$last) = $self->GetLabeledBackbone($compound, $frag);
   
   return ($self->GetPositionString($first, $last));
}


################################################################################
# Subroutine: GenerateMassList
#
# Generates a list of mass values in a hash (indexed by compound key) of hashes
# (indexed by fragment key).
#    - Each element is the truncated mass (by default, but may be rounded if 
#      round flag is set) of the corresponding fragment.
#
# Overridden to loop through the fragment list and to use the atom count from 
# the fragment instead of the compound.
#
################################################################################
sub GenerateMassList {
   my ($self, $roundFlag) = @_;

   my @atomMass = &GetElementMasses($self->GetAtomList);

   my %massList;
   foreach my $compound ($self->GetCompoundList) {
      my %list = ();
      
      foreach my $frag ($self->GetFragsList($compound)) {
         my @totAtoms = $self->GetTotalAtomCount($compound, $frag);

         my $mass = 0;
         for(my $i=0; $i<@totAtoms; $i++) {
            $mass += ($totAtoms[$i] * $atomMass[$i]);
         }
         
         if ( !$roundFlag ) {
            $list{$frag} = &Trunc($mass);
         } else {
            $list{$frag} = &Round($mass);
         }
      }
      
      $massList{$compound} = \%list;
   }
   
   return \%massList;
}


################################################################################
# Subroutine: PrintInfoTable
#
# This method generates a file containing an information table for the specific
# fragments of each compound, including its atomic make up, mass, and labeled
# backbone and full name. 
#
# Overridden to loop through the fragment list and to use the atom count and
# labeled backbone from the fragment instead of the compound.
#
################################################################################
sub PrintInfoTable{
   my ($self, $outFile, $title) = @_;   

   if ( !defined($outFile) ) { $outFile = "Data_Table.txt"; }
   if ( !defined($title) )   { $title   = $self->GetDataType; }

   open (FILE, ">" . $outFile) or FileSystemError(900, $outFile, $!);

   print FILE $title . " Data Table\n\n";
   
   my @daNames = $self->GetDA_Names;
   for (my $i=0; $i<@daNames; $i++) {
      print FILE "Derivatizing agent " . ($i+1) . " is: " . $daNames[$i] . "\n";
   }
   
   foreach my $compound ($self->GetCompoundList) {
      print FILE "\nFor: " . $compound . " (" . $self->GetCompoundName($compound) . ")\n";
   
      print FILE "\t" . join("\t", $self->GetAtomList) . "\tMass\tC-Backbone\n";
            
      foreach my $frag ($self->GetFragsList($compound)) {
         my @totAtoms       = $self->GetTotalAtomCount($compound, $frag);
         my ($first, $last) = $self->GetLabeledBackbone($compound, $frag);
         my $mass           = $self->GetMass($compound, $frag);

         print FILE $frag . "\t" . join("\t", @totAtoms) . "\t" . $mass . "\t" . $first . "-" . $last . "\n";
      }
   }
   
   close(FILE)                 or FileSystemError(901, $outFile, $!);
}

1;