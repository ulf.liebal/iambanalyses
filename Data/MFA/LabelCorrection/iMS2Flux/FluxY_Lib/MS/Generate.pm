package FluxY_Lib::MS::Generate;

################################################################################
# This module provides supportfor generating Mass Spectrometry output ready for
# inclusion in third party MFA models. Each specific generate class provides the
# details of a given MFA software model and implements the provided methods.
################################################################################

use strict;
use warnings;

use base 'Exporter';

my $modelType = "";   # The name of the MFA model type.
my @headers   = ();   # Section headers for the corresponding model file.


################################################################################
# Class constructor
#
# Used to instantiate an object for this class.
#
################################################################################
sub new {
   my ($class, $hRef, $type) = @_;
    
   @headers    = @{$hRef};
	$modelType  = $type;
    
   my $self = bless {}, $class;
    
   return $self;
}


################################################################################
# Subroutine: Generate
#
# For a given set of measurements and errors, this will print out the 
# measurements and error sections, one per averaged set of data, ready for
# insertion into the appropriate model file.
#
################################################################################
sub Generate{}


################################################################################
# Subroutine: GenerateModels
#
# Generates a set of model files, one for each combination of model and
# measurement.
#
################################################################################
sub GenerateModels{}


################################################################################
# Subroutine: GetSectionIndex
#
# Given a model file, this will find the indexes corresponding to the specified
# section headers.
#
################################################################################
sub GetSectionIndex{
   my ($self, @lines) = @_;
   
   my @sectionIdx = ();            # Index of start line for each section.
   my $hIndex     = 0;
   for (my $i=0; $i<@lines; $i++) {
      if ( $lines[$i] =~ $headers[$hIndex] ) {
         push(@sectionIdx, $i);
         $hIndex++;
			
			if ( $hIndex == @headers ) { last;}
      }
   }

   return @sectionIdx;
}


################################################################################
# Subroutine: GetIndex
#
# Given an array and a target value, this returns an index of each occurance of
# the target in the array.
#
################################################################################
sub GetIndex{
   my ($self, $target, @lines) = @_;
   
   my @index = ();            # Index of start line for each section.
   for (my $i=0; $i<@lines; $i++) {
      if ( $lines[$i] =~ $target ) { push(@index, $i); }
   }

   return @index;
}

1;