package FluxY_Lib::Data::Data_Utils;

################################################################################
# This module provides support for basic data utilities.
#
# TransposeTSL - Transposes a matrix of generic data provided as an array of
#              tab separated lines. 
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple arithmetic functions.

use base 'Exporter';

our @EXPORT = qw(TransposeTSL);

################################################################################
# Subroutine: TransposeTSL
#
# Transposes matrix data in the format of tab separated lines.
# Returns the transposed data in the same format.
#
################################################################################
sub TransposeTSL{
   my (@lines) = @_;

   # Create 2D data matrix.
   my @data2D = ();
   foreach my $line (@lines) {
      push (@data2D, [ split (/\t/, $line) ]);
   }
      
   # Transpose 2D data array.
   my @dataMatrix = &Transpose(@data2D);
   
   # Re-create tab separated form, with transposed data.
   @lines = ();
   for (my $i=0; $i<@dataMatrix; $i++) {
      my @tmp = @{$dataMatrix[$i]};
      
      my $str = $tmp[0];
      for (my $j=1; $j<@tmp; $j++) {
         $str .= "\t";
         if ( defined($tmp[$j]) ) { $str .= $tmp[$j]; }
      }      
      
      push (@lines, $str);
   }
  
   return (@lines);
}


1;
