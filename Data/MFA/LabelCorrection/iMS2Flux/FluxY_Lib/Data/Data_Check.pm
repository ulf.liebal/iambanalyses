package FluxY_Lib::Data::Data_Check;

################################################################################
# This module provides support for basic data checking, the error messages refer
# to the location in the "configuration file", but otherwise there is nothing
# file specific in the supplied checks.
#
################################################################################

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw(CheckFileName CheckFileExtension CheckIfNumber CheckLLimit CheckULimit CheckLimits CheckValue CheckOptions CheckOptionsYN CheckEmail);

################################################################################
# Subroutine: CheckFileName
#
# Parameter check: passed string is a valid filename.
#
################################################################################
sub CheckFileName {
   my ($var, $index) = @_;
   
   if ( !(-e $var) ) { &CheckError(120, $var, $index); }

   return $var;
}


################################################################################
# Subroutine: CheckFileExtension
#
# Parameter check: passed string is a filename with one of set of valid file
#                  extensions.
#
# All strings are converted and compared in upper case, if valid the filename
# is returned in two part, the name and the extension.
#
################################################################################
sub CheckFileExtension {
   my ($var, $index, @list) = @_;
   
   my @parts = split(/\./, $var);

   my $ext  = uc(pop(@parts));
   my $name = join("\.", @parts);

   for (my $i=0; $i < @list; $i++) {
      if ($ext eq uc($list[$i])) {
         return ($name, $ext);
      }
   }
   
   &CheckError(121, $var, $index, @list);
}


################################################################################
# Subroutine: CheckIfNumber
#
# Parameter check: passed variable is some type of number.
# P - Positive Integer
# N - Non-negative Integer
# I - Integer number
# D - Decimal number
# F - Floating Point number (C-style)
#
################################################################################
sub CheckIfNumber {
   my ($var, $type) = @_;
   
   $type = uc($type);
   
   if ( $type eq "P" ) {
      return ( ($var =~ /^\d+$/) && ($var > 0) )?1:0;
   } elsif ( $type eq "N" ) {
      return ($var =~ /^\d+$/)?1:0;
   } elsif ( $type eq "I" ) {
      return ($var =~ /^[+-]?\d+$/)?1:0;
   } elsif ( $type eq "D" ) {
      return ($var =~ /^-?(?:\d+(?:\.\d*)?|\.\d+)$/)?1:0;
   } elsif ( $type eq "F" ) {
      return ($var =~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/)?1:0;
   }
   
   &CheckError(130, $type);
}


################################################################################
# Subroutine: CheckLLimit
#
# Parameter check: passed value is a number greater than or equal to a value.
#
################################################################################
sub CheckLLimit {
   my ($var, $index, $min, $type) = @_;

   if ( !&CheckIfNumber($var, $type) ) { &CheckError(135, $var, $index, $type); }
   if ( !&CheckIfNumber($min, $type) ) { &CheckError(136, $min, "specified as a lower limit in the call to CheckLLimit", $type); }

   if ($var < $min) { &CheckError(140, $var, $index, $min); }
   
   return $var;
}


################################################################################
# Subroutine: CheckULimit
#
# Parameter check: passed value is a number less than or equal to a value.
#
################################################################################
sub CheckULimit {
   my ($var, $index, $max, $type) = @_;

   if ( !&CheckIfNumber($var, $type) ) { &CheckError(135, $var, $index, $type); }
   if ( !&CheckIfNumber($max, $type) ) { &CheckError(137, $max, "specified as a upper limit in the call to CheckULimit", $type); }
   
   if ($var > $max) { &CheckError(141, $var, $index, $max); }
   
   return $var;
}


################################################################################
# Subroutine: CheckLimits
#
# Parameter check: passed value is a number between a set of limits (inclusive).
#
################################################################################
sub CheckLimits {
   my ($var, $index, $min, $max, $type) = @_;

   if ( !&CheckIfNumber($var, $type) ) { &CheckError(135, $var, $index, $type); }
   if ( !&CheckIfNumber($min, $type) ) { &CheckError(136, $min, "specified as a lower limit in the call to CheckLimits", $type); }
   if ( !&CheckIfNumber($max, $type) ) { &CheckError(137, $max, "specified as a lower limit in the call to CheckLimits", $type); }
   
   if ( ($var < $min) || ($var > $max) ) { &CheckError(142, $var, $index, $min, $max); }
   
   return $var;
}


################################################################################
# Subroutine: CheckValue
#
# Parameter check: passed value is a number equal to the specified value.
#
################################################################################
sub CheckValue {
   my ($var, $index, $val, $type) = @_;

   if ( !&CheckIfNumber($var, $type) ) { &CheckError(135, $var, $index, $type); }
   if ( !&CheckIfNumber($val, $type) ) { &CheckError(136, $val, "specified as the value in the call to CheckValue", $type); }
   
   if ( $var != $val ) { &CheckError(143, $var, $index, $val); }
   
   return $var;
}


#################################################################################
# Subroutine: CheckOptions
#
# Parameter check: passed string is one of set of valid characters.
#
# All strings are converted and compared in upper case, if valid the parameter
# is returned in upper case.
#
################################################################################
sub CheckOptions {
   my ($var, $index, @list) = @_;
   
   $var = uc($var);
   
   for (my $i=0; $i < @list; $i++) {
      if ($var eq uc($list[$i])) {
         return $var;
      }
   }
   
   &CheckError(150, $var, $index, @list);
}


#################################################################################
# Subroutine: CheckOptionsYN
#
# Parameter check: passed string is one of Y or N (returns 1 or 0).
#
################################################################################
sub CheckOptionsYN {
   my ($var, $index) = @_;

   if ( &CheckOptions($var, $index, "Y", "N") eq "Y" ) { return 1; }   
   
   return 0;
}


#################################################################################
# Subroutine: CheckEmail
#
# Parameter check: passed string is in the form of a valid email address.
#
################################################################################
sub CheckEmail {
   my ($var, $index) = @_;


   if (($var =~ /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/) || ($var !~ /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z0-9]+)(\]?)$/)) {
      &CheckError(160, $var, $index);
   }
   
   return $var;
}

#################################################################################
# Subroutine: CheckError
#
# Prints out appropriate error message and terminates the program.
#
################################################################################
sub CheckError {
   my ($errorCode, @values) = @_;
   
   print "\n\n\tError: " . $errorCode . " - called by:\n";

   my $i = 0;
   my @calledBy = ();
   while ( @calledBy = caller($i++) ) {
      print "\tLine " . $calledBy[2] . " - " . $calledBy[3] . "\n";
   }
	
	my $message = "";
	if ( &CheckIfNumber($values[1], "P") ) {
      print "\nInvalid Configuration Value.\n\n\t";
		$message = "specified at line " . $values[1] . " of the configuration file";
	} else {
      print "\nInvalid Value.\n\n\t";
		$message = $values[1];	
	}

   if ($errorCode == 120) {
      print "The filname, '" . $values[0] . "', " . $message . ",\n\tdoes not exist."; 
   } elsif ($errorCode == 121) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tdoes not have a valid file extension:\n\n";
      print join("\t", @values[2..$#values]);
   } elsif ($errorCode == 130) {
      print "The value could not be checked. There is no number type: '" . $values[0] . "'."; 
   } elsif ($errorCode == 135) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tis not a valid " . &Expand($values[2]) . "."; 
   } elsif ($errorCode == 136) {
      print "The parameter: '" . $values[0] . "', " . $message . ",\n\t is not a valid " . &Expand($values[2]) . "."; 
   } elsif ($errorCode == 137) {
      print "The parameter: '" . $values[0] . "', " . $message . ",\n\t is not a valid " . &Expand($values[2]) . "."; 
   } elsif ($errorCode == 140) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tis less than the minimum value (" . $values[2] . ")."; 
   } elsif ($errorCode == 141) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tis greater than the maximum value (" . $values[2] . ")."; 
   } elsif ($errorCode == 142) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tis outside the specified range (" . $values[2] . " - " . $values[3] . ")."; 
   } elsif ($errorCode == 143) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tdoes not equal the specified value (" . $values[2] . ")."; 
   } elsif ($errorCode == 150) {
      print "The value, '" . $values[0] . "', " . $message . ",\n\tis not one of the valid options:\n\n";
      print "\t\t" . join("\t", @values[2..$#values]);
   } elsif ($errorCode == 160) {
      print "The parameter, '" . $values[0] . "', " . $message . ",\n\tdoes not meet the criteria to be a valid email address." ; 
   }

   print "\n\n";
   
   exit -1;
}


################################################################################
# Subroutine: Expand
#
# Expand the number type.
#
################################################################################
sub Expand {
   my ($type) = @_;
   
   if ( $type eq "N" ) {
      return "non-negative Integer";
   } elsif ( $type eq "I" ) {
      return "Integer";
   } elsif ( $type eq "P" ) {
      return "positive Integer";
   } elsif ( $type eq "D" ) {
      return "Decimal number";
   } elsif ( $type eq "F" ) {
      return "Floating Point number";
   }
   
   return "*error*";
}


1;
