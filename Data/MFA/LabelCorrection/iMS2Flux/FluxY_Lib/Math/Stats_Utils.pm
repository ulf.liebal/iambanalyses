package FluxY_Lib::Math::Stats_Utils;

#use strict;
#use warnings;

use FluxY_Lib::Data::Data_Check; # User Lib - Performs basic data checking.

use base 'Exporter';

our @EXPORT = qw(Average StdDev);

################################################################################
# Subroutine: Average
#
# A simple function for computing the average of a list of values.
#
################################################################################
sub Average {
   my @list = @_;
   
   my $total = scalar(@list);
   if ( $total < 1 ) { print "\t- empty list\n"; return 0; } #Change to throw error
   
   my $sum   = 0;
   for (my $i=0; $i < @list; $i++) {
      if ( !CheckIfNumber($list[$i], "D") ) {
         $total--;
         next;
      }
            
      $sum += $list[$i];
   }
   
   if ( $total == 0 ) { return 0; } #Change to throw error   
   
   return ($sum/$total);
}


################################################################################
# Subroutine: StdDev
#
# A simple function for computing the standard deviation of a list of values.
#
################################################################################
sub StdDev {
   my @list = @_;
   
   my $total = scalar(@list);
   if ( $total < 2 ) { print "\t- empty list\n"; return 0; } #Change to throw error

   my $avg = &Average(@list);
   
   my $sumOfSquares = 0;
   for (my $i=0; $i < @list; $i++) {
      if ( !CheckIfNumber($list[$i], "D") ) {
         $total--;
         next;
      }

      $sumOfSquares +=  ($list[$i] - $avg)**2;
   }

   if ( $total < 2 ) { return 0; } #Change to throw error

   return sqrt($sumOfSquares/($total-1));
}

1;
