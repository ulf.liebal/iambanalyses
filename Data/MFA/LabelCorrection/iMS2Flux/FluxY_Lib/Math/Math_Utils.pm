package FluxY_Lib::Math::Math_Utils;

use strict;
use warnings;

use FluxY_Lib::Data::Data_Check; # User Lib - Performs basic data checking.

use base 'Exporter';

our @EXPORT = qw(Fact FactDiv Binomial Multinomial Round Trunc Scale Int2Bin Transpose);


################################################################################
# Subroutine: Fact
#
# Returns the simple factorial of an integer value.
#
################################################################################
sub Fact {
    my($n) = @_;

   if ( !(defined $n) ) { &MathError(810, "first"); }

   if ( !&CheckIfNumber($n,"N") ) { &MathError(815, "first", $n); }
    
   my $product = 1;
   while( $n > 1 ) {
      $product *= $n--;
   }

   return $product;
}


################################################################################
# Subroutine: FactDiv
#
# A simple function for computing the division of factorials.
# - the parameters represent the factorials in the numerator and the denominator 
# respectively:
#                 n1 >= n2: n1!/n2! = (n1)(n1-1)...(n2+1)
#                 n2 > n1:  n1!/n2! = 1 / (n2)(n2-1)...(n1+1)
#
################################################################################
sub FactDiv {
   my($n1, $n2) = @_;

   if ( !(defined $n1) ) { &MathError(810, "first"); }
   if ( !(defined $n2) ) { &MathError(810, "second"); }

   if ( !&CheckIfNumber($n1,"N") ) { &MathError(815, "first", $n1); }
   if ( !&CheckIfNumber($n2,"N") ) { &MathError(815, "second", $n2); }

   my $inv = 0;
   if ($n2 > $n1) {
      $inv = 1;

      my $tmp = $n1;
      $n1  = $n2;
      $n2  = $tmp;
   }

   my $product = 1;
   while( $n1 > $n2 ) {
      $product *= $n1--;
   }

   if ( $inv ) { return 1/$product; }

   return $product;
}


################################################################################
# Subroutine: Binomial
#
# A simple function for computing binomial coefficients (2 variable unorderred
# combinations).
# - The number of k-combinations (each of size k) from a set S with n elements 
#(size n) is the binomial coefficient (also known as the "choose function"):
#
#                 C(n,k) =  n!/(k! (n-k)!) = (n)(n-1)...(k+1) / (n-k)!
#                                          = (n)(n-1)...(n-k+1) / k!
#
################################################################################
sub Binomial {
   my($n, $k) = @_;

   if ( !(defined $n) ) { &MathError(810, "first"); }
   if ( !(defined $k) ) { &MathError(810, "second"); }

   if ( !&CheckIfNumber($n,"N") ) { &MathError(815, "first", $n); }
   if ( !&CheckIfNumber($k,"N") ) { &MathError(815, "second", $k); }

   if ($k > $n) { &MathError(818, "k", "n"); }

   my $diff = $n - $k;
   
   # Swap order of numerator factorial, for efficiency with larger n.
   if ($diff > $k) {
      my $tmp = $k;
      $k      = $diff;
      $diff   = $tmp;
   }
   
   my $result = 1;
   while( $n > $k ) {
      $result *= $n--;
   }
   
   $result /= &Fact($diff);

   return $result;
}


################################################################################
# Subroutine: Multinomial
#
# A simple function for computing multinomial coefficients ('M' variable 
# unorderred combinations).
#
#                 C(n,[k1,k2,...,kM) =  n!/(k1! k2!...kM!)
#
################################################################################
sub Multinomial {
   my($n, $k_ref) = @_;
   
   my @k = @{$k_ref};
      
   my $result = &Fact($n);

   my $sum = 0;
   for (my $i=0; $i<@k; $i++) {
      $sum += $k[$i];
      $result /= &Fact($k[$i]);
   }
   if ($sum != $n) { &MathError(819, "n"); }

   return $result;
}


################################################################################
# Subroutine: Round
#
# Rounds a number to the nearest integer (positive up, negative down).
#
################################################################################
sub Round {
    my($n) = @_;

   if ( !(defined $n) ) { &MathError(810, "first"); }

   return int( $n + (0.5 * ($n <=> 0)) );
}


################################################################################
# Subroutine: Trunc
#
# Accepts a number to be truncated and the number of decimal points, then
# returns the number truncated to the given number of decimal points (padded
# with zeros if necessary).
#
################################################################################
sub Trunc {
   my ($num, $places) = @_;

   if ( !(defined $num) ) { &MathError(810, "first"); }

   my ($int, $frac) = split /\./, $num;
   
   # If the number of places is not defined, default to zero decimal places.
   if ( !defined($places) ) { return $int; }   
   if ( $places == 0 )      { return $int; }

   my $fPlaces = length($frac);
   
   # If the correct number of decimal places already exists return the number.
   if ($places == $fPlaces) { return $num; }

   # If there are too few decimal places pad with zeros.
   if ( $places > $fPlaces) {
      $frac .= "0" x ($places - $fPlaces);
      return "$int.$frac";
   }

   # finally if there are too many decimal places truncate.
   $frac = substr($frac,0,$places);
   return "$int.$frac";
}


################################################################################
# Subroutine: Scale
#
# Scales a list of non-negative numbers to sum to 100%.
# - requires at least one non-zero value, or returns unmodified list.
#
################################################################################
sub Scale {
   my @list = @_;

   my $sum = 0;
   for(my $i=0; $i<@list; $i++) {
      if ( $list[$i] < 0 ) { &MathError(840, $list[$i]); }
      $sum += $list[$i];
   }

   if ( $sum == 0 ) { return @list; }
   
   for(my $i=0; $i<@list; $i++) {
       $list[$i] /= $sum;
   }

   return @list;
}


################################################################################
# Subroutine: Int2Bin
#
# Converts a non-negative integer to binary representation with 'N' digits. The 
# binary number is stored in an array with MSD in position 0. 
#
################################################################################
sub Int2Bin{
   my ($val, $n) = @_;

   if ( !(defined $val) ) { &MathError(810, "first"); }
   if ( !(defined $n) )   { &MathError(810, "second"); }

   if ( !&CheckIfNumber($val,"N") ) { &MathError(815, "first", $val); }
   if ( !&CheckIfNumber($n,"N") )   { &MathError(815, "second", $n); }

   if ( $val >= (2**$n) ) { &MathError(830, $val, $n); }
   
   my @bin = ();
   
   while ( $val > 0 ) {
      my $rem = $val % 2;
      unshift (@bin, $rem);
      $val = ($val - $rem) / 2;
   }
   
   while ( $n > @bin ) {
      unshift (@bin,0);
   }
   
   return @bin;
}


################################################################################
# Subroutine: Transpose
#
# Returns the transpose of the original 2D array.
#
################################################################################
sub Transpose {
   my(@array) = @_;

   my @transpose = ();

   push @transpose, [map { shift @$_ } @array ] while grep {scalar @$_} @array;

   return @transpose;
}


################################################################################
# Subroutine: MathError
#
# Prints out file and directory handling error messages and terminates the 
# program.
#
################################################################################
sub MathError {
   my ($errorCode, @values) = @_;
      
   print "\n\n\tError: " . $errorCode . " - called by:\n";

   my $i = 0;
   my @calledBy = ();
   while ( @calledBy = caller($i++) ) {
      print "\tLine " . $calledBy[2] . " - " . $calledBy[3] . "\n";
   }
   print "\n";

   if ($errorCode == 810) {
      print "The " . $values[0] . " value passed is not defined."; 
   } elsif ($errorCode == 815) {
      print "Invalid parameter: The " . $values[0] . " parameter has value, " . $values[1] . "."
          . "\n\tThis function requires a non-negative integer for this parameter.";
   } elsif ($errorCode == 818) {
      print "Invalid parameter: " . $values[0] . " is greater than, " . $values[1] . "."
   } elsif ($errorCode == 819) {
      print "Invalid parameter: The sum of the indices does not add to " . $values[0] . "."
   } elsif ($errorCode == 820) {
      print "Invalid parameter:";
      for (my $i=0; $i<(@values-1); $i++) {
         print "\n\tParameter " . $i+1 . " has value: " . $values[$i];
      }
   } elsif ($errorCode == 830) {
      print "Invalid parameter(s):";
      print "\n\tThe number " . $values[0] . "cannot be represented in binary with only " . $values[1] . " digits.";
   } elsif ($errorCode == 840) {
      print "Invalid value: " . $values[0];
      print "\n\tThis function requires a list of non-negative numbers.";
   }

   print "\n\n";

   exit -3;
}


1;
