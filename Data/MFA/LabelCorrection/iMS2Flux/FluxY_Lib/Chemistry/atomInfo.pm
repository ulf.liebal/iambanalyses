package FluxY_Lib::Chemistry::atomInfo;

################################################################################
# This module provides the basic information for the supported elements along
# with the methods for accessing it.
#
################################################################################

use strict;
use warnings;

use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple arithmetic functions.

use base 'Exporter';

our @EXPORT = qw(GetFullList GetElementSymbols CheckElementSymbols GetAtomicNumbers GetElementNames GetElementMasses GenerateProbTable GenerateProbTable2);

# Hash table with information for the first 112 elements, the key is the atomic
# symbol. Each atomic symbol references an array of four values:
# - value 0: the atomic number,
# - value 1: the name,
# - value 2: the atomic mass, and
# - value 3: an array for naturally occuring stable isotope abundance, and 
#            containing two parts:
#   > the first element specifies the nuclear mass (int) of the first isotope.
#   > the remaining elements specify the fractional abundance specified in
#     increasing order of nuclear mass (from NIST 2011).
#   Note 1: If the element posesses no stable isotope, it has a single value, 0.
#   Note 2: Following standard practice, due to its extremely long half life BI 
#           is treated as having one stable isotope.
my %atomInfo = (
   H  => [1,   "Hydrogen",       1.00794,    [1, 0.999885,0.000115]],
   HE => [2,   "Helium",         4.002602,   [3, 0.00000137,0.99999866]],
   LI => [3,   "Lithium",        6.941,      [6, 0.0759,0.9241]],
   BE => [4,   "Beryllium",      9.012182,   [9, 1]],
   B  => [5,   "Boron",         10.811,      [10, 0.199,0.801]],
   C  => [6,   "Carbon",        12.0107,     [12, 0.9893,0.0107]],
   N  => [7,   "Nitrogen",      14.0067,     [14, 0.99636,0.00364]],
   O  => [8,   "Oxygen",        15.9994,     [16, 0.99757,0.00038,0.00205]],
   F  => [9,   "Fluorine",      18.9984032,  [19, 1]],
   NE => [10,  "Neon",          20.1797,     [20, 0.9048,0.0027,0.0925]],
   NA => [11,  "Sodium",        22.98976928, [23, 1]],
   MG => [12,  "Magnesium",     24.3050,     [24, 0.7899,0.1000,0.1101]],
   AL => [13,  "Aluminum",      26.9815386,  [27, 1]],
   SI => [14,  "Silicon",       28.0855,     [28, 0.92223,0.04685,0.03092]],
   P  => [15,  "Phosphorus",    30.973762,   [31, 1]],
   S  => [16,  "Sulfur",        32.065,      [32, 0.9499,0.0075,0.0425,0,0.0001]],
   CL => [17,  "Chlorine",      35.453,      [35, 0.7576,0,0.2424]],
   AR => [18,  "Argon",         39.948,      [36, 0.003365,0,0.000632,0,0.996003]],
   K  => [19,  "Potassium",     39.0983,     [39, 0.932581,0.000117,0.067302]],
   CA => [20,  "Calcium",       40.078,      [40, 0.96941,0,0.00647,0.00135,0.02086,0,0.00004,0,0.00187]],
   SC => [21,  "Scandium",      44.955912,   [45, 1]],
   TI => [22,  "Titanium",      47.867,      [46, 0.0825,0.0744,0.7372,0.0541,0.518]],
   V  => [23,  "Vanadium",      50.9415,     [50, 0.0025,0.9975]],
   CR => [24,  "Chromium",      51.9961,     [50, 0.04345,0,0.83789,0.09501,0.02365]],
   MN => [25,  "Manganese",     54.938045,   [55, 1]],
   FE => [26,  "Iron",          55.845,      [54, 0.05845,0,0.91754,0.02119,0.00282]],
   CO => [27,  "Cobalt",        58.933195,   [59, 1]],
   NI => [28,  "Nickel",        58.6934,     [58, 0.680769,0,0.262231,0.011399,0.036345,0,0.009256]],
   CU => [29,  "Copper",        63.546,      [63, 0.6915,0,0.3085]],
   ZN => [30,  "Zinc",          65.38,       [64, 0.48268,0,0.27975,0.04102,0.19024,0,0.00631]],
   GA => [31,  "Gallium",       69.723,      [69, 0.60108,0,0.39892]],
   GE => [32,  "Germanium",     72.64,       [70, 0.2038,0,0.2731,0.0776,0.3672,0,0.0783]],
   AS => [33,  "Arsenic",       74.92160,    [75, 1]],
   SE => [34,  "Selenium",      78.96,       [74, 0.0089,0,0.0937,0.0763,0.2377,0,0.4961,0,0.0873]],
   BR => [35,  "Bromine",       79.904,      [79, 0.5069,0,0.4931]],
   KR => [36,  "Krypton",       83.798,      [78, 0.00355,0,0.02286,0,0.11593,0.115,0.56987,0,0.17279]],
   RB => [37,  "Rubidium",      85.4678,     [85, 0.7217,0,0.2783]],
   SR => [38,  "Strontium",     87.62,       [84, 0.0056,0,0.0986,0.07,0.8258]],
   Y  => [39,  "Yttrium",       88.90585,    [89, 1]],
   ZR => [40,  "Zirconium",     91.224,      [90, 0.5145,0.1122,0.1715,0,0.1738,0,0.028]],
   NB => [41,  "Niobium",       92.90638,    [93, 1]],
   MO => [42,  "Molybdenum",    95.96,       [92, 0.1477,0,0.0923,0.159,0.1668,0.0956,0.2419,0,0.0967]],
   TC => [43,  "Technetium",    97.9072,     [0]],
   RU => [44,  "Ruthenium",    101.07,       [96, 0.0554,0,0.0187,0.1276,0.126,0.1706,0.3155,0,0.1862]],
   RH => [45,  "Rhodium",      102.90550,    [103, 1]],
   PD => [46,  "Palladium",    106.42,       [102, 0.0102,0,0.1114,0.2233,0.2733,0,0.2646,0,0.1172]],
   AG => [47,  "Silver",       107.8682,     [107, 0.51839,0,0.48161]],
   CD => [48,  "Cadmium",      112.411,      [106, 0.0125,0,0.0089,0,0.1249,0.128,0.2413,0.1222,0.2873,0,0.0749]],
   IN => [49,  "Indium",       114.818,      [113, 0.0429,0,0.9571]],
   SN => [50,  "Tin",          118.710,      [112, 0.0097,0,0.0066,0.0034,0.1454,0.0768,0.2422,0.0859,0.3258,0,0.0463,0,0.0579]],
   SB => [51,  "Antimony",     121.760,      [121, 0.5721,0,0.4279]],
   TE => [52,  "Tellurium",    127.60,       [120, 0.0009,0,0.0255,0.0089,0.0474,0.0707,0.1884,0,0.3174,0,0.3408]],
   I  => [53,  "Iodine",       126.90447,    [127, 1]],
   XE => [54,  "Xenon",        131.293,      [124, 0.000952,0,0.00089,0,0.019102,0.264006,0.04071,0.212324,0.269086,0,0.104357,0,0.088573]],
   CS => [55,  "Cesium",       132.9054519,  [133, 1]],
   BA => [56,  "Barium",       137.327,      [130, 0.00106,0,0.00101,0,0.02417,0.06592,0.07854,0.11232,0.71698]],
   LA => [57,  "Lanthanum",    138.90547,    [138, 0.0009,0.9991]],
   CE => [58,  "Cerium",       140.116,      [136, 0.00185,0,0.00251,0,0.8845,0,0.11114]],
   PR => [59,  "Praseodymium", 140.90765,    [141, 1]],
   ND => [60,  "Neodymium",    144.242,      [142, 0.272,0.122,0.238,0.083,0.172,0,0.057,0,0.056]],
   PM => [61,  "Promethium",   144.9127,     [0]],
   SM => [62,  "Samarium",     150.36,       [144, 0.0307,0,0,0.1499,0.1124,0.1382,0.0738,0,0.2675,0,0.2275]],
   EU => [63,  "Europium",     151.964,      [151, 0.4781,0,0.5219]],
   GD => [64,  "Gadolinium",   157.25,       [152, 0.002,0,0.0218,0.148,0.2047,0.1565,0.2484,0,0.2186]],
   TB => [65,  "Terbium",      158.92535,    [159, 1]],
   DY => [66,  "Dysprosium",   162.50,       [156, 0.00056,0,0.00095,0,0.02329,0.18889,0.25475,0.24896,0.2826]],
   HO => [67,  "Holmium",      164.93032,    [165, 1]],
   ER => [68,  "Erbium",       167.259,      [162, 0.00139,0,0.01601,0,0.33503,0.22869,0.26978,0,0.1491]],
   TM => [69,  "Thulium",      168.93421,    [169, 1]],
   YB => [70,  "Ytterbium",    173.054,      [168, 0.0013,0,0.0304,0.1428,0.2183,0.1613,0.3183,0,0.1276]],
   LU => [71,  "Lutetium",     174.9668,     [175, 0.9741,0.0259]],
   HF => [72,  "Hafnium",      178.49,       [174, 0.0016,0,0.0526,0.186,0.2728,0.1362,0.3508]],
   TA => [73,  "Tantalum",     180.94788,    [180, 0.00012, 0.99988]],
   W  => [74,  "Tungsten",     183.84,       [180, 0.0012,0,0.265,0.1431,0.3064,0,0.2843]],
   RE => [75,  "Rhenium",      186.207,      [185, 0.374,0,0.626]],
   OS => [76,  "Osmium",       190.23,       [184, 0.0002,0,0.0159,0.0196,0.1324,0.1615,0.2626,0,0.4078]],
   IR => [77,  "Iridium",      192.217,      [191, 0.373, 0,0.627]],
   PT => [78,  "Platinum",     195.084,      [190, 0.00014,0,0.00782,0,0.32967,0.33832,0.25242,0,0.07163]],
   AU => [79,  "Gold",         196.966569,   [197, 1]],
   HG => [80,  "Mercury",      200.59,       [196, 0.0015,0,0.0997,0.1687,0.231,0.1318,0.2986,0,0.0687]],
   TL => [81,  "Thallium",     204.3833,     [203, 0.2952,0,0.7048]],
   PB => [82,  "Lead",         207.2,        [204, 0.014,0,0.241,0.221,0.524]],
   BI => [83,  "Bismuth",      208.98040,    [209, 1]],
   PO => [84,  "Polonium",     208.9824,     [0]],
   AT => [85,  "Astatine",     209.9871,     [0]],
   RN => [86,  "Radon",        222.0176,     [0]],
   FR => [87,  "Francium",     223.0197,     [0]],
   RA => [88,  "Radium",       226.0254,     [0]],
   AC => [89,  "Actinium",     227.0278,     [0]],
   TH => [90,  "Thorium",      232.03806,    [0]],
   PA => [91,  "Protactinium", 231.03588,    [0]],
   U  => [92,  "Uranium",      238.02891,    [0]],
   NP => [93,  "Neptunium",    237.0482,     [0]],
   PU => [94,  "Plutonium",    244.0642,     [0]],
   AM => [95,  "Americium",    243.0614,     [0]],
   CM => [96,  "Curium",       247.0703,     [0]],
   BK => [97,  "Berkelium",    247.0703,     [0]],
   CF => [98,  "Californium",  251.0796,     [0]],
   ES => [99,  "Einsteinium",  252.083,      [0]],
   FM => [100, "Fermium",      257.0951,     [0]],
   MD => [101, "Mendelevium",  258.10,       [0]],
   NO => [102, "Nobelium",     259.1009,     [0]],
   LR => [103, "Lawrencium",   262.11,       [0]],
   RF => [104, "Rutherfordium",261,          [0]],
   DB => [105, "Dubnium",      262,          [0]],
   SG => [106, "Seaborgium",   266,          [0]],
   BH => [107, "Bohrium",      264,          [0]],
   HS => [108, "Hassium",      277,          [0]],
   MT => [109, "Meitnerium",   268,          [0]],
   DS => [110, "Darmstadtium", 281,          [0]],
   RG => [111, "Roentgenium",  272,          [0]],
   CN => [112, "Copernicium",  285,          [0]],
);


################################################################################
# Subroutine: GetFullList
#
# Returns the full list of atomic information.
#
################################################################################
sub GetFullList { return %atomInfo; }


################################################################################
# Subroutine: GetElementSymbols
#
# Returns the list of element symbols for the specified list of element IDs.
# - the element IDs can be either the elements name or its atomic number
# - if the name is not found in the hash, or the number exceeds the supported
# elements, -1 is returned in place of the corresponding symbol.
#
################################################################################
sub GetElementSymbols {
   my @IDs = @_;
      
   my @list = ();
   foreach my $id (@IDs) {
      my $symbol = -1;

      # Check if the identifier is a positive integer (i.e. atomic number).
      # Otherwise assume it is the name of an element.
      if ($id =~ /^\d+$/ ) {
         foreach my $key (keys %atomInfo) {
            if ( ${$atomInfo{$key}}[0] == $id ) {
               $symbol = $key;
               last;
            }
         }
      } else {
         foreach my $key (keys %atomInfo) {
            if ( uc(${$atomInfo{$key}}[1]) eq uc($id) ) {
               $symbol = $key;
               last;
            }
         }
      }
         
      push(@list, $symbol);
   }

   return @list;
}


################################################################################
# Subroutine: CheckElementSymbols
#
# Accepts one or more element symbols and returns true (1) if all symbols exist
# as keys in the hash, otherwise it returns false (0).
# - If no symbols are passed, an error (-1) is returned. 
#
################################################################################
sub CheckElementSymbols {
   my @symbols = @_;

   if (@symbols == 0) { return -1; }
   
   foreach my $symbol (@symbols) {
      if ( !(defined $atomInfo{uc($symbol)}) ) { return 0; }
   }
   return 1;
}


################################################################################
# Subroutine: GetAtomicNumbers
#
# Returns the list of atomic numbers for the specified list of element symbols.
# - if the elements symbol is not found in the hash, a -1 is returned in place
# of the corresponding atomic number.
#
################################################################################
sub GetAtomicNumbers { return &ExtractHashData(0, @_); }


################################################################################
# Subroutine: GetElementNames
#
# Returns the list of element names for the specified list of element symbols.
# - if the elements symbol is not found in the hash, a -1 is returned in place
# of the corresponding name.
#
################################################################################
sub GetElementNames { return &ExtractHashData(1, @_); }


################################################################################
# Subroutine: GetElementMasses
#
# Returns the list of element masses for the specified list of element symbols.
# - if the elements symbol is not found in the hash, a -1 is returned in place
# of the corresponding mass.
#
################################################################################
sub GetElementMasses { return &ExtractHashData(2, @_); }


################################################################################
# Subroutine: GenerateProbTable
#
# Returns the natural abundance probability table (2D array) and the offset list
# from the first stable isotopic mass to the atomic mass.
# - if an element symbol is not found in the hash, an "X" is returned in place 
# of the offset.
#
################################################################################
sub GenerateProbTable {
   my @symbols = @_;

   my @masses = &GetElementMasses(@symbols);

   my @offset = ();
   my @table  = ();

   foreach my $symbol (@symbols) {
      my $mass = ${$atomInfo{uc($symbol)}}[2];
      
      my $offset = "X";
      my $first  = "";
      my @list   = ();
      if ( defined($mass) ) {
         ($first, @list) = @{${$atomInfo{uc($symbol)}}[3]};
         
         if ( defined $first ) { $offset = $first - &Round($mass); }         
      }
      
      push(@offset, $offset);
      push(@table, [ @list ]);
   }
      
   return (\@table, \@offset);
}


################################################################################
# Subroutine: GenerateProbTable2
#
# Returns the natural abundance probability table (2D array) if the offset for
# all elements are zero, otherwise return an empty table.
# - if an element symbol is not found in the hash, an "X" is returned in place
# of the offset value, and an empty table is returned.
#
################################################################################
sub GenerateProbTable2 {
   my @symbols = @_;

   my ($refTable, $refOffset) = &GenerateProbTable(@symbols);

   my @offset = @{$refOffset};

   foreach my $offset (@offset) {
      if ( $offset != '0' ) { return; }
   }
      
   return ($refTable);

}


################################################################################
# Subroutine: ExtractHashData
#
# Generates the specified list of data from a hash of arrays. The specified
# array index defines the data to return for each hash key provided.
# - The list is indexed to the specified set of hash keys.
# - If the elements symbol is not found in the hash, a flag value (-1) is
# returned in place of the corresponding value.
#
################################################################################
sub ExtractHashData {
   my ($hashIndex, @keyList) = @_;

   my @list = ();
   foreach my $key (@keyList) {
      my $value = ${$atomInfo{uc($key)}}[$hashIndex];
      
      if ( !(defined $value) ) { $value = -1; }
      
      push(@list, $value);
   }
   return @list;
}

1;