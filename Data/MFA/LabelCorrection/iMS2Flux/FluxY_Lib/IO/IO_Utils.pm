package FluxY_Lib::IO::IO_Utils;

use strict;
use warnings;

use base 'Exporter';

use FluxY_Lib::Data::Data_Check;

our @EXPORT = qw(ReadFile ReadFileSkipB ReadFileSkipBC ReadPipedCommand GetStrings GetString GetFirstString GetLastString GetOptionString FileSystemError);

################################################################################
# Subroutine: ReadFile
#
# Read the specified file into an array.
#
################################################################################
sub ReadFile{
   my ($fileName) = @_;
   my @lines;

   open (FILE, $fileName) or FileSystemError(900, $fileName, $!);
      @lines = <FILE>;
   close (FILE)           or FileSystemError(901, $fileName, $!);
   chomp(@lines);
   
   # Check for and remove the UTF-8 byte order mark if present.
   $lines[0] =~ s/^\xEF\xBB\xBF//;

   # Check for and remove extra EOL character from Windows file.
   if ( $lines[0] =~ "\r" ) {
      foreach my $lines (@lines) { $lines =~ s/\r{1}$//; }
   }
   
   return @lines;
}


################################################################################
# Subroutine: ReadFileSkipB
#
# Read the specified file into an array, skipping all blank lines.
#
################################################################################
sub ReadFileSkipB{
   my ($fileName) = @_;
   my @lines;

   my @fLines = &ReadFile($fileName);
   foreach my $fLine (@fLines) {
      if ( !defined($fLine) ) { next; }             #Skip undefined lines.

      if ( $fLine =~ /^\s*$/ ) { next; }            #Skip blank lines.

      push(@lines, $fLine);
   }

   return @lines;
}


################################################################################
# Subroutine: ReadFileSkipBC
#
# Read the specified file into an array, skipping all blank and commented lines.
#
################################################################################
sub ReadFileSkipBC{
   my ($fileName) = @_;
   my @strings;
   my @lines;

   my @fLines = &ReadFile($fileName);

   foreach my $fLine (@fLines) {
      if ( !defined($fLine) ) { next; }             #Skip undefined lines.

      if ( $fLine =~ /^\s*$/ ) { next; }            #Skip blank lines.

      if ( substr($fLine,0,2) eq "//" ) { next; }   #Skip comment lines.

      @lines = (@lines, $fLine);
   }

   return @lines;
}


################################################################################
# Subroutine: ReadPipedCommand
#
# Create a piped output stream, and return the output of a command line
# instruction.
#
################################################################################
sub ReadPipedCommand{
   my ($inst) = @_;
   my @lines;

   open (PIPE, "$inst |") or FileSystemError(910, $inst, $!);
      @lines = <PIPE>;
   close (PIPE)           or FileSystemError(911, $inst, $!);
   chomp(@lines);

   return @lines;
}


################################################################################
# Subroutine: GetStrings
#
# Parse a line into strings. Strings are any sequence of characters separated by
# a delimiting character (may be any valid regular expression, by default any 
# amount of white space). Return the list of strings.
#
################################################################################
sub GetStrings{
   my ($line, $delim) = @_;

   if ( !defined($delim) ) { $delim = "\\s+"; }

   return split(/$delim/, $line);
}


################################################################################
# Subroutine: GetString
#
# Return the i'th string in a line using the given delimiter.
# - The index (i) is not used as an array index (starting at zero) but rather
# specifies the desired term - i.e. for the first term the index is 1.
# - The default delimiter (if not defined) is any amount of white space.
# - Returns Null if the desired string is not found.
#
################################################################################
sub GetString{
   my ($line, $i, $delim) = @_;

   if ( !defined($i) ) {
      &IOError(920, "GetString");
   } elsif ( !(&CheckIfNumber($i, "P")) ) {
      &IOError(921, "GetString", $i);
   }

   my @strings = &GetStrings($line, $delim);

   if ( @strings == 0 ) { return; }

   if ( @strings < $i ) { return; }

   return $strings[$i-1];
}


################################################################################
# Subroutine: GetFirstString
#
# Parse a line into strings (separated by white space), and return the first 
# string entry.
# - Returns Null if the desired string is not found.
#
################################################################################
sub GetFirstString{
   my ($line, $delim) = @_;

   return &GetString($line, 1, $delim);
}


################################################################################
# Subroutine: GetLastString
#
# Parse a line into strings seperated by a given delimiter (white space by 
# default), and return the last string entry.
# - Returns Null if the desired string is not found.
#
################################################################################
sub GetLastString{
   my ($line, $delim) = @_;

   my @strings = &GetStrings($line, $delim);

   if ( @strings == 0 ) { return; }

   return $strings[$#strings];
}


################################################################################
# Subroutine: GetOptionString
#
# The option string is contained after the last colon in a line. The option
# itself may contain any number and form of white space. This is replaced by a
# single string of options seperated by single spaces.
# - Returns Null if the desired string is not found.
#
################################################################################
sub GetOptionString{
   my ($line) = @_;

   return join(' ', &GetStrings(&GetLastString($line, ":")));
}


################################################################################
# Subroutine: FileSystemError
#
# Prints out file and directory handling error messages and terminates the 
# program.
#
################################################################################
sub FileSystemError {
   my ($errorCode, $errorMsg) = @_;
   
   print "\n\n\tError: " . $errorCode . " - called by:\n";

   my $i = 0;
   my @calledBy = ();
   while ( @calledBy = caller($i++) ) {
      print "\tLine " . $calledBy[2] . " - " . $calledBy[3] . "\n";
   }
   print "\n\n\t- ";
  
   if ($errorCode == 900) {
      print "Could not open/create the file "; 
   } elsif ($errorCode == 901) {
      print "Could not close the file "; 
   } elsif ($errorCode == 902) {
      print "Could not create the directory "; 
   } elsif ($errorCode == 903) {
      print "Could not change to the directory "; 
   } elsif ($errorCode == 904) {
      print "Could not change the permissions of "; 
   } elsif ($errorCode == 910) {
      print "Could not open the pipe for the instruction,\n\t\t"; 
   } elsif ($errorCode == 911) {
      print "Could not close the pipe for the instruction,\n\t\t"; 
   }

   print $errorMsg . "\n\n";

   exit -2;
}

################################################################################
# Subroutine: IOError
#
# Prints out IO related error messages and terminates the program.
#
################################################################################
sub IOError {
   my ($errorCode, @values) = @_;
  
   print "\n\nError(IO::IO_Utils): " . $errorCode . " - called by:\n";

   my $i = 0;
   my @calledBy = ();
   while ( @calledBy = caller($i++) ) {
      print "\tLine " . $calledBy[2] . " - " . $calledBy[3] . "\n";
   }
   print "\n\n\t";

   if ($errorCode == 920) {
      print "This function requires an index value to specify which string to return."; 
   } elsif ($errorCode == 921) {
      print "The requested index, " . $values[0] . ", is invalid.";
      print "\n\t\t- the index must be a positive integer.";
      print "\n\n\ti.e. to specify the first string, the index should be 1."; 
   }

   print "\n\n";

   exit -2;
}

1;
