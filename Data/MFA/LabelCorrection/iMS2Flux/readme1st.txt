README FILE


CONTENTS
I.   INTRODUCTION
II.  DIRECTORY STRUCTURE


I.   INTRODUCTION

To get started with the iMS2Flux software, please download and unzip the file 
iMS2Flux.zip from the SourceForge project
http://sourceforge.net/projects/ims2flux/.

Additionally a Windows based GUI (iMS2Flux GUI.zip) is available that can be 
downloaded from the same SourceForge project under the files link, 
http://sourceforge.net/projects/ims2flux/files/.

In the unzipped folder iMS2Flux you will find the main program, iMS2Flux.pl, and 
program libraries, FluxY_Lib and Math, as well as folders containing a complete 
example with expected results, the getting started guides and instructions for 
adding a new analyte data class.

For complete instructions for getting started, including installing Perl if 
necessary, please see the getting started guide for your operating system in the 
Getting_Started folder.

The current version of iMS2Flux is v.7.2, released Oct. 31, 2013, it includes 
a redeveloped set of analyte data classes designed to fascilitate adding a new
user defined analyte class via a new experimental feature. See the change log 
for complete details and the manual in the Adding_New_Analytes folder for 
complete details and an example.


II.  DIRECTORY STRUCTURE

iMS2Flux/
  readme1st.txt
    -  this file.
  change.log
    -  a text file listing the changes made to the software.
  iMS2Flux.pl
    - the main program.
  iMS2Flux-Manual_v7.2.pdf
    - the instruction manual.
  Adding_New_Analytes
    - a folder with the adding new analytes manual and example file.
  Getting_Started
    - a folder with the getting started files in PDF format.
  Example_AA
    - a folder with the example files and expected results.
  FluxY_Lib
    - a folder with the program library files (not further described).
  Math
    - a folder with included CPAN library file(s) (not further described).
   
Adding_New_Analytes
  newDataClassExample.txt
    - The new analyte data class for the working example.
  iMS2Flux-Adding_New_Analytes_v7.2.pdf
    - A users manual for adding new analyte data classes.
  Results
    - a folder containing the result files from running the example as 
    described in chapter 2 of the adding new analytes users manual.

Adding_New_Analytes/Results/
  GE-DE_Data_Table.txt
    - The default generic analyte data table - before importing the example data
    class.
  GE-SM_Data_Table.txt
    - The new generic analyte data table (based on SM data) - after importing 
    the example data class.

Getting_Started
  Getting Started with Linux-Unix.pdf
    - A quick start guide for getting started using FluxY with Linux.
  Getting Started with Mac OSX.pdf
    - A quick start guide for getting started using FluxY with Mac OSX.
  Getting Started with Windows.pdf
    - A quick start guide for getting started using FluxY with Windows.
  MSto13C with QuanLynx and iMS2Flux.pdf
    - A complete workflow for 13C labelling experiments utilizing QuanLynx 
    software from Waters and iMS2Flux.pl to generate data for 13C flux analysis.

Example_AA/
  config.txt
    - an example configuration file.
  Example_AA.txt
    - an example of Mass Spec. data for amino acids from 3 experiment and a 
    total of 24 replicates.
  headers.txt
    - an example of the auxiliary file of experimental names.
  OBM.txt
    - an example of the auxiliary file for providing original biomass data.
  Results
    - a folder containing the result files from running the example as 
    described in the getting started guides.

Example_AA/Results/
  Example_AA-AvgCarbon.txt
    - The average carbon labelling (one set per experiment).
  Example_AA-CorrectedData_(NA).txt
    - The corrected measurement data for natural abundance, one set per 
    replicate.
  Example_AA-CorrectedData_(NA)-Average.txt
    - The corrected measurement data, one set per experiment.
  Example_AA-CorrectedData_(NA)-StdDev.txt
    - The standard deviation of the corrected measurements, one set per 
    experiment.
  Example_AA-MeasuredData.txt
    - The original measurement data, unprocessed.
  Example_AA-MS_Exp_1.txt
    - The corrected measurement data for experiment 1, formatted for inclusion 
    in the MASS_SPECTROMETRY section of a 13CFLUX ftbl file.
  Example_AA-MS_Exp_2.txt
    - The corrected measurement data for experiment 2, formatted for inclusion 
    in the MASS_SPECTROMETRY section of a 13CFLUX ftbl file.
  Example_AA-MS_Exp_3.txt
    - The corrected measurement data for experiment 3, formatted for inclusion 
    in the MASS_SPECTROMETRY section of a 13CFLUX ftbl file.

