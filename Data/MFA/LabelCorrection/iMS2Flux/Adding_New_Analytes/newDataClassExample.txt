// Class Name:
Soluble Metabolites

// Class abbreviation:
SM

// Labeled Element (LE):
C

// Data Table:
Key	C	H	O	N	S	Si	P	Num-C	Num-LE	Position-LE	Name
ala2	5	14	0	1	0	1	0	2	3	2-3	alanine_(2TMS)
ala3_b	11	28	2	1	0	3	0	3	3	1-3	alanine_(3TMS)
ala3_b1	7	20	0	1	0	2	0	1	3	3-3	alanine_beta-_(3TMS)
ala3_b2	11	28	2	1	0	3	0	3	3	1-3	alanine_beta-_(3TMS)
