#!/usr/bin/perl -w

#  Copyright (c) 2009 C. Hart Poskar.
################################################################################
# iMS2Flux.pl - Automatically check and correct Mass Spectrometry data and 
#                generate output in a format ready for inclusion in  model files
#                for 13C metabolic flux analysis.
#
# For complete details see the accompanying users manual.
#
# Quick Start:
# To run this correction program, open a command prompt, change to the directory 
# containing your data and configuration file (by default config.txt) and type:
#
#        PROMPT> iMS2Flux.pl <ENTER>
#
# Alternatively the configuration file may have a different (more meaningful)
# name. In this case the program will accept a single command line argument that
# specifies the name of the configuration file:
#
#        PROMPT> iMS2Flux.pl [configuration file name] <ENTER>
#
# To see the full help screen use the command:
#
#        PROMPT> iMS2Flux.pl -help <ENTER>
#
################################################################################
# Versions
# ========
#   1   - Original (full command line).
#   2   - Added data checks and support for additional isotopomer masses.
#   3   - Changed to configuration file for most user switches.
#   4   - Added additional correction options (proton loss & original biomass)
#   5   - Generalized to MS correction, moved to simple OO data classes,
#       include support for Cell Wall compounds.
#   6   - Add support for multiple derivatizing agents, average carbon labelling
#       check, proton gain correction, and support for Fatty Acids, Glycerol and
#       Glucose.
#   7   - Extend support to a variable number of replicates.
#       - Optional secondary data file of retention time data, for RT data check.
#       - Add ability to directly replace/append measurements in model files.
#       - Added scan mode support for importing MS data (in MAM format).
#   7.1 - added S.D. to ACL output, implemented as command line switch "-P2".
#       - added support to exclude the backbone carbon from NA correction, 
#       implemented as command line switch "-BB #".
#       - Corrected OBM indexing error, and implemented explicit checks.
#       - Generalized data check error messages to support sources other than 
#       the configuration file.
#   7.2 - Extended and replaced the parent data class with a compounds class and
#       a fragments class. Rewrote analyte data classes to focus on the data.
#       - Added a generic (GE) data class and new command line switch "-I" to
#       support the automatic generation of  an analyte class from a text file
#       with similar format to the analyte information tables.
#
# Notes:
# - look at combining extractPre and extractPost.
#
################################################################################
#Pragma to find the absolute path to the directory containing this script.
use FindBin qw($Bin);      

#Pragma to add the directory containing this script to the list of directories 
#to be searched for modules.
use lib "$Bin";

###########
#         #
# Modules #
#         #
###########

# List the available analyte libraries:
use FluxY_Lib::MS::DataClass_AA;   # User Lib - Provides Amino Acid Class.
use FluxY_Lib::MS::DataClass_CW;   # User Lib - Provides Cell Wall Class.
use FluxY_Lib::MS::DataClass_FA;   # User Lib - Provides Fatty Acid Class.
use FluxY_Lib::MS::DataClass_GE;   # User Lib - Provides a Generic Analyte Class.
use FluxY_Lib::MS::DataClass_GL;   # User Lib - Provides Glucose Class.
use FluxY_Lib::MS::DataClass_GY;   # User Lib - Provides Glycerol Class.
use FluxY_Lib::MS::DataClass_SM;   # User Lib - Provides Starch Monomer.

use FluxY_Lib::MS::DAgent;         # User Lib - Provides Derivatizing Agent Info.

use FluxY_Lib::Chemistry::atomInfo; # User Lib - Natural Abundance Probability.
use FluxY_Lib::Data::Data_Check;    # User Lib - Basic configuration data check.
use FluxY_Lib::IO::IO_Utils;        # User Lib - I/O routines.
use FluxY_Lib::Math::Math_Utils;    # User Lib - Simple arithmetic functions.
use FluxY_Lib::Math::Stats_Utils;   # User Lib - Simple stats functions.
use FluxY_Lib::MS::CorrectFragment; # User Lib - AA corrections per fragment.
use FluxY_Lib::MS::ErrorsAndHelp;   # User Lib - Prints GCMS related messages.
use FluxY_Lib::MS::Generate13C;     # User Lib - Generates 13CFLUX compatible MS files.
use FluxY_Lib::MS::Generate13C_2;   # User Lib - Generates 13CFLUX2 compatible MS files.
use FluxY_Lib::MS::GenerateOpenFlux;# User Lib - Generates OpenFLUX compatible MS files.
use FluxY_Lib::MS::GenerateTSV;     # User Lib - Generates generic TSV files.
use FluxY_Lib::MS::ImportDataClass; # User Lib - Imports a new generic data class.
use FluxY_Lib::MS::OptionalChecks;  # User Lib - Optional MS data checks (Pre correction).
use FluxY_Lib::MS::OptionalChecks2; # User Lib - Post correction optional data checks.
use FluxY_Lib::MS::ParseMAM;        # User Lib - Parses MS data from agilent macros.
use FluxY_Lib::MS::ParseQL;         # User Lib - Parses MS data from Waters QuanLynx.
use FluxY_Lib::MS::ParseTSV;        # User Lib - Parses MS data from TSV file.
use FluxY_Lib::MS::Stats;           # User Lib - Stats for 2D MS data sets.
use Getopt::Long;                   # Std. Lib - Parse the command line for options.

####################
#                  #
# GLOBAL VARIABLES #
#                  #
####################

$configFile  = "config.txt";  # The default name of the configuration file.

$dataFlag = ""; # If set: Select the Area data for processing.
$zFlag   = 0;   # If set: Replace missing data with zero.
$scnFlag = 0;   # If set: MS data extracted from scan mode chromatogram (MAM).
$rhfFlag = 0;   # If set: Replicate Header File is provided.
$m_1Flag = 0;   # If set: Fragments include measurement values preceding M+0.
$mp1Flag = 0;   # If set: Fragments include measurement values post M+numC.
$cplFlag = 0;   # If set: perform the Correction for Proton Loss (M-1).
$cpgFlag = 0;   # If set: perform the Correction for Proton Gain (M+1).
$cnaFlag = 0;   # If set: perform the Correction for Natural Abundance.
$cobFlag = 0;   # If set: perform the Correction for Original Biomass.
$prtFlag = 0;   # If set: Print the Retention Time Table.
$pmdFlag = "";  # If set (A|B|R): Print the Measured Data Table.
$pcdFlag = 0;   # If set: Print the full Corrected Data Table.
$padFlag = 0;   # If set: Print the Corrected Data Table averaged over 'n' replicates.
$psdFlag = 0;   # If set: Print the Standard Deviation Table.
$pmsFlag = 0;   # If set: Print MS model data.
$aclFlag = 0;   # If set: Perform average carbon labelling data check.
$pacFlag = 0;   # If set: Print standard deviations for the average carbon labelling.
$bbcFlag = 1;   # The percentage (in decimal form) of the carbon backbone to correct for NA.

$dThreshold = 0;  # The threshold of the detector, above which data is invalid.
$tThreshold = 0;  # Total Threshold, below which fragments have poor peak quality.
$rThreshold = 0;  # Retention Time Threshold, in number of standard deviations.
$vThreshold = 0;  # Value threshold, the percentage (in decimal form) of the 
                  # fragment sum, below which all data values are set to zero.

$modelType  = ""; # The object providing information for different model formats.
$modelsFlag = 0;  # If set: replace/append measurement data in specified models.
$appendOnly = 0;  # If set: append measurements if modelsFlag is set.
@modelFiles = (); # The list of model files to use if modelsFlag is set.

@dataFiles  = (); # The name of one or more input data file.
$dataFormat = ""; # The format of the input data file. 
$baseName   = ""; # Derived from the input data file, used for output file names.
@replicates = (); # The number of consecutive replicate data sets.
$rhFile     = ""; # The name of the file containing experimental/replicate names.
$rtDataFile = ""; # The name of the optionally included retention time data file.
$obFile     = ""; # The data file containing original biomass data.
$sdOffset   = 0;  # A constant which may be added to the calculated standard deviation.

$msType     = ""; # The object providing information for different MS data types.

@cNames     = (); # The array of compund names.
@cMasses    = (); # The array of compund masses.
@sNames     = (); # The array of sample names.
@fIndex     = (); # The starting index of each fragment (the last entry is EOA+1).
@colHeaders = (); # List of column headers for replicates.
@obData     = (); # Original Biomass percentage, used only if flag is set.

@numCarbons   = (); # The number of carbon atoms in each fragment.
@fragAtomStrs = (); # List of carbon atoms in each fragment.

# The following are 2D arrays. Each element is a set of data measurements, one
# for each experiment. 
@dataResps = ();  # The primary array of MS measurements (dependent on data source).
@dataAreas = ();  # Second MS quantification method (dependent on data source).
@dataRT    = ();  # The retention time for found peaks (dependent on data source).
@dataFlags = ();  # Detection flags for each data set (dependent on data source).

@processData   = (); # Either the Area or Response - depends on $dataFlag value.
@M_1           = (); # M-1 data measurements, used only if flag is set.
@Mp1           = (); # M+numC+1 data measurements, used only if flag is set.
@missingData   = (); # The list of fragments with missing data measurements.
@correctedData = (); # The list of corrected data sets (area or response data).
@averages      = (); # Average of the corrected data over replicates.
@stdDevs       = (); # The standard deviation of corrected data over replicates.

#############
# Constants #
#############

# List of supported data types.
@dataTypes = ("AA", "CW", "FA", "GE", "GL", "GY", "SM");
# List of supported input formats.
@inputFormats = ("QL", "TSV", "MAM");
# List of supported network file formats.
@networkFormats = ("MS", "LM", "OF", "C2");
# List of supported print options for the measurment data.
@printOptions = ("Y", "N", "A", "R", "B");

######################
#                    #
# START MAIN PROGRAM #
#                    #
######################

# Read and parse all input sources.
&ParseCommandLine();   # Command Line
&ParseConfigFile();    # Configuration File
&GetInputData();       # MS and Optional Data.

# Print requested raw data.
if ( ($pmdFlag eq 'A') || ($pmdFlag eq 'B') ) { 
   &PrintMatrix("Integrated Area (Raw Data)", $baseName . "-MeasuredData(Area).txt", \@sNames, \@cNames, \@cMasses, \@dataAreas);
}
if ( ($pmdFlag eq 'R') || ($pmdFlag eq 'B') ) {
   &PrintMatrix("Response Values (Raw Data)", $baseName . "-MeasuredData.txt", \@sNames, \@cNames, \@cMasses, \@dataResps);
}
if ( $prtFlag ) {
   &PrintMatrix("Retention Time", $baseName . "-RetentionTimes.txt", \@sNames, \@cNames, \@cMasses, \@dataRT);
}

# Check for missing data.
# If missing data is found and the Zero override is not set throw an error.
if ( my $num = &CheckForMissingData() ) {
   &PrintMatrix("Missing (Uncorrected) Data", $baseName . "-Missing.txt", \@sNames, \@cNames, \@cMasses, \@missingData);
   if ( !($zFlag) ) {
      &Error(10, $baseName . ".txt", $baseName . "-Missing.txt", $num);
   }
}

# Extract M-1 data, M+numC+1 data, and remove pre and post measurements from raw data.
if ($cplFlag) { @M_1 = &ExtractPre($m_1Flag,1); }
if ($cpgFlag) { @Mp1 = &ExtractPost($mp1Flag,1); }
if ($m_1Flag) { &Remove("PRE",  $m_1Flag); }
if ($mp1Flag) { &Remove("POST", $mp1Flag); }

# Perform optional data checks.
&CheckData();

# Perform requested data correction.
# If no correction has been chosen, each uncorrected fragment is simply scaled.
&CorrectData();

# Perform optional post correction processing.
&CheckData2();

#Calculate statistics over each set of replicates.
@averages = &MS_Average(\@replicates, \@correctedData);
@stdDevs  = &MS_StdDev (\@replicates, \@correctedData, \@averages, $sdOffset);

# Generate requested output.
my $fileID = "OriginalData_(Normalized)";

if ( $cplFlag || $cpgFlag || $cnaFlag || $cobFlag ) {
   $fileID = "CorrectedData_(";
   
   if ( $cplFlag ) { $fileID .= "PL,"; }
   if ( $cpgFlag ) { $fileID .= "PG,"; }
   if ( $cnaFlag ) { $fileID .= "NA,"; }
   if ( $cobFlag ) { $fileID .= "OBM,"; }
   
   chop($fileID);
   
   $fileID .= ")";
}

if ( $pcdFlag ) { &PrintMatrix("Processed Data", $baseName . "-" . $fileID . ".txt", \@sNames, \@cNames, \@cMasses, \@correctedData); }

if ( $padFlag ) { &PrintMatrix("Average Data", $baseName . "-" . $fileID . "-Average.txt", \@colHeaders, \@cNames, \@cMasses, \@averages); }

if ( $psdFlag ) { &PrintMatrix("Standard Deviation", $baseName . "-" . $fileID . "-StdDev.txt", \@colHeaders, \@cNames, \@cMasses, \@stdDevs); }

if ( $pmsFlag ) {
	# If the model flag is set, then combine the measurements with the corresponding
	# models, otherwise print out the measurement data in the requested formats.
	if ( $modelsFlag ) {
		$modelType->GenerateModels(\@modelFiles, \@fIndex, \@colHeaders, \@cNames, \@averages, \@stdDevs, \@fragAtomStrs, $appendOnly, $msType);
	} else {
		$modelType->Generate($baseName, \@fIndex, \@colHeaders, \@cNames, \@averages, \@stdDevs, \@fragAtomStrs, $msType);
   }
}

######################
#                    #
#  End MAIN PROGRAM  #
#                    #
######################


###############
#             #
# SUBROUTINES #
#             #
###############

################################################################################
# Subroutine: ParseCommandLine
#
# Parses the command line to extract required and optional parameters:
# - optional: param 1 - configuration file name.
# If the P1 flag is set, then there is an alternative optional parameter:
# - optional: param 1 - derivatizing agent identifier.
# The P2 option is ignored if the ACL flag is not set in the configuration file.
#
################################################################################
sub ParseCommandLine{
   my $helpFlg = 0;   # If set: Print the Program Syntax, and exit.
   my $pmtFlag = 0;   # If set: Print the Mass Table, and exit.
   my $idcFlag = 0;   # If set: Import the specified file as the new GE class.
   
   GetOptions("P1=s"   =>\$pmtFlag,    # [N] Print Mass Table - contains MS Type.
              "P2"     =>\$pacFlag,    # [N] Print Standard Deviations for ACL.
              "I1=s"   =>\$idcFlag,    # [N] Imports a new generic data class.
              "Z"      =>\$zFlag,      # [N] Override Missing Data Restriction. 
              "BB=f"   =>\$bbcFlag,    # [1] % Carbon backbone to correct for NA. 
              "scan"   =>\$scnFlag,    # [N] MS data extracted in scan mode.
              "help|?" =>\$helpFlg);   # [N] Print the Help screen.

   if ( $helpFlg ) { 
      &PrintSyntax;
      exit 0;
   }

   if ( $idcFlag ) {
      &ImportDataClass_GE($idcFlag, $Bin);
      exit 0;
   }

   # Check if the print mass table flag is set.
   if ( $pmtFlag ) {
      $pmtFlag = &CheckOptions($pmtFlag, "spesified with the command line switch -P1", @dataTypes);

	   my $dAgent = "NONE";
      if ( @ARGV == 1 ) { $dAgent = $ARGV[0]; }

      # Set the MS type object.
      &SetDataClass($pmtFlag, $dAgent);

      $msType->PrintInfoTable();
      exit 0;
   }   

   
   if ( @ARGV > 1 ) { &Error(101, @ARGV); } #Need to redo error messages
   
   if ( @ARGV == 1 ) { $configFile = $ARGV[0]; }
	
	# Check that the value of the labeled backbone switch is between 0 and 100% in decimal form.
	&CheckLimits ($bbcFlag, "spesified with the command line switch -BB", 0, 1, "D");   

}


################################################################################
# Subroutine: ParseConfigFile
#
# Acquires configuration settings from the given configuration file. Checks for
# basic consistency in setting values.
#
################################################################################
sub ParseConfigFile{
   my $configLines = 33;
   my $dataType    = "";
   my $modelFormat = "";
   my $dAgent      = "";
   my $rtFlag      = 0;
   
   if ( !(-e $configFile) ) { &Error(110, $configFile); }
      
   my @lines = &ReadFileSkipBC($configFile);

   if ( @lines !=  $configLines) { &Error(111, $configLines); }
   
   my $index = 0;
   
   @dataFiles = split(/\s/, &GetLastString($lines[$index++], ":"));

	# Remove leading whitespace.
	shift @dataFiles;

   if ( @dataFiles == 0) { &Error(112); }

   foreach my $file (@dataFiles) { &CheckFileName($file, $index); }
	
   ($baseName) = split (/\./, $dataFiles[0]);

   $dataFormat = &CheckOptions(&GetLastString($lines[$index++]), $index, @inputFormats);
   $dataType   = &CheckOptions(&GetLastString($lines[$index++]), $index, @dataTypes);
	
   $dAgent = &CheckOptions(&GetLastString($lines[$index++]), $index, "NONE", &GetDAgentList());

   # Set the MS type object.
   &SetDataClass($dataType, $dAgent);

   $dataFlag   = &CheckOptions  (&GetLastString($lines[$index++]), $index, "A", "R");
	
	@replicates = split(/\s+/, &GetLastString($lines[$index++], ":"));
   shift(@replicates);

	if ( @replicates == 0) { &Error(113); }

	foreach my $rep (@replicates) {
	   &CheckLLimit ($rep, $index, 1, "I");
	}

	$rhfFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   
   # The replicate header file is required if the flag is set.
   if ( $rhfFlag ) {
      $rhFile  = &CheckFileName (&GetLastString($lines[$index++]), $index);
   } else {
      $index++;
   }

   $m_1Flag    = &CheckLLimit(&GetLastString($lines[$index++]), $index, 0, "I"); 
   $mp1Flag    = &CheckLLimit(&GetLastString($lines[$index++]), $index, 0, "I");
   $dThreshold = &CheckLLimit(&GetLastString($lines[$index++]), $index, 0, "I");
   $tThreshold = &CheckLLimit(&GetLastString($lines[$index++]), $index, 0, "I");
   
   $rThreshold = &CheckLLimit(&GetLastString($lines[$index++]), $index, 0, "D");
   
   # If the retention time threshold data check is being used, check for
   # optional data file. This file may be optionally passed to the appropriate
   # data parser.
   # This is not currently required by any of the data parsers.
   # - If this changes additional checks should be implemented.
   if ( $rThreshold ) {
	   $rtFlag   = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
      
      if ( $rtFlag ) {
         $rtDataFile  = &CheckFileName (&GetLastString($lines[$index++]), $index);
      } else {
         $index++;
      }
   } else {
      $index += 2;
   }

   $vThreshold = &CheckLimits   (&GetLastString($lines[$index++]), $index, 0, 1, "D");
   $aclFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
	
   $cplFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   
   # M-1 data is required if the correction flag is set.
   if ( $cplFlag && !($m_1Flag) ) { &Error(115); }
	
   $cpgFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   
   # M+1 data is required if the correction flag is set.
   if ( $cpgFlag && !($mp1Flag) ) { &Error(116); }
   
   # Check that only one of proton gain and proton loss is selected.
   if ( $cpgFlag && $cplFlag ) { &Error(117); }

   $cnaFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);

   # Verify natural abundance correction is viable for given compound.
   # - Must only contain atoms with a zero offset between the first isotope mass
   #   and the atomic mass.
   if ( $cnaFlag ) {
      my @atomList = $msType->GetAtomList;
      my ($refTable, $refOffset) = &GenerateProbTable(@atomList);

      my @offset  = @{$refOffset};
      my @badList = ();
      for (my $i; $i<@offset; $i++) {
         if ( $offset[$i] ne '0' ) { push(@badList, $atomList[$i]); }
      }
      
      if ( @badList ) { &Error(119, @badList); }
   }

   
   $cobFlag    = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   
   # The original biomass data file is required if the correction flag is set.
   if ( $cobFlag ) {
      $obFile  = &CheckFileName (&GetLastString($lines[$index++]), $index);
   } else {
      $index++;
   }
   
   $prtFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   $pmdFlag = &CheckOptions  (&GetLastString($lines[$index++]), $index, @printOptions);

   # The Yes option is defined to print both data sets for QL data.
   if ( $pmdFlag eq "Y" ) { $pmdFlag = "B"; }

   $pcdFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   $padFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   $psdFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
	
	$pmsFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);

	if ( $pmsFlag ) {
	   $modelFormat = &CheckOptions(&GetLastString($lines[$index++]), $index, @networkFormats);
	   &SetGenerateClass($modelFormat);

      $modelsFlag = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
   
		if ( $modelsFlag ) {
			$appendOnly = &CheckOptionsYN(&GetLastString($lines[$index++]), $index);
			@modelFiles = split(/\s/, &GetLastString($lines[$index++], ":"));

			# Remove leading whitespace.
			shift @modelFiles;

			if ( @modelFiles == 0) { &Error(112); }

			foreach my $file (@modelFiles) { &CheckFileName($file, $index); }
		} else {
			$index += 2;
		}
	} else {
		$index += 4;
	}
   
   $sdOffset = &CheckLimits   (&GetLastString($lines[$index++]), $index, 0, 1, "D");
   
   # Depending on data format, some options may be unavailable - enforced here.
   #? Perhaps add an informative message if values changed.
   if ( $dataFormat eq "TSV" ) {
      # Only one set of data values, stored in @dataResps.
      $dataFlag   = "R"; 
      if ( $pmdFlag eq "B" ) { $pmdFlag = "R"; } 
      if ( $pmdFlag eq "A" ) { $pmdFlag = "N"; } 
      # No retention time data, so can't perform check unless extra data is provided.
      if ( $rThreshold && !$rtFlag ) {
         $rThreshold = 0;
         $prtFlag    = 0;
      }
      
      # Zero option not supported
      $zFlag = 0;
   }
   if ( $dataFormat eq "MAM" ) {
      # Only one set of data values, stored in @dataResps.
      $dataFlag   = "R"; 
      if ( $pmdFlag eq "B" ) { $pmdFlag = "R"; } 
      if ( $pmdFlag eq "A" ) { $pmdFlag = "N"; } 
      # No retention time data, so can't perform check (for now).
      $rThreshold = 0;
      $prtFlag    = 0;

      # Zero option not supported
      $zFlag = 0;
   }
}


################################################################################
# Subroutine: GetInputData
#
# Extracts the input data from any data files and organizes it for later use.
# - MS Data File (Required).
# - Replicate Header File (Optional).
# - Original Biomass Data (Optional).
# - Model File(s) (Optional).
#
################################################################################
sub GetInputData{
   my @results = ();
	
   foreach my $file (@dataFiles) {
      if ( $dataFormat eq "TSV" ) {
         @results = &ParseTabbedInput_V($file, $msType, $rtDataFile);
      } elsif ( $dataFormat eq "MAM" ) {
         @results = &ParseAgilentInput($file, $msType, $m_1Flag, $mp1Flag, $scnFlag);
      } elsif ( $dataFormat eq "QL" ) {
         @results = &ParseQuanLynxInput($file, $msType);
      }
		
		# Requires all files (if more than one) to have the same fragments.
		@cNames    = @{$results[0]};
		@cMasses   = @{$results[1]};
		push(@sNames   , @{$results[2]});
		push(@dataResps, @{$results[3]});
		push(@dataAreas, @{$results[4]});
		push(@dataRT   , @{$results[5]});
		push(@dataFlags, @{$results[6]});
   }
   	
   # Checks that the compound names / masses match the available compounds. 
   # If not in the list of supported compounds, issue warning and remove.
   my (@compounds) = $msType->GetCompoundList();
   my @list = ();
   for (my $i=0; $i < @cNames;) {
      my $name = $cNames[$i];
      
      my $found = 0;
      foreach my $compound (@compounds) {
         if ( $name eq $compound ) {
            $found = 1;
            last;
         }
      }
      
      # Find the last measurement for this metabolite name:
      my $start = $i;
      for ($i++; $i<@cNames; $i++) {
         if ( $cNames[$i] ne $name ) { last; }
      }

      # If not in the list, remove from all appropriate data structures:
      if ( !$found ) {
         push(@list, $name);
         
         my $length = $i-$start;
         splice(@cNames, $start, $length);
         splice(@cMasses, $start, $length);
         for (my $j=0; $j < @sNames; $j++) {
            splice(@{$dataResps[$j]}, $start, $length);
            splice(@{$dataFlags[$j]}, $start, $length);
            splice(@{$dataRT[$j]}   , $start, $length);
            splice(@{$dataAreas[$j]}, $start, $length);
         }
         
         $i = $start;
      }
   }
   
   if ( @list > 0 ) { &Warning(20, @list); }
 
   # Check that there are compounds to evaluate.
   if ( @cNames == 0 ) { &Error(205, $msType->GetDataType()); }
   
   
   # Create an index for the starting point of each fragment. Each fragment
   # is defined by a series of mass isotopomers. A Discontinuity in the mass
   # or a change in the amino acid name indicates the start of a new fragment.
   $name = $cNames[0];
   $mass = $cMasses[0];   
   push(@fIndex,0);
   for (my $i=1; $i < @cMasses; $i++) {
      $mass++;
      if ( ($cMasses[$i] != $mass) || ($cNames[$i] != $name) ) {
         $name = $cNames[$i];
         $mass = $cMasses[$i];
         push(@fIndex,$i);
      }
   }
   push(@fIndex, scalar(@cMasses));
	
   # Checks that the starting fragment mass is available.
   @list1 = ();
   @list2 = ();
   my $offset1 = $m_1Flag?1:0;        # Account for M-1 data present.
   my $offset2 = $mp1Flag?1:0;        # Account for M+numC+1 data present.
   for (my $i=0; $i < $#fIndex; $i++) {
      $name = $cNames[$fIndex[$i]+$offset1];
      $mass = $cMasses[$fIndex[$i]+$offset1];
      
      my $temp = $msType->GetLabeledPositionString($name, $mass);      
      #Prints warning if compound and M+0 mass do not match.
      if ( !defined($temp) ) { push(@list1, ($name,$mass)); }
      push(@fragAtomStrs, $temp);

      $temp = $msType->GetNumFragmentedLabeledElement($name, $mass);
      #Prints warning if less than full measurements are found.
      if ( $temp != ($fIndex[$i+1]-$fIndex[$i]-($offset1+$offset2+1)) ) {
         push(@list2, ("$name-$mass", $temp, ($fIndex[$i+1]-$fIndex[$i]-($offset1+$offset2+1))));
      }
      
      push(@numCarbons, $msType->GetNumFragmentedCarbons($name, $mass));
   }

   if ( @list1 > 0 ) { &Error(206, @list1); } 
   if ( @list2 > 0 ) { &Warning(25, @list2); }
   
   # Set the data to be processed (Area or Response).
   if ( $dataFlag eq "A" ) { 
      @processData = @dataAreas;
   } else {
      @processData = @dataResps;
   }
   
   #Check that the number of replicates is consistent.
   my $numExp = @sNames;
      
	if (@replicates > 1) {
	   my $sum = 0;
		foreach my $val (@replicates) { $sum += $val; }
		
		if ( $sum != $numExp ) { &Error(215, $numExp, "[" . join(",", @replicates) . "]"); }
	} else {
	   my $val = $replicates[0];
	   if ( ($numExp%$val) != 0 ) { &Error(215, $numExp, $val); }
		
		for (my $i=1; $i < ($numExp/$val); $i++) { push(@replicates, $val) }
	}
	
   #Create the column header list (one per replicate).
   if ( $rhfFlag ) {
      @colHeaders = &ReadFileSkipBC($rhFile);
      
      # Check that the number of headers matches the number of replicate sets.
      if ( @replicates != @colHeaders ) { 
         &Error(216, $rhFile, scalar(@colHeaders), $numExp, scalar(@replicates)) 
      };      
   } elsif ( @replicates == $numExp ) {
      @colHeaders = @sNames;
   } else {
      for (my $i=1; $i<=@replicates; $i++) {
         push (@colHeaders, "Experiment-" . $i);
      }      
   }
   
   # Get original biomass data if correction flag is set.
   if ( $cobFlag ) {
      @obData = &ReadFileSkipBC($obFile);
      
      # Check that each OBM value is between 0 and 100% in decimal form.
		foreach my $val (@obData) { &CheckLimits($val, "specified in the file; $obFile", 0, 1, "D"); }

      # Check that there is the correct number of datasets
      if ( @obData != $numExp ) { &Error(221, $numExp, scalar(@obData)); }
   }
}


################################################################################
# Subroutine: SetDataClass
#
# Initializes the Mass Spec type with the correct data class.
# - Issue warning for dataTypes that require no derivatizing agent.
#
################################################################################
sub SetDataClass{
   my ($dataType, $dAgent) = @_;
   
   # Set the MS type object.
   if ( $dataType eq "AA" ) {
      $msType = new FluxY_Lib::MS::DataClass_AA($dAgent);
   } elsif ( $dataType eq "CW" ) {
      if ( uc($dAgent) ne "NONE" ) { &Warning(30, $dataType, $dAgent); }
      $msType = new FluxY_Lib::MS::DataClass_CW();          # No Derivatization
   } elsif ( $dataType eq "FA" ) {
      if ( uc($dAgent) ne "NONE" ) { &Warning(30, $dataType, $dAgent); }
      $msType = new FluxY_Lib::MS::DataClass_FA();          # No Derivatization
   } elsif ( $dataType eq "GE" ) {
      if ( uc($dAgent) ne "NONE" ) { &Warning(30, $dataType, $dAgent); }
      $msType = new FluxY_Lib::MS::DataClass_GE();          # No Derivatization
   } elsif ( $dataType eq "GL" ) {
      $msType = new FluxY_Lib::MS::DataClass_GL($dAgent);
   } elsif ( $dataType eq "GY" ) {
      $msType = new FluxY_Lib::MS::DataClass_GY($dAgent);
   } elsif ( $dataType eq "SM" ) {
      if ( uc($dAgent) ne "NONE" ) { &Warning(30, $dataType, $dAgent); }
      $msType = new FluxY_Lib::MS::DataClass_SM();          # No Derivatization
   }
}


################################################################################
# Subroutine: SetGenerateClass
#
# Initializes the model type with the correct model generating class.
#
################################################################################
sub SetGenerateClass{
   my ($modelFormat) = @_;
   
   # Set the MS type object.
   if ( $modelFormat eq "MS" ) {
      $modelType = new FluxY_Lib::MS::Generate13C($modelFormat);
   } elsif ( $modelFormat eq "LM" ) {
      $modelType = new FluxY_Lib::MS::Generate13C($modelFormat);
   } elsif ( $modelFormat eq "OF" ) {
      $modelType = new FluxY_Lib::MS::GenerateOpenFlux();
   } elsif ( $modelFormat eq "C2" ) {
      $modelType = new FluxY_Lib::MS::Generate13C_2();
   }
}



################################################################################
# Subroutine: CheckForMissingData
#
# Generate the missingData array - each element contains either a '0' if the
# data value is present or a flag value if the data value is missing.
#
# In addition the value in the process data list is set to zero in anticipation
# of the zero flag being set. If not set this list is not used.
#
# The detection flag is used to determine if the data was manually deleted, in
# which case the missing data is not flagged, just zeroed.
#
# Returns the number of missing data values.
#
################################################################################
sub CheckForMissingData{
   my $flag  = "MD";    # Value used to flag 'Missing Data'.
   my $dFlag = "MM-";   # Detection flag value indicating manual exclusion.
      
   my $count = 0;
   for (my $i=0; $i<@processData ; $i++) {
      my @data = @{$processData[$i]};
      
      for(my $j=0; $j<@data; $j++) {
         if ( !(defined $data[$j]) ) {              # Check for missing data.
            $data[$j] = $flag;                       # Flag if missing.
            $count++;                                # Count if missing.
            $processData[$i][$j] = 0;                # Always zero missing data.
         } elsif ($dataFlags[$i][$j] eq $dFlag) {   # Check if manually deleted.
            $data[$j] = 0;                           # Do not flag as missing.
            $processData[$i][$j] = 0;                # Always zero missing data.
         } else {
            $data[$j] = 0;
         }
      }
      
      push (@missingData, [ @data ]);
   }
   
   return $count;
}


################################################################################
# Subroutine: ExtractPre
#
# Each fragment may contain data measurements for one or more additional mass
# isotopes preceding the M+0 mass isotope. This will extract the values for one
# of these mass isotopes, without removing the correspoding mass isotope from
# the raw data.
#
# The required parameters are the number of preceding measurements, and the
# corresponding mass isotope reference. For example with 2 preceding values,
# there are measurements for M-1 and M-2. To extract the M-1 measurements the
# parameters are: (2,1).
#
# Returns a 2D array containing one measurement per fragment per dataset.
#
################################################################################
sub ExtractPre{
   my ($numPre, $numBack) = @_;
   my $offset = $numPre - $numBack;

   if ( $offset<0 ) { &Error(300, $numBack, $numPre); }
   
   my @data2D = ();
   for (my $i=0; $i<@processData; $i++) {
      my @data = @{$processData[$i]};
   
      my @m1 = ();
      for (my $j=0; $j<$#fIndex; $j++) {
         push(@m1, $data[$fIndex[$j]+$offset]);
      }
      push(@data2D, [ @m1 ]);
   }

   return @data2D;
}


################################################################################
# Subroutine: ExtractPost
#
# Each fragment may contain data measurements for one or more additional mass
# isotopes following the M+NumC mass isotope. This will extract the values for
# one of these mass isotopes, without removing the correspoding mass isotope
# from the raw data.
#
# The required parameters are the number of measurements after the M+NumC mass,
# and the corresponding mass isotope reference. For example with 2 following
# values, there are measurements for M+numC+1 and M+numC+2. To extract the
# M+numC+1 measurements the parameters are: (2,1).
#
# Returns a 2D array containing one measurement per fragment per dataset.
#
################################################################################
sub ExtractPost{
   my ($numAft, $ref) = @_;
   my $offset = $numAft - $numBack;

   if ( $offset<0 ) { &Error(301, $numBack, $numAft); }
   
   my @data2D = ();
   for (my $i=0; $i<@processData; $i++) {
      my @data = @{$processData[$i]};
   
      my @m1 = ();
      for (my $j=1; $j<@fIndex; $j++) {
         push(@m1, $data[$fIndex[$j] - $offset]);
      }		
      push(@data2D, [ @m1 ]);
   }

   return @data2D;
}


################################################################################
# Subroutine: Remove
#
# Each fragment may contain data measurements for one or more additional mass
# isotopes before the M+0 mass isotope, or after the N+numC mass isotope. This
# will removing these additional mass isotope measurements from the raw data
# before processing.
#
################################################################################
sub Remove{
   my ($type, $num) = @_;
   
   &CheckOptions($type, 0, "PRE", "POST");

   for (my $i=$#fIndex; $i>0; $i--) {
   
      my $offset = $fIndex[$i-1];
      if ( uc($type) eq "POST" ) { $offset  = $fIndex[$i] - $num; }

      for (my $j=0; $j<@processData; $j++) {
         my @data = @{$processData[$j]};
         my @rt   = @{$dataRT[$j]};
         
         splice(@data, $offset, $num);
         splice(@rt,   $offset, $num);
         
         $processData[$j] = [ @data ];
         $dataRT[$j]      = [ @rt ];
      }

      splice(@cNames,  $offset, $num);
      splice(@cMasses, $offset, $num);
   }

   for (my $i=1; $i<@fIndex; $i++) { $fIndex[$i] -= ($i*$num); }
}


################################################################################
# Subroutine: CheckData
#
# Incorporates all optional pre-correction data checking mechanisms, and the
# extraction and removal of extra data values before and after each
# compound/fragment mass isotopomers.
# - Extraction before removal is necessary if proton loss correction is to be
# applied.
#
################################################################################
sub CheckData{
   my $count     = 0;
   my $refCheck  = 0;
   my $checkFlag = 0;
   
   # Initialize the 2D arrays for storing the check results.
   my @overLimitData = ();
   my @poorPeakQual  = ();
   for (my $i=0; $i<@processData ; $i++) {
      my @data   = @{$processData[$i]};
      my @tmp = ();
      for (my $j=0; $j<@data ; $j++) {
         push(@tmp, 0);
      }
      push(@overLimitData, [ @tmp ]);
      push(@poorPeakQual, [ @tmp ]);
   }  

   if ( $dThreshold > 0 ) {
      if ( &CheckDL($dThreshold, \@processData, \@overLimitData) ) { $checkFlag += 1; }
   }

   if ( $tThreshold > 0 ) { 
      if ( &CheckPQ($tThreshold, \@processData, \@fIndex, \@poorPeakQual) ) { $checkFlag += 2; }
   }
   if ( $rThreshold > 0 ) { 
      if ( &CheckRT($rThreshold, \@dataRT, \@cNames) ) { $checkFlag += 4; }
   }

   # Summarize and output and flags from the data checks.
   if ( $checkFlag ) { 
      my @check = &ProcessChecks($checkFlag, \@overLimitData, \@poorPeakQual, \@dataRT, \@sNames, \@cMasses);
      &PrintMatrix("Flagged Problems", $baseName . "-Check.txt", \@sNames, \@cNames, \@cMasses, \@check);
      &Error(400, $baseName . ".txt", $baseName . "-Check.txt", $checkFlag);
   }
}


################################################################################
# Subroutine: CorrectData
#
# Incorporates all data correction mechanisms.
# - Applies an optional minimum value threshold to all measurement data, 
# including M-1 data if present and performing correction for proton loss.
# Corrects the measurement data one fragment at a time for any combination of;
# proton loss (M-1), natural abundance and original biomass (in that order).
#
# Each fragment is first checked to see if there is at least one non-zero
# measurement value, if not that fragment is skipped. No subsequent check is
# performed in any correction function, but a general check is performed by the
# scaling function.
#
################################################################################
sub CorrectData{
   for (my $i=0; $i<@processData ; $i++) {
      my @data   = @{$processData[$i]};
      my @plData = @{$M_1[$i]};
      my @pgData = @{$Mp1[$i]};
		
      for(my $j=0; $j<(@fIndex-1); $j++) {
         my @fragList = @data[$fIndex[$j]..($fIndex[$j+1]-1)];
			
			# Check if fragment contains no data (all mass values are zero). If
			# this is the case, no correction is attempted on this fragment.
			my $sum = 0;
			foreach my $frag (@fragList) { $sum += $frag; }
			if ( $sum == 0 ) { next; }
         
         # Apply value threshold before correction - to both the main fragment
         # values and if applicable the extra value corresponding to the fragment.
         if ( $vThreshold > 0 ) {
            if ( $cplFlag ) {
               ($plData[$j], @fragList) = &ThresholdFragment($vThreshold, $plData[$j], @fragList);
            } elsif ($cpgFlag) {
               ($pgData[$j], @fragList) = &ThresholdFragment($vThreshold, $pgData[$j], @fragList);
            } else {
               @fragList = &ThresholdFragment($vThreshold, 0, @fragList);
               
               shift(@fragList);   # Remove the (preceeding) extra value.
            }
         }

         #Perform proton loss, N-1, correection (if set)
         if ( $cplFlag ) {
            my $flag = 0;
            
			   ($plData[$j], @fragList) = &Scale($plData[$j], @fragList);
            ($flag, @fragList)       = &CorrectPL($plData[$j], @fragList);
            
            if ( $flag ) { &Warning(50, $flag, $sNames[$i], $cNames[$fIndex[$j]], $cMasses[$fIndex[$j]]); }
         }

         #Perform proton gain, N+1, correection (if set)
         if ( $cpgFlag ) {
            my $flag = 0;

			   ($pgData[$j], @fragList) = &Scale($pgData[$j], @fragList);
            ($flag, @fragList)       = &CorrectPG($pgData[$j], @fragList);
            
            if ( $flag ) { &Warning(50, $flag, $sNames[$i], $cNames[$fIndex[$j]], $cMasses[$fIndex[$j]]); }
			}

         # Scale the fragment values.
         @fragList = &Scale(@fragList);         

         #Perform Natural Abundance Correction (if set)
         if ( $cnaFlag ) { @fragList = &CorrectNA($cNames[$fIndex[$j]], $cMasses[$fIndex[$j]], $msType, \@fragList, $bbcFlag); }
         
         #Perform Original Biomass correction (if set)
         if ( $cobFlag ) { @fragList = &CorrectOB($numCarbons[$j], $obData[$i], \@fragList); }
         
         @data[$fIndex[$j]..($fIndex[$j+1]-1)] = @fragList;
      }
      
      push (@correctedData, [ @data ]);
   }
}


################################################################################
# Subroutine: CheckData2
#
# Incorporates all post correction data checking mechanisms.
#
################################################################################
sub CheckData2{
   if ( $aclFlag ) {
      my ($refAvgExp, $refAvgFrag, $refSDExp) = &calcAvgLabelling(\@processData, \@correctedData, \@fIndex, \@replicates);
      
      my $element = $msType->GetLabeledElement();
      my ($name)  = &GetElementNames($element);
               
      &PrintMatrixOPF("Avg. " . $element . " Labelling", $baseName . "-Avg" . $name . ".txt", \@colHeaders, \@cNames, \@cMasses, \@fIndex, $refAvgExp, $refAvgFrag);
		if ( $pacFlag ) {
         &PrintMatrixOPF("Avg. " . $element . " Labelling, S.D.", $baseName . "-Avg" . $name . "SD.txt", \@colHeaders, \@cNames, \@cMasses, \@fIndex, $refSDExp, $refAvgFrag); 
		}
   }
}
