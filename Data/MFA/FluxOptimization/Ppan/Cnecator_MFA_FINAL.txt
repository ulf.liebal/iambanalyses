#
//Reactions
r1	FruSubs -- >F6P	FruSubs-->F6P	ABCDEF-->ABCDEF	FRUpts2	0	1000
r2	ac --> AcCoA	ac-->AcCoA	AB-->AB	ACS	0	1000
r3	G6P --> F6P	G6P-->F6P	ABCDEF --> ABCDEF	PGI_f	0	1000
r4	F6P --> G6P	F6P-->G6P	ABCDEF --> ABCDEF	PGI_r	0	1000
r5	FBP --> F6P	FBP-->F6P	ABCDEF --> ABCDEF	FBP	0	1000
r6	FBP --> GAP + DHAP	FBP-->GAP+DHAP	ABCDEF-->CBA+DEF	FBA_f	0	1000
r7	GAP + DHAP --> FBP	GAP + DHAP --> FBP	CBA+DEF-->ABCDEF	FBA_r	0	1000
r8	GAP --> DHAP	GAP-->DHAP	ABC-->CBA	TPI_f	0	1000
r9	DHAP --> GAP	DHAP-->GAP	CBA-->ABC	TPI_r	0	1000
r10	GAP --> 13DPG	GAP-->13DPG	ABC-->ABC	GAPD_f	0	1000
r11	13DPG --> GAP	13DPG-->GAP	ABC-->ABC	GAPD_r	0	1000
r12	13DPG --> 3PG	13DPG-->3PG	ABC-->ABC	PGK_f	0	1000
r13	3PG --> 13DPG	3PG-->13DPG	ABC-->ABC	PGK_r	0	1000
r14	3PG --> 2PG	3PG-->2PG	ABC-->ABC	PGM_f	0	1000
r15	2PG --> 3PG	2PG-->3PG	ABC-->ABC	PGM_r	0	1000
r16	2PG --> PEP	2PG-->PEP	ABC-->ABC	ENO_f	0	1000
r17	PEP --> 2PG	PEP-->2PG	ABC-->ABC	ENO_r	0	1000
r18	PEP --> Pyr	PEP-->Pyr	ABC-->ABC	PYK	0	1000
r19	Pyr --> PEP	Pyr-->PEP	ABC-->ABC	PPS	0	1000
r20	G6P --> 6PGL	G6P-->6PGL	ABCDEF-->ABCDEF	G6PDH2r	0	1000
r22	R5P --> Rib5P	R5P-->Rib5P	ABCDE-->ACBDE	RPI_f	0	1000
r23	Rib5P --> R5P	Rib5P-->R5P	ABCDE-->ACBDE	RPI_r	0	1000
r24	R5P --> X5P	R5P-->X5P	ABCDE-->ACBDE	RPE_f	0	1000
r25	X5P --> R5P	X5P-->R5P	ABCDE-->ACBDE	RPE_r	0	1000
r26	Rib5P + X5P --> SH7P + GAP	Rib5P+X5P-->SH7P+GAP	ABCDE+FGHIJ-->FGABCDE+HIJ	TKT1_f	0	1000
r27	SH7P + GAP --> Rib5P + X5P	SH7P+GAP-->Rib5P+X5P	FGABCDE+HIJ-->ABCDE+FGHIJ	TKT1_r	0	1000
r28	SH7P + GAP --> E4P + F6P	SH7P+GAP-->E4P+F6P	ABCDEFG+HIJ-->DEFG+JHIABC	Tah_f	0	1000
r29	E4P + F6P --> SH7P + GAP	E4P+F6P-->SH7P+GAP	DEFG+JHIABC-->ABCDEFG+HIJ	Tah_r	0	1000
r30	E4P + X5P --> GAP + F6P	E4P+X5P-->GAP+F6P	ABCD+EFGHI-->ABC+DIHGFE	TKT2_f	0	1000
r31	GAP + F6P --> E4P + X5P	GAP+F6P-->E4P+X5P	ABC+DIHGFE-->ABCD+EFGHI	TKT2_r	0	1000
r32	6PG --> KDGP	6PG-->KDGP	ABCDEF-->ABCDEF	EDD	0	1000
r33	KDGP --> Pyr + GAP	KDGP-->Pyr+GAP	ABCDEF-->ABC+DEF	EDA	0	1000
r34	AcCoA + OAA --> Cit	AcCoA+OAA-->Cit	AB+CDEF-->FEDBAC	CS_f	0	1000
r36	Cit --> cisAco	Cit-->cisAco	ABCDEF-->ABCDEF	ACONT1_f	0	1000
r37	cisAco --> Cit	cisAco-->Cit	ABCDEF-->ABCDEF	ACONT1_r	0	1000
r38	cisAco --> ISCit	cisAco-->ISCit	ABCDEF-->ABCDEF	ACONT2_f	0	1000
r39	ISCit --> cisAco	ISCit-->cisAco	ABCDEF-->ABCDEF	ACONT2_r	0	1000
r40	ISCit --> AKG + CO2	ISCit-->AKG+CO2	ABCDEF-->ABCDE+F	ICDHx_f	0	1000
r42	SuccCoA --> Succ	SuccCoA-->Succ	ABCD-->ABCD	SUCOAS_f	0	1000
r43	Succ --> SuccCoA	Succ-->SuccCoA	ABCD-->ABCD	SUCOAS_r	0	1000
r44	Succ --> Fum	Succ-->Fum	ABCD-->ABCD	SUCD1	0	1000
r45	Fum --> Mal	Fum-->Mal	ABCD-->ABCD	FUM_f	0	1000
r46	Mal --> Fum	Mal-->Fum	ABCD-->ABCD	FUM_r	0	1000
r47	Mal --> OAA	Mal-->OAA	ABCD-->ABCD	MDH_f	0	1000
r48	OAA --> Mal	OAA-->Mal	ABCD-->ABCD	MDH_r	0	1000
r49	ISCit --> GLYOX + Succ	ISCit-->GLYOX+Succ	ABCDEF-->AB+EDCF	ICL_f	0	1000
r51	AcCoA + GLYOX --> Mal	AcCoA+GLYOX-->Mal	AB+CD-->CDBA	MALS	0	1000
r54	DHAP + E4P --> SH7P	DHAP+E4P-->SH7P	ABC+DEFG-->ABCDEFG	SBSAL_r	0	1000
r55	PEP + hco3 --> OAA	PEP+hco3-->OAA	ABC+D-->ABCD	PPC_f	0	1000
r56	Mal --> Pyr + CO2	Mal-->Pyr+CO2	ABCD-->ABC+D	ME1	0	1000
r57	AKG --> Lglt	AKG-->Lglt	ABCDE-->ABCDE	GLUDy_f	0	1000
r59	OAA + Lglt --> Lasp + AKG	OAA+Lglt-->Lasp+AKG	ABCD+EFGHI-->ABCD+EFGHI	ASPTA_f	0	1000
r60	Lasp + AKG --> OAA + Lglt	Lasp+AKG-->OAA+Lglt	ABCD+EFGHI-->ABCD+EFGHI	ASPTA_r	0	1000
r61	Lasp --> Lasp4P	Lasp-->Lasp4P	ABCD-->ABCD	ASPK	0	1000
r62	Lasp4P --> LaspSA	Lasp4P-->LaspSA	ABCD-->ABCD	ASPSAD_f	0	1000
r64	LaspSA --> Lhser	LaspSA-->Lhser	ABCD-->ABCD	HSDy	0	1000
r65	Lhser --> phom	Lhser-->phom	ABCD-->ABCD	HSK	0	1000
r66	phom --> Lthr	phom-->Lthr	ABCD-->ABCD	THRS	0	1000
r67	Lhser + AcCoA --> ahser	Lhser+AcCoA-->ahser	ABCD+EF-->ABCDEF	HSERA_f	0	1000
r69	ahser --> Lhcys + ac	ahser-->Lhcys+ac	ABCDEF-->ABCD+EF	CYTS4_f	0	1000
r71	Lcys + succLhser --> Succ + Lcystion	Lcys+succLhser-->Succ+Lcystion	ABC+DEFHGIJK-->GIJK+ABCHFED	CYTTS1_f	0	1000
r72	Succ + Lcystion --> Lcys + succLhser	Succ+Lcystion-->Lcys+succLhser	GIJK+ABCHFED-->ABC+DEFHGIJK	CYTTS1_r	0	1000
r73	Lcystion --> Pyr + Lhcys	Lcystion-->Pyr+Lhcys	ABCDEFG-->ABC+GFED	CYSTBL1	0	1000
r74	Lhcys + 5METTHF --> Lmet	Lhcys+5METTHF-->Lmet	ABCD+E-->ABCDE	METS	0	1000
r75	Pyr + LaspSA --> hthdp	Pyr+LaspSA-->hthdp	ABC+DEFG-->ABCGFED	DHDPS	0	1000
r76	hthdp --> thdp	hthdp-->thdp	ABCDEFG-->ABCDEFG	DHDPRy	0	1000
r77	thdp + SuccCoA --> sl2a6o	thdp+SuccCoA-->sl2a6o	ABCDEFG+HIJK-->ABCDEFGHIJK	THDPS	0	1000
r78	sl2a6o + Lglt --> sl28da + AKG	sl2a6o+Lglt-->sl28da+AKG	ABCDEFGHIJK+LMNOP-->ABCDEFGHIJK+LMNOP	SDPTA	0	1000
r79	sl28da --> 26dapLL + Succ	sl28da-->26dapLL+Succ	ABCDEFGHIJK-->GFEDCBA+HIJK	SDPDS	0	1000
r80	26dapLL --> 26dapM	26dapLL-->26dapM	ABCDEFG-->ABCDEFG	DAPE_f	0	1000
r82	26dapM --> Llys + CO2	26dapM-->Llys+CO2	ABCDEFG-->ABCDEF+G	DAPDC	0	1000
r83	3PG --> 3php	3PG-->3php	ABC-->ABC	PGLCED_f	0	1000
r85	3php + Lglt --> 3pser + AKG	3php+Lglt-->3pser+AKG	ABC+DEFGH-->ABC+DEFGH	PSERT_f	0	1000
r87	3pser --> Lser	3pser-->Lser	ABC-->ABC	PSP_L	0	1000
r88	Lser --> gly + METTHF	Lser-->gly+METTHF	ABC-->AB+C	GHMT3_f	0	1000
r89	gly + METTHF --> Lser	gly+METTHF-->Lser	AB+C-->ABC	GHMT3_r	0	1000
r91	METTHF --> 5METTHF	METTHF-->5METTHF	A-->A	MTHFR2	0	1000
r93	Lthr --> AKB	Lthr-->AKB	ABCD-->ABCD	THRD_L	0	1000
r94	Pyr + AKB --> 2a2hb + CO2	Pyr+AKB-->2a2hb+CO2	ABC+DEFG-->DEFGBC+A	ACHBS	0	1000
r95	2a2hb --> dh3mp	2a2hb-->dh3mp	ABCDEF-->ABECDF	KARIS2	0	1000
r96	dh3mp --> AKMV	dh3mp-->AKMV	ABCDEF-->ABCDEF	DHADT2	0	1000
r97	AKMV + Lglt --> Lile + AKG	AKMV+Lglt-->Lile+AKG	ABCDEF+GHIJK-->ABCDEF+GHIJK	ILETA_f	0	1000
r98	Lile + AKG --> AKMV + Lglt	Lile+AKG-->AKMV+Lglt	ABCDEF+GHIJK-->ABCDEF+GHIJK	ILETA_r	0	1000
r99	{2} Pyr --> AAL + CO2	Pyr+Pyr--> AAL+CO2	ABC+DEF-->ABCEF+D	ACLS	0	1000
r100	AAL --> dhiv	AAL-->dhiv	ABCDE-->ABDEC	KARI	0	1000
r101	dhiv --> 3mob	dhiv-->3mob	ABCDE-->ABCDE	DHADT1	0	1000
r102	3mob + Lglt --> Lval + AKG	3mob+Lglt-->Lval+AKG	ABCDE+FGHIJ-->ABCDE+FGHIJ	VALTA_f	0	1000
r103	Lval + AKG --> 3mob + Lglt	Lval+AKG-->3mob+Lglt	ABCDE+FGHIJ-->ABCDE+FGHIJ	VALTA_r	0	1000
r104	3mob + AcCoA --> 3c3hmp	3mob+AcCoA-->3c3hmp	ABCDE+FG-->ABCDEGF	IPPS	0	1000
r105	3c3hmp --> 3c2hmp	3c3hmp-->3c2hmp	ABCDEFG-->GFBCDEA	IPMALD2_f	0	1000
r107	3c2hmp --> 4mop + CO2	3c2hmp-->4mop+CO2	ABCDEFG-->ABCDEF+G	OMCDC	0	1000
r108	4mop + Lglt --> Lleu + AKG	4mop+Lglt-->Lleu+AKG	ABCDEF+GHIJK-->ABCDEF+GHIJK	LEUTA_f	0	1000
r109	Lleu + AKG --> 4mop + Lglt	Lleu+AKG-->4mop+Lglt	ABCDEF+GHIJK-->ABCDEF+GHIJK	LEUTA_r	0	1000
r110	Pyr + Lglt --> Lala + AKG	Pyr+Lglt-->Lala+AKG	ABC+DEFGH-->ABC+DEFGH	AATF_f	0	1000
r111	Lala + AKG --> Pyr + Lglt	Lala+AKG-->Pyr+Lglt	ABC+DEFGH-->ABC+DEFGH	AATF_r	0	1000
r112	Lcys --> Lala	Lcys-->Lala	ABC-->ABC	CDS	0	1000
r113	Lglt --> Lgln	Lglt-->Lgln	ABCDE-->ABCDE	GLNS	0	1000
r114	Lgln --> Lglt	Lgln-->Lglt	ABCDE-->ABCDE	GAM	0	1000
r115	R5P --> ruBP	R5P-->ruBP	ABCDE-->ABCDE	PRUK_f	0	1000
r117	ruBP + CO2 --> {2} 3PG	ruBP + CO2 --> 3PG + 3PG	ABCDE+F-->CBA+FDE	RBPC	0	1000
r118	Lser + AcCoA --> acser	Lser+AcCoA-->acser	ABC+DE-->ABCDE	SEROAT	0	1000
r119	acser --> Lcys + ac	acser-->Lcys+ac	ABCDE-->ABC+DE	CYSS	0	1000
r120	Lglt + AcCoA --> acglu	Lglt+AcCoA-->acglu	ABCDE+FG-->ABCDEFG	ACGS_f	0	1000
r122	acglu --> acg5p	acglu-->acg5p	ABCDEFG-->ABCDEFG	ACGK	0	1000
r123	acg5p --> acg5sa	acg5p-->acg5sa	ABCDEFG-->ABCDEFG	AGPR_f	0	1000
r125	acg5sa + Lglt --> acorn + AKG	acg5sa+Lglt-->acorn+AKG	ABCDEFG+HIJKL-->ABCDEFG+HIJKL	ACOTA_f	0	1000
r127	acorn --> orn + ac	acorn-->orn+ac	ABCDEFG-->ABCDE+FG	ACODA	0	1000
r128	Lglt + acorn --> acglu + orn	Lglt+acorn-->acglu+orn	ABCDE+FGHIJKL-->ABCDEKL+FGHIJ	GACT_h	0	1000
r129	CO2Subs --> CO2	CO2Subs-->CO2	A-->A	CO2_up	0	1000
r130	CO2 --> CO2Ex	CO2-->CO2Ex	A-->A	CO2t	0	1000
r131	CO2 --> hco3	CO2-->hco3	A-->A	HCO3E_f	0	1000
r132	hco3 --> CO2	hco3-->CO2	A-->A	HCO3E_r	0	1000
r133	form --> CO2	form-->CO2	A-->A	FDH	0	1000
r134	Lgln + hco3 --> Lglt + cbp	Lgln+hco3-->Lglt+cbp	ABCDE+F-->ABCDE+F	CBPS	0	1000
r135	orn + cbp --> citrL	orn+cbp-->citrL	ABCDE+F-->ABCDEF	OCT_f	0	1000
r136	citrL --> orn + cbp	citrL-->orn+cbp	ABCDEF-->ABCDE+F	OCT_r	0	1000
r137	citrL + Lasp --> argsuc	citrL+Lasp-->argsuc	ABCDEF+GHIJ-->ABCDEFGHIJ	ARGSS	0	1000
r138	argsuc --> Larg + Fum	argsuc-->Larg+Fum	ABCDEFGHIJ-->ABCDEF+JIHG	ARGSL_f	0	1000
r140	Lglt --> glu5p	Lglt-->glu5p	ABCDE-->ABCDE	GLU5K	0	1000
r141	glu5p --> glu5sa	glu5p-->glu5sa	ABCDE-->ABCDE	G5SD	0	1000
r142	glu5sa --> 1pyr5c	glu5sa-->1pyr5c	ABCDE-->ABCDE	G5SADs_f	0	1000
r144	1pyr5c --> Lpro	1pyr5c-->Lpro	ABCDE-->ABCDE	P5CR1	0	1000
r145	PEP + E4P --> 2dda7p	PEP+E4P-->2dda7p	ABC+DEFG-->ABCDEFG	DPHS	0	1000
r146	2dda7p --> 3dhq	2dda7p-->3dhq	ABCDEFG-->ABCDEFG	DHQS	0	1000
r147	3dhq --> 3dhsk	3dhq-->3dhsk	ABCDEFG-->ABCDEFG	DHQND_f	0	1000
r149	3dhsk --> skm	3dhsk-->skm	ABCDEFG-->ABCDEFG	SKDH	0	1000
r150	skm --> skm5p	skm-->skm5p	ABCDEFG-->ABCDEFG	SHKK	0	1000
r151	skm5p + PEP --> 3psme	skm5p+PEP-->3psme	ABCDEFG+HIJ-->ABCDEFGJHI	PSCIT_f	0	1000
r153	3psme --> chor	3psme-->chor	ABCDEFGHIJ-->ABCDEFGHIJ	CHORS	0	1000
r154	chor --> pphn	chor-->pphn	ABCDEFGHIJ-->ABCDEFGHIJ	CHORM	0	1000
r155	pphn --> phpyr + CO2	pphn-->phpyr+CO2	ABCDEFGHIJ-->BCDEFGHIJ+A	PPNDH	0	1000
r156	phpyr + Lglt --> Lphe + AKG	phpyr+Lglt-->Lphe+AKG	ABCDEFGHI+JKLMN-->IHGABCDEF+JKLMN	PHETA2_f	0	1000
r157	Lphe + AKG --> phpyr + Lglt	Lphe+AKG-->phpyr+Lglt	IHGABCDEF+JKLMN-->ABCDEFGHI+JKLMN	PHETA2_r	0	1000
r158	pphn --> hppyr + CO2	pphn-->hppyr+CO2	ABCDEFGHIJ-->BCDEFGHIJ+A	PPND	0	1000
r159	hppyr + Lglt --> Ltyr + AKG	hppyr+Lglt-->Ltyr+AKG	ABCDEFGHI+JKLMN-->IHGAFEDCB+JKLMN	TYRTA_f	0	1000
r160	Ltyr + AKG --> hppyr + Lglt	Ltyr+AKG-->hppyr+Lglt	IHGAFEDCB+JKLMN-->ABCDEFGHI+JKLMN	TYRTA_r	0	1000
r161	Rib5P --> PRPP	Rib5P-->PRPP	ABCDE-->EDCBA	PRPPS	0	1000
r162	chor + Lgln --> anth + Pyr + Lglt	chor+Lgln-->anth+Pyr+Lglt	ABCDEFGHIJ+KLMNO-->ABCDEFG+JHI+KLMNO	ANS	0	1000
r163	anth + PRPP --> pran	anth+PRPP-->pran	ABCDEFG+HIJKL-->ABCDEFGLKJIH	ANPRT	0	1000
r164	pran --> 2cpr5p	pran-->2cpr5p	ABCDEFGHIJKL-->ABCDEFGHIJKL	PRAI	0	1000
r165	2cpr5p --> 3ig3p + CO2	2cpr5p-->3ig3p+CO2	ABCDEFGHIJKL-->HIBCDEFGJKL+A	IG3PS	0	1000
r166	3ig3p --> indole + GAP	3ig3p-->indole+GAP	ABCDEFGHIJK-->ABCDEFGH+IJK	TRPS2_1_f	0	1000
r168	Lser + indole --> Ltrp	Lser+indole-->Ltrp	ABC+DEFGHIJK-->ABCDEFGHIJK	TRPS2	0	1000
r169	Lasp --> Lasn	Lasp-->Lasn	ABCD-->ABCD	ASNS	0	1000
r176	PRPP + METTHF --> prbatp	PRPP+METTHF-->prbatp	ABCDE+F-->ABCDEF	ATPPRT_f	0	1000
r178	prbatp --> prbamp	prbatp-->prbamp	ABCDEF-->ABCDEF	PRATPP	0	1000
r179	prbamp --> prfp	prbamp-->prfp	ABCDEF-->ABCDEF	PRAMPC	0	1000
r180	prfp --> prlp	prfp-->prlp	ABCDEF-->ABCDEF	PRMIIZCI	0	1000
r181	prlp + Lgln --> eig3p + Lglt	prlp+Lgln-->eig3p+Lglt	ABCDEF+GHIJK-->ABCDEF+GHIJK	IMG3PS	0	1000
r182	eig3p --> imacp	eig3p-->imacp	ABCDEF-->ABCDEF	IGPDH	0	1000
r183	imacp + Lglt --> hisp + AKG	imacp+Lglt-->hisp+AKG	ABCDEF+GHIJK-->ABCDEF+GHIJK	HSTPT_f	0	1000
r184	hisp + AKG --> imacp + Lglt	hisp+AKG-->imacp+Lglt	ABCDEF+GHIJK-->ABCDEF+GHIJK	HSTPT_r	0	1000
r185	hisp --> hisd	hisp-->hisd	ABCDEF-->ABCDEF	HISTP	0	1000
r186	hisd --> Lhis	hisd-->Lhis	ABCDEF-->ABCDEF	HISTD	0	1000
r187	AcCoA --> ac	AcCoA-->ac	AB-->AB	ACCOAHY	0	1000
r188	Lhcys + Succ --> succLhser	Lhcys+Succ-->succLhser	ABCD+EFGH-->ABCDEFGH	CYTTS5_f	0	1000
r189	succLhser --> Lhcys+Succ	succLhser-->Lhcys+Succ	ABCDEFGH-->ABCD+EFGH	CYTTS5_r	0	1000
r190	Pyr --> AcCoA + CO2	Pyr-->AcCoA+CO2	ABC -->BC+A	PYRUVDEH	0	1000
r191	AKG --> SuccCoA + CO2	AKG-->SuccCoA+CO2	ABCDE-->BCDE+A	AKGDH	0	1000
r192	{0.925} Mprotein + {0.075} Mcarbo --> Biomass	nd	nd	Biomass	0	1000
r193	{0.687} Lval + {0.997} Lpro + {1.135} gly + {0.115} Lcys + {0.764} Lthr + {1.211} Lala + {0.456} Larg + {0.369} Lasn + {0.43} Lphe + {0.223} Lhis + {0.159} Lmet + {0.522} Lleu + {0.369} Lasp + {0.008} Ltrp + {0.512} Lglt + {0.512} Lgln + {0.222} Ltyr + {0.306} Lile + {0.189} Llys + {0.421} Lser --> Mprotein	nd	nd	Protein	0	1000
r194	{0.950214672} G6P + {0.328635221} F6P + {0.597939964} DHAP + {2.618884339} 3PG + {3.33316766} PEP + {13.13150324} Pyr + {13.57184663} AcCoA + {5.000910289} AKG + {8.281700263} OAA + {4.161013223} R5P + {1.673304861} E4P --> Mcarbo	nd	nd	Carbohydrate	0	1000
r195	OAA --> PEP + CO2	OAA --> PEP + CO2	ABCD-->ABC+D	PEPck	0	1000
#
//Reversible_reactions
PGI	r3	r4	5.3.1.9	-1000	1000
FBA	r6	r7	4.1.2.13	-1000	1000
TPI	r8	r9	5.3.1.1	-1000	1000
GAPD	r10	r11	1.2.1.12	-1000	1000
PGK	r12	r13	2.7.2.3	-1000	1000
PGM	r14	r15	5.4.2.11	-1000	1000
ENO	r16	r17	4.2.1.11	-1000	1000
RPI	r22	r23	5.3.1.6	-1000	1000
RPE	r24	r25	5.1.3.1	-1000	1000
TKT1	r26	r27	2.2.1.1	-1000	1000
TAh	r28	r29	2.2.1.2	-1000	1000
TKT2	r30	r31	2.2.1.1	-1000	1000
ACONT1	r36	r37	4.2.1.3	-1000	1000
ACONT2	r38	r39	4.2.1.3	-1000	1000
SUCOAS	r42	r43	6.2.1.5	-1000	1000
FUM	r45	r46	4.2.1.2	-1000	1000
MDH	r47	r48	1.1.1.37	-1000	1000
ASPTA	r59	r60	2.6.1.1	-1000	1000
CYTTS1	r71	r72	2.5.1.48	-1000	1000
GHMT3	r88	r89	2.1.2.1	-1000	1000
ILETA	r97	r98	2.6.1.42	-1000	1000
VALTA	r102	r103	2.6.1.42	-1000	1000
LEUTA	r108	r109	(2.6.1.6; 2.6.1.42)	-1000	1000
AATF	r110	r111	2.6.1.2	-1000	1000
HCO3E	r131	r132	4.2.1.1	-1000	1000
OCT	r135	r136	2.1.3.3	-1000	1000
PHETA2	r156	r157	(2.6.1.1; 2.6.1.27,;2.6.1.57)	-1000	1000
TYRTA	r159	r160	(2.6.1.1; 2.6.1.5; 2.6.1.27; 2.6.1.57)	-1000	1000
HSTPT	r183	r184	2.6.1.9	-1000	1000
CYTTS5	r188	r189	2.5.1.48	-1000	1000
#
//Metabolites
FruSubs	6	no	carbonsource	no	C02336	0	300
form	1	no	no	no	C00058	0	300
G6P	6	no	no	no	Kegg:C00092	0	300
F6P	6	no	no	no	C05345	0	300
FBP	6	no	no	no	C00354	0	300
GAP	3	no	no	no	C00118	0	300
DHAP	3	no	no	no	C00111	0	300
13DPG	3	no	no	no	C00236	0	300
G3P	3	no	no	no	C00197	0	300
2PG	3	no	no	no	C00631	0	300
PEP	3	no	no	no	C00074	0	300
Pyr	3	no	no	no	C00022	0	300
6PGL	6	no	no	no	C01236	0	300
6PG	6	no	no	no	C00345	0	300
R5P	5	no	no	no	C00199	0	300
ruBP	5	no	no	no	C01182	0	300
CO2Subs	1	no	carbonsource	no	C00011	0	300
CO2	1	no	no	no	C00011	0	300
CO2Ex	1	no	no	excreted	C00011	0	300
hco3	1	no	no	no	C00288	0	300
Rib5P	5	no	no	no	C00117	0	300
X5P	5	no	no	no	C00231	0	300
SH7P	7	no	no	no	C05382	0	300
E4P	4	no	no	no	C00279	0	300
AcCoA	2	no	no	no	C00024	0	300
KDGP	6	no	no	no	C04442	0	300
OAA	4	no	no	no	C00036	0	300
Cit	6	no	no	no	C00158	0	300
aconc	6	no	no	no	C00417	0	300
TICit	6	no	no	no	C00451	0	300
AKG	5	no	no	no	C00026	0	300
SuccCoA	4	no	no	no	C00091	0	300
Succ	4	symmetry	no	no	C00042	0	300
Fum	4	symmetry	no	no	C00122	0	300
Mal	4	no	no	no	C00149	0	300
GLYOX	2	no	no	no	C00048	0	300
SHBP	7	no	no	no	C00447	0	300
Lglt	5	no	no	no	C00025	0	300
Lgln	5	no	no	no	C00064	0	300
Lasp	4	no	no	no	C00049	0	300
Lasn	4	no	no	no	C00152	0	300
Lasp4P	4	no	no	no	C03082	0	300
LaspSA	4	no	no	no	C00441	0	300
Lhser	4	no	no	no	C00263	0	300
phom	4	no	no	no	C01102	0	300
Lthr	4	no	no	no	C00188	0	300
succLhser	8	no	no	no	C01118	0	300
Lcys	3	no	no	no	C00097	0	300
Lcystion	7	no	no	no	C02291	0	300
ahser	6	no	no	no	C01077	0	300
Lhcys	4	no	no	no	C00155	0	300
Lmet	5	no	no	no	C00073	0	300
3php	3	no	no	no	C03232	0	300
3pser	3	no	no	no	C01005	0	300
Lser	3	no	no	no	C00065	0	300
gly	2	no	no	no	C00037	0	300
METTHF	1	no	no	no	(see THF)	0	300
5METTHF	1	no	no	no	(see THF)	0	300
AKB	4	no	no	no	C00109	0	300
2a2hb	6	no	no	no	C06006	0	300
dh3mp	6	no	no	no	C06007	0	300
AKMV	6	no	no	no	C00671	0	300
Lile	6	no	no	no	C00407	0	300
AAL	5	no	no	no	C06010	0	300
dhiv	5	no	no	no	C04272	0	300
3mob	5	no	no	no	C00141	0	300
Lval	5	no	no	no	C00183	0	300
3c3hmp	7	no	no	no	C02504	0	300
3c2hmp	7	no	no	no	C04411	0	300
4mop	6	no	no	no	C00233	0	300
Lleu	6	no	no	no	C00123	0	300
Lala	3	no	no	no	C00041	0	300
hthdp	7	no	no	no	C20258	0	300
thdp	7	no	no	no	C03972	0	300
sl2a6o	11	no	no	no	C04462	0	300
sl28da	11	no	no	no	C04421	0	300
26dapLL	7	no	no	no	C00666	0	300
26dapM	7	no	no	no	C00680	0	300
Llys	6	no	no	no	C00047	0	300
acser	5	no	no	no	C00979	0	300
ac	2	no	no	no	C00033	0	300
acglu	7	no	no	no	C00624	0	300
acg5p	7	no	no	no	C04133	0	300
acg5sa	7	no	no	no	C01250	0	300
acorn	7	no	no	no	C00437	0	300
orn	5	no	no	no	C00077	0	300
cbp	1	no	no	no	C00169	0	300
citrL	6	no	no	no	C00327	0	300
argsuc	10	no	no	no	C03406	0	300
Larg	6	no	no	no	C00062	0	300
glu5p	5	no	no	no	C03287	0	300
glu5sa	5	no	no	no	C01165	0	300
1pyr5c	5	no	no	no	C03912	0	300
Lpro	5	no	no	no	C00148	0	300
2dda7p	7	no	no	no	C04691	0	300
3dhq	7	no	no	no	C00944	0	300
3dhsk	7	no	no	no	C02637	0	300
skm	7	no	no	no	C00493	0	300
skm5p	7	no	no	no	C03175	0	300
3psme	10	no	no	no	C01269	0	300
chor	10	no	no	no	C00251	0	300
pphn	10	no	no	no	C00254	0	300
phpyr	9	no	no	no	C00166	0	300
Lphe	9	no	no	no	C00079	0	300
hppyr	9	no	no	no	C01179	0	300
Ltyr	9	no	no	no	C00082	0	300
anth	7	no	no	no	C00108	0	300
PRPP	5	no	no	no	C00119	0	300
pran	12	no	no	no	C04302	0	300
2cpr5p	12	no	no	no	C01302	0	300
3ig3p	11	no	no	no	C03506	0	300
indole	8	no	no	no	C00463	0	300
Ltrp	11	no	no	no	C00078	0	300
prbatp	6	no	no	no	C02739	0	300
prbamp	6	no	no	no	C02741	0	300
prfp	6	no	no	no	C04896	0	300
prlp	6	no	no	no	C04916	0	300
eig3p	6	no	no	no	C04666	0	300
imacp	6	no	no	no	C01267	0	300
hisp	6	no	no	no	C01100	0	300
hisd	6	no	no	no	C00860	0	300
Lhis	6	no	no	no	C00135	0	300
Mcarbo	1	no	no	no	(dummy)	0	300
Mprotein	1	no	no	no	(dummy)	0	300
Biomass	1	no	no	excreted	(dummy)	0	300
#
//Target_fragments
ALA_232	gcms	Lala_2:3	use	(dummy)
ALA_260	gcms	Lala_1:2:3	use	(dummy)
GLY_218	gcms	gly_2	use	(dummy)
GLY_246	gcms	gly_1:2	use	(dummy)
GLY_288	gcms	gly_1:2	use	(dummy)
VAL_302	gcms	Lval_1:2	use	(dummy)
VAL_260	gcms	Lval_2:3:4:5	use	(dummy)
VAL_288	gcms	Lval_1:2:3:4:5	use	(dummy)
LEU_200	gcms	Lleu_2:3:4:5:6	use	(dummy)
LEU_274	gcms	Lleu_2:3:4:5:6	use	(dummy)
LEU_344	gcms	Lleu_1:2:3:4:5:6	use	(dummy)
ILE_200	gcms	Lile_2:3:4:5:6	use	(dummy)
ILE_274	gcms	Lile_2:3:4:5:6	use	(dummy)
ILE_344	gcms	Lile_1:2:3:4:5:6	use	(dummy)
PRO_184	gcms	Lpro_2:3:4:5	use	(dummy)
PRO_258	gcms	Lpro_2:3:4:5	use	(dummy)
PRO_286	gcms	Lpro_1:2:3:4:5	use	(dummy)
PRO_328	gcms	Lpro_1:2:3:4:5	use	(dummy)
MET_218	gcms	Lmet_2:3:4:5	use	(dummy)
MET_292	gcms	Lmet_2:3:4:5	use	(dummy)
MET_320	gcms	Lmet_1:2:3:4:5	use	(dummy)
SER_302	gcms	Lser_1:2	use	(dummy)
SER_288	gcms	Lser_2:3	use	(dummy)
SER_362	gcms	Lser_2:3	use	(dummy)
SER_390	gcms	Lser_1:2:3	use	(dummy)
SER_432	gcms	Lser_1:2:3	use	(dummy)
THR_376	gcms	Lthr_2:3:4	use	(dummy)
THR_404	gcms	Lthr_1:2:3:4	use	(dummy)
THR_446	gcms	Lthr_1:2:3:4	use	(dummy)
PHE_302	gcms	Lphe_1:2	use	(dummy)
PHE_234	gcms	Lphe_2:3:4:5:6:7:8:9	use	(dummy)
PHE_308	gcms	Lphe_2:3:4:5:6:7:8:9	use	(dummy)
PHE_336	gcms	Lphe_1:2:3:4:5:6:7:8:9	use	(dummy)
ASP_302	gcms	Lasp_1:2	use	(dummy)
ASP_316	gcms	Lasp_2:3:4	use	(dummy)
ASP_390	gcms	Lasp_2:3:4	use	(dummy)
ASP_418	gcms	Lasp_1:2:3:4	use	(dummy)
ASP_460	gcms	Lasp_1:2:3:4	use	(dummy)
GLU_302	gcms	Lglt_1:2	use	(dummy)
GLU_330	gcms	Lglt_2:3:4:5	use	(dummy)
GLU_404	gcms	Lglt_2:3:4:5	use	(dummy)
GLU_432	gcms	Lglt_1:2:3:4:5	use	(dummy)
GLU_474	gcms	Lglt_1:2:3:4:5	use	(dummy)
LYS_329	gcms	Llys_2:3:4:5:6	use	(dummy)
LYS_431	gcms	Llys_1:2:3:4:5:6	use	(dummy)
HIS3_302 gcms	Lhis_1:2	use	(dummy)
HIS3_338 gcms	Lhis_2:3:4:5:6	use	(dummy)
HIS3_412 gcms	Lhis_2:3:4:5:6	use	(dummy)
HIS3_440 gcms	Lhis_1:2:3:4:5:6	use	(dummy)
HIS3_482 gcms	Lhis_1:2:3:4:5:6	use	(dummy)
TYR_302	gcms	Ltyr_1:2	use	(dummy)
TYR_364	gcms	Ltyr_2:3:4:5:6:7:8:9	use	(dummy)
TYR_438	gcms	Ltyr_2:3:4:5:6:7:8:9	use	(dummy)
TYR_466	gcms	Ltyr_1:2:3:4:5:6:7:8:9	use	(dummy)
TYR_508	gcms	Ltyr_1:2:3:4:5:6:7:8:9	use	(dummy)
