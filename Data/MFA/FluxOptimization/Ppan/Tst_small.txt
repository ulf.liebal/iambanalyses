#-------------------------------------------------------------------------------
# Name:        Example_4_Simulation_model.txt
#              Model definition file for Example 4 of mfapy
#
# Author:      Fumio_Matsuda
#
# Created:     12/06/2018
# Copyright:   (c) Fumio_Matsuda 2021
# Licence:     MIT license
#-------------------------------------------------------------------------------
//Reactions
PTS	SubsGlc --> G6P	SubsGlc --> G6P	ABCDEF --> ABCDEF	(kegg:R02848)	0	1000
CIN	SubsCO2 --> CO2in	SubsCO2 --> CO2in	A --> A	(no)	0	1000
PGI	G6P --> F6P	G6P --> F6P	ABCDEF --> ABCDEF	(kegg:R00771f)	0	1000
FBA	F6P --> {2}GAP	F6P --> GAP + GAP	ABCDEF --> CBA + DEF	(kegg:R01068r)	0	1000
ENO	GAP --> PEP	GAP --> PEP	ABC --> ABC	(kegg:R01068r)	0	1000
v3	PEP --> Pyr	PEP --> Pyr	ABC --> ABC	(kegg:R01196)	0	1000
v4	PEP + CO2in --> OAC	PEP + CO2in --> OAC	ABC + D --> ABCD	(kegg:Lacex)	0	1000
v5	OAC --> PEP + CO2in	OAC --> PEP + CO2in	ABCD --> ABC + D	(kegg:SubsGln)	0	1000
v6	OAC --> Pyr + CO2in	OAC --> Pyr + CO2in	ABCD --> ABC + D	(kegg:R00253)	0	1000
v7	Pyr --> AcCoA + CO2in	Pyr --> AcCoA + CO2in	ABC --> BC + A	(kegg:R00709f)	0	1000
v8	AcCoA + OAC --> Cit	AcCoA + OAC --> Cit	AB + CDEF --> FEDBAC	(kegg:R00351)	0.0	300
v9	Cit --> AKG + CO2in	Cit --> AKG + CO2in	ABCDEF --> ABCDE + F	(kegg:R00709)	0.0	300
v11	AKG --> Suc + CO2in	AKG --> Suc + CO2in	ABCDE --> BCDE + A	(kegg:R01197)	0.0	300
v12	Suc --> Fum	Suc --> Fum	ABCD --> ABCD	(kegg:R02164)	0.0	300
v13	Fum --> OAC	Fum --> OAC	ABCD --> ABCD	(kegg:R01082)	0.0	300
v14	OAC --> Fum	OAC --> Fum	ABCD --> ABCD	(kegg:R01082)	0.0	300
GLX	Cit + AcCoA --> Fum + Suc	Cit + AcCoA --> Fum + Suc	ABCDEF + GH --> ABHG + FCDE	(kegg:R00216)	0	1000
GND	G6P --> R5P + CO2in	G6P --> R5P + CO2in	ABCDEF --> BCDEF + A	(kegg:R01528)	0	1000
RPEf	R5P --> Xu5P	R5P --> Xu5P	ABCDE --> ABCDE	(kegg:R01529f)	0	1000
RPEr	Xu5P --> R5P	Xu5P --> R5P	ABCDE --> ABCDE	(kegg:R01529r)	0	1000
TKT1f	R5P + Xu5P --> S7P + GAP	R5P + Xu5P --> S7P + GAP	ABCDE + FGHIJ --> FGABCDE + HIJ	(kegg:R01641f)	0	1000
TKT1r	GAP + S7P --> Xu5P + R5P	GAP + S7P --> Xu5P + R5P	HIJ + FGABCDE --> FGHIJ + ABCDE	(kegg:R01641r)	0	1000
TALf	GAP + S7P --> F6P + E4P	GAP + S7P --> F6P + E4P	ABC + DEFGHIJ --> DEFABC + GHIJ	(kegg:R08575f)	0	1000
TALr	E4P + F6P --> S7P + GAP	E4P + F6P --> S7P + GAP	GHIJ + DEFABC --> DEFGHIJ + ABC	(kegg:R08575r)	0	1000
TKT2f	E4P + Xu5P --> F6P + GAP	E4P + Xu5P --> F6P + GAP	ABCD + EFGHI --> EFABCD + GHI	(kegg:R01067f)	0	1000
TKT2r	GAP + F6P --> Xu5P + E4P	GAP + F6P --> Xu5P + E4P	GHI + EFABCD --> EFGHI + ABCD	(kegg:R01067r)	0	1000
ASY	Pyr --> Ala	Pyr --> Ala	ABC --> ABC	(kegg:Ala)	0	1000
DSY	OAC --> Asp	OAC --> Asp	ABCD --> ABCD 	(kegg:R00243)	0.0	300
GSY	AKG --> Glu	AKG --> Glu	ABCDE --> ABCDE 	(kegg:R00243)	0.0	300
SSY	GAP --> Ser	GAP --> Ser	ABC --> ABC 	(kegg:R00243)	0.0	300
BIO	{3.748}AcCoA + {0.361}E4P + {0.071}F6P + {1.42}GAP + {0.205}G6P + {1.079}Glu + {1.329}OAC + {0.519}PEP + {2.345}Pyr + {0.898}R5P + {0.488}Ala + {0.205}Ser + {0.458}Asp --> Biomass	nd	nd	(kegg:Biomass)	0	1000
CEX	CO2in --> CO2ex	nd	nd	(kegg:Glu)	0	1000
#AEX	Ala --> Alaex	nd	nd 	(kegg:R00243)	0.0	300
BEX	Biomass --> Bex	nd	nd 	(kegg:R00243)	0.0	300
DEX	Asp --> Aspex	nd	nd 	(kegg:R00243)	0.0	300
#GEX	Glu --> Gluex	nd	nd 	(kegg:R00243)	0.0	300
#SEX	Ser --> Serex	nd	nd 	(kegg:R00243)	0.0	300
//Metabolites
AcCoA	2	no	no	no	(kegg:C00024)	0.0	300
AKG	5	no	no	no	(kegg:C00026)	0.0	300
Ala	3	no	no	no	(kegg:C00022)	0	300
#Alaex	3	no	no	excreted	(kegg:C00022)	0	300
Asp	4	no	no	no	(kegg:C00049)	0.0	300
Aspex	4	no	no	excreted	(kegg:C00011)	0.0	300
Biomass	43	no	no	no	(dummy)	0	300
Bex	43	no	no	excreted	(kegg:C00011)	0.0	300
Cit	6	no	no	no	(kegg:C00158)	0.0	300
CO2in	1	no	no	no	(dummy)	0	300
CO2ex	1	no	no	excreted	(kegg:C00011)	0.0	300
E4P	4	no	no	no	(kegg:C00092)	0	300
Fum	4	symmetry	no	no	(kegg:C00122)	0.0	300
F6P	6	no	no	no	(kegg:C00092)	0	300
GAP	3	no	no	no	(kegg:C00074)	0	300
Glu	5	no	no	no	(kegg:C00025)	0.0	300
#Gluex	5	no	no	excreted	(kegg:C00025)	0.0	300
G6P	6	no	no	no	(kegg:C00092)	0	300
OAC	4	no	no	no	(kegg:C00036)	0.0	300
PEP	3	no	no	no	(kegg:C00074)	0	300
Pyr	3	no	no	no	(kegg:C00022)	0	300
R5P	5	no	no	no	(kegg:C00092)	0	300
S7P	7	no	no	no	(kegg:C00092)	0	300
Ser	3	no	no	no	(kegg:C00022)	0	300
#Serex	3	no	no	excreted	(kegg:C00022)	0	300
Suc	4	symmetry	no	no	(kegg:C00042)	0.0	300
SubsGlc	6	no	carbonsource	no	(dummy)	0	300
SubsCO2	1	no	carbonsource	no	(dummy)	0	300
Xu5P	5	no	no	no	(kegg:C00092)	0	300
//Reversible_reactions
FUM	v13	v14	(kegg:R01082)	-300	300
RPE	RPEf	RPEr	(kegg:R01082)	-300	300
TKT1	TKT1f	TKT1r	(kegg:R01082)	-300	300
TKT2	TKT2f	TKT2r	(kegg:R01082)	-300	300
TAL	TALf	TALr	(kegg:R01082)	-300	300

//Target_fragments
Ala[M-57]	gcms	Ala_1:2:3	use	
Ala[M-85]	gcms	Ala_2:3	use	
Asp[M-85]	gcms	Asp_2:3:4	use	C17H40N3O1Si3
Asp[f302]	gcms	Asp_1:2	use	C14H32N2O1Si2
Glu[M-57]	gcms	Glu_1:2:3:4:5	use	C19H42N4O1Si3
Glu[M-85]	gcms	Glu_2:3:4:5	use	C18H42N3O1Si3
CO2m	gcms	CO2in_1	use	CO2
//End
