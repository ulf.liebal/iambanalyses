# %load ../Data/Coscine/Load_Metadata_GrowthProfiler.py
# %load ../Data/Coscine/Load_Metadata_OMICS.py
# %load ../Data/Coscine/Load_Metadata_ThesisReports.py
# %load ../Data/Coscine/Load_Metadata_GrowthProfiler.py

Directory = '../Data/GrowthProfiler' # file address on local computer
File = 'GP-en_Example.csv' # file name
FileAddress = os.path.join(Directory,File)
print('File & Size:\t{}\t{}'.format(File, sizeof_fmt(os.stat(FileAddress).st_size)))

form = resource.metadata_form()
form['Title'] = File
form['Creator'] = 'NA'
form['Supervisor'] = 'Lars Blank'
form['Institute'] = 'iAMB'
form['Project'] = 'NA'
form['Project Partners'] = ['NA']
form['Generation Date'] =  datetime.now().date()
form['Organism'] = 'NA'
form['Medium'] = 'NA'
form['Cultivation conditions'] = 'NA'
form['Comment'] = 'NA'

print(form)
