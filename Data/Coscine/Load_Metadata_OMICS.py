# %load ../Data/Coscine/Load_Metadata_OMICS.py
# %load ../Data/Coscine/Load_Metadata_ThesisReports.py
# %load ../Data/Coscine/Load_Metadata_GrowthProfiler.py


form = resource.metadata_form()
form['Creator'] = ''
form['Supervisor'] = ''
form['Institute'] = ''
form['Project'] = ''
form['Project Partners'] = ['']
form['Generation Date'] = '' # datetime.now()
form['Purpose'] = ''
form['Organism'] = ''
form['Medium'] = ''
form['Cultivation conditions'] = ''
form['Methodology'] = ''
form['Metadata excel file'] = ''
form['Comment'] = ''

print(form)

Directory = '' # file address on local computer
File = '' # file name
FileAddress = os.path.join(Directory,File)
print('File & Size:\t{}\t{}'.format(File, sizeof_fmt(os.stat(FileAddress).st_size)))