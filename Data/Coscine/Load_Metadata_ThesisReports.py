# %load ../Data/Coscine/Load_Metadata_OMICS.py
# %load ../Data/Coscine/Load_Metadata_ThesisReports.py


form = resource.metadata_form()
form['Title'] = ''
form['Name'] = ''
form['Final Date'] = datetime.now()
form['Supervisor, direct'] = ''
form['Reviewer(s)'] = ['Lars Blank', '']
form['Institute'] = 'iAMB'
form['Abstract'] = ''
form['Degree'] = ''
form['Study Course Field'] = ''
form['Organism'] = ''
form['Target Molecules'] = ''
form['Methodology'] = ['','']
form['Project'] = ''
form['Files Location'] = ''
form['Name of the archivist'] = ''

print(form)

Directory = '' # file address on local computer
File = '' # file name
FileAddress = os.path.join(Directory,File)
print('File & Size:\t{}\t{}'.format(File, sizeof_fmt(os.stat(FileAddress).st_size)))